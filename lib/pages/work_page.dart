import 'package:flutter/material.dart';
import 'package:fluttertoast/fluttertoast.dart';

import 'package:shequ/font_class/font_icons.dart';
import 'package:shequ/model/data_item_model.dart';
import 'package:shequ/model/tab_item_model.dart';
import 'package:shequ/pages/view_datail.dart';
import 'package:shequ/service/common_service.dart';
import 'package:shequ/service/work_service.dart';

import 'package:shequ/widget/list_data_pull_loading.dart';
import 'package:shequ/widget/loading_container.dart';
import 'package:shequ/widget/scroll_cross_tab.dart';
import 'package:shequ/widget/top_bar.dart';

class WorkPage extends StatefulWidget {
  @override
  _WorkPage createState() => _WorkPage();
}
class _WorkPage extends State<WorkPage> with AutomaticKeepAliveClientMixin {
  bool _loading = true;
  bool _contentLoading = false;
  bool _showToTopBtn = false;
  int _dataTotalCount = 0;
  List<DataItemModel> _dataArr = [];
  Map<String, List<TabItemModel>> _tabMap = {
    'workPageTab': [],
    'lifePageTab': [],
  };
  ScrollController _listViewController = new ScrollController();
  @override
  bool get wantKeepAlive => true;

  Map<String, dynamic> initSearchQuery = {
    'f': 0,
    'page': 1,
    'sort': 'new'
  };

  @override 
  void initState() {
    _retrieveState();
    _listenerScroll();
    _getAllTabForum();
    super.initState();
  }

  @override 
  void dispose() {
    _listViewController.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    super.build(context);
    return Scaffold(
      backgroundColor: Colors.white,
      body: LoadingContainer(
        isLoading: _loading,
        child: Column(
          children: <Widget>[
            // 顶部bar
            _appBar,
            // 中部导航
            _centerTab,
            // 页面主题内容
            LoadingContainer(
              isLoading: _contentLoading,
              child: _contentArea,
            )
          ],
        ),
      ),
      floatingActionButton: !_showToTopBtn ? null : _createFloatButton,
    );
  }

  // 顶部bar
  Widget get _appBar {
    return Column(
      children: <Widget>[
        Container(
          decoration: BoxDecoration(
            gradient: LinearGradient(
              colors: [Color(0x66000000), Colors.transparent],
              begin: Alignment.topCenter,
              end: Alignment.bottomCenter,
            )
          ),
          child: Container(
            padding: EdgeInsets.fromLTRB(0, 0, 0, 0),
            height: 80.0,
            child: TopBar(
              hideLeft: false,
              hideRight: false,
              titleText: '工作',
            ),
          )
        )
      ],
    );
  }

  // 中间的tab
  Widget get _centerTab {
    if (_tabMap['workPageTab'].length == 0) return Text('');
    return ScrollCrossTab(
      defaultKey: _tabMap['workPageTab'][0]?.id??0,
      tabArr: _tabMap['workPageTab'],
      tabCallBack: (TabItemModel item) {
        _initState(item);
        _retrieveState();
        if (!mounted) return;
        setState(() {
          _contentLoading = true;
        });
      },
    );
  }

  // 页面主题内容
  Widget get _contentArea {
    return MediaQuery.removePadding( // 清除顶部空白
      removeTop: true,
      context: context,
      child: Expanded(
        flex: 1,
        child: RefreshIndicator(
          onRefresh: _refresh,
          child: ListView.separated(
            shrinkWrap: true,
            controller: _listViewController,
            separatorBuilder: (context, index) {
              return Divider(color: Colors.black12);
            },
            itemCount: _dataArr.length,
            itemBuilder: (context, index) {
              if (_dataArr.length == 0 ) {  // 没有数据显示加载
                return ListDataPullLoading();
              }
              if (index >= _dataArr.length - 1) { // 显示完数组最后一条数据
                if (_dataArr.length < _dataTotalCount) { // 但是还没加载完数据库所有数据
                  _retrieveState();
                  return ListDataPullLoading();
                } else {
                  return Container(
                    alignment: Alignment.center,
                    padding: EdgeInsets.all(16),
                    child: Text('没有更多了', style: TextStyle(color: Colors.grey),),
                  );
                } 
              }
              return _contentItem(_dataArr[index]);
            }
          ),
        ),
      )
    ) ;
  }

  Widget _contentItem(DataItemModel item) {
    return Container(
      padding: EdgeInsets.fromLTRB(10, 5, 10, 1),
      child: Row(
        mainAxisSize: MainAxisSize.max,
        children: <Widget>[
          Expanded(
            child: Column(
              mainAxisAlignment: MainAxisAlignment.start,
              crossAxisAlignment: CrossAxisAlignment.start,
              children: <Widget>[
                Row(
                  mainAxisAlignment: MainAxisAlignment.start,
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: <Widget>[
                    ClipOval(
                      child: Image.network(item.userAvatar, fit: BoxFit.fill, 
                        height: 40,
                        width: 40,
                      ),
                    ),
                    Expanded(
                      flex: 1,
                      child: Padding(
                        padding: EdgeInsets.only(left: 10),
                        child: Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: <Widget>[
                            Row(
                              children: <Widget>[
                                Text(
                                  item.authorName, maxLines: 1, 
                                  softWrap: false, overflow: TextOverflow.ellipsis,
                                  style: TextStyle(fontWeight: FontWeight.w600, fontSize: 16),
                                ),
                                item.userLevelIcon != false ? Image.network(item.userLevelIcon, width: 60, height: 12,) : Text(''),
                              ],
                            ),
                            Padding(
                              padding: EdgeInsets.only(top: 5),
                              child: Text.rich(
                                TextSpan(
                                  children: [
                                    TextSpan(text: '发表于  ', style: TextStyle(color: Colors.black54, fontSize: 14)),
                                    TextSpan(
                                      text: item.createTimeFormat,
                                      style: TextStyle(color: Colors.black54, fontSize: 14)
                                    ),
                                  ]
                                ),
                              )
                            )
                          ],
                        )
                      )
                    ),
                  ],
                ),
                Row(
                  mainAxisAlignment: MainAxisAlignment.start,
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: <Widget>[
                    Expanded(
                      flex: 1,
                      child: Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: <Widget>[
                          Padding(
                            child: GestureDetector(
                              child: Text(
                                item.title, 
                                maxLines: 1, softWrap: false, 
                                overflow: TextOverflow.ellipsis,
                                style: TextStyle(fontWeight: FontWeight.w500, fontSize: 16),
                              ),
                              onTap: () {
                                _jumpToViewDetail(item);
                              },
                            ),
                            padding: EdgeInsets.symmetric(vertical: 5),
                          ),
                          Padding(
                            child: GestureDetector(                        
                              child: Text(item.content, maxLines: 2, softWrap: false, overflow: TextOverflow.ellipsis,),
                              onTap: () {
                                _jumpToViewDetail(item);
                              },
                            ),
                            padding: EdgeInsets.symmetric(vertical: 5),
                          ),
                          Padding(
                            padding: EdgeInsets.only(top: 5),
                            child: Row(
                              mainAxisAlignment: MainAxisAlignment.start,
                              children: <Widget>[
                                Padding(
                                  child: Text('【${item.forum}】'),
                                  padding: EdgeInsets.only(right: 15),
                                ),
                                Padding(
                                  padding: EdgeInsets.only(right: 10),
                                  child: Row(
                                    children: <Widget>[
                                      GestureDetector(                        
                                        child: Row(
                                          children: <Widget>[
                                            Icon(MyIcon.view, size: 20),
                                            Padding(
                                              child: Text(item.viewCount.toString()),
                                              padding: EdgeInsets.symmetric(horizontal: 5),
                                            )
                                          ],
                                        ),
                                        onTap: () {
                                          _jumpToViewDetail(item);
                                        },
                                      ),
                                    ],
                                  ),
                                ),
                                Padding(
                                  padding: EdgeInsets.only(right: 10),
                                  child: Row(
                                    children: <Widget>[
                                      GestureDetector(
                                        child: Icon(MyIcon.like, size: 18, color: item.currentUserPraised ? Colors.red : Colors.black26),
                                        onTap: () {
                                          _praiseItem(item);
                                        },
                                      ),
                                      Padding(
                                        child: Text(item.praiseCount.toString()),
                                        padding: EdgeInsets.symmetric(horizontal: 5),
                                      )
                                    ],
                                  ),
                                ),
                                Padding(
                                  padding: EdgeInsets.zero,
                                  child: Row(
                                    children: <Widget>[
                                      Icon(MyIcon.talk, size: 18),
                                      Padding(
                                        child: Text(item.replyCount.toString()),
                                        padding: EdgeInsets.symmetric(horizontal: 5),
                                      )
                                    ],
                                  ),
                                ),
                              ],
                            ),
                          ),
                        ],
                      ),
                    ),
                  ],
                )
              ],
            ),
          ),
          item.imgs.length >0  ? Padding(
            child: Image.network(item.imgs[0], width: 80, height: 80, fit: BoxFit.cover),
            padding: EdgeInsets.only(left: 10),
          ) : Text(''),
        ],
      )
    ); 
  }

  // 状态复位
  void _initState (TabItemModel item) {
    if (!mounted) return;
    setState(() {
      _dataTotalCount = 0;
      _dataArr = [];
      _showToTopBtn = false;
      initSearchQuery = {
        'f': int.parse(item.id.toString()),
        'page': 1,
        'sort': 'new'
      };
    });
  }

  // 置顶按钮
  Widget get _createFloatButton {
    return FloatingActionButton(
      heroTag: 'toWorkPageTop',
      child: Icon(Icons.arrow_upward),
      onPressed: () {
        _listViewController.animateTo(
          0,
          duration: Duration(milliseconds: 200),
          curve: Curves.ease
        );
      },
    );
  }
  
  // 监听滚动距离
  void _listenerScroll() {
    _listViewController.addListener(() {
      if (_listViewController.offset < 100 && _showToTopBtn) {
        if (!mounted) return;
        setState(() {
          _showToTopBtn = false;
        });
      } else if (_listViewController.offset >= 1000 && _showToTopBtn == false) {
        if (!mounted) return;
        setState(() {
          _showToTopBtn = true;
        });
      }
    });
  }

   // 点赞
  void _praiseItem(DataItemModel item) async {
    _praiseItemAsync(item);
  }

  // 跳转详情页
  void _jumpToViewDetail(DataItemModel item) {
    Navigator.push(
      context,
      MaterialPageRoute(builder: (BuildContext context) => ViewDetail(articleId: item.id.toString())),
    ).then((val) => val == true ? _refresh() : print('没做处理'));
  }

  // refresh
  Future<void> _refresh() async {
    _dataArr = [];
    initSearchQuery['page'] = 1;
    Future.delayed(const Duration(microseconds: 500), () {
      _retrieveState();
    });
  }

  // 列表数据接口
  Future<Null> _retrieveState() async {
    if (_dataArr.length > 0 ) {
      initSearchQuery['page'] += 1; 
    }
    var dataModel = await WorkService.fetchListData(initSearchQuery);
    if (dataModel['code'] == -1) {
      Navigator.pushNamed(context, 'LoginPage'); // 未登录跳转login页
      return;
    }
    dataModel = dataModel['data'];
    List<DataItemModel> _model = (dataModel['posts'] as List).map((item) => DataItemModel.fromJson(item)).toList();
    _dataArr.insertAll(
      _dataArr.length, 
      _model
    );
    if (!mounted) return;
    setState(() {
      _dataTotalCount = _dataTotalCount != 0? _dataTotalCount : dataModel['total'];
      _dataArr = _dataArr;
      _loading = false;
      _contentLoading = false;
    });
  }

  // 点赞接口调用
  Future<Null> _praiseItemAsync(DataItemModel item) async {
    Map<String, dynamic> postData = {
      'post_id': item.id.toString()
    };
    var dataModel = await WorkService.praiseItem(postData);
    if (dataModel['code'] == 0) {
      item.currentUserPraised = !item.currentUserPraised;
      Fluttertoast.showToast(
        gravity: ToastGravity.CENTER,
        backgroundColor: Colors.black,
        fontSize: 16,
        msg: item.currentUserPraised ? '点赞成功' : '取消点赞'
      );
      if (!mounted) return;
      setState(() {});
    }
  }

  // 获取所有tab
  Future<Null> _getAllTabForum() async{
    // List<TabItemModel> workPageTab = await StorageUtil.getStringListItem("workPageTab");
    // if (workPageTab != null && workPageTab.length > 0) {
    //   setState(() {
    //     _tabMap['workPageTab'] = workPageTab;
    //   }); 
    //   return;
    // }
    var dataModel = await CommonService.getAllForum({'is_show_all': 1});
    if (dataModel['code'] == 0) {
      (dataModel['data']['forum_types'] as List).forEach((item) {
        var _list = (item['forums'] as List).map((tabItem) => TabItemModel.fromJson(tabItem)).toList();
        if (item['title'] == '工作') {
          _tabMap['workPageTab'] = _list;
        } else if (item['title'] == '生活') {
          _tabMap['lifePageTab'] = _list;
        }
      });
      // StorageUtil.setStringListItem('workPageTab', _tabMap['workPageTab']);
      // StorageUtil.setStringListItem('lifePageTab', _tabMap['lifePageTab']);
    }
    setState(() {
      _tabMap = _tabMap;
    });
  }
}
