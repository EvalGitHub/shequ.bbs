import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_html/flutter_html.dart';
import 'package:fluttertoast/fluttertoast.dart';
// import 'package:flutter_html_view/flutter_html_view.dart';

import 'package:shequ/font_class/font_icons.dart';
import 'package:shequ/model/article_detail_model.dart';
import 'package:shequ/model/comment_model.dart';
import 'package:shequ/model/sub_comments_model.dart';
import 'package:shequ/service/work_service.dart';
import 'package:shequ/utils/confirm_model.dart';
import 'package:shequ/utils/storage_util.dart';
import 'package:shequ/widget/action_meun_sheet.dart';
import 'package:shequ/widget/comment_post_modal.dart';
import 'package:shequ/widget/loading_container.dart';

class ViewDetail extends StatefulWidget {
  final String articleId;
  const ViewDetail({Key key, @required this.articleId}) : super(key:key);

  @override 
  _ViewDetail createState() => _ViewDetail();
}
class _ViewDetail extends State<ViewDetail> {
  ArticleDetailModel _articleDetail = ArticleDetailModel.fromJson(null);
  bool _loading = true;
  bool _showToTopBtn = false;
  bool _hasCollect = false;
  bool _watchPosterOnlyFlag = false;
  int _initPage = 1;
  int _replyCountNumber = 0;
  String _uid;
  List<CommentModel> _commentListArr = [];
  ScrollController _listViewController = new ScrollController();
  final _contentController = new TextEditingController();
  @override
  void initState() {
    _getArticleDetail();
    _getComments({'id': widget.articleId.toString()});
    _listenerScroll();
    super.initState();
  }

  @override 
  void dispose() {
    _listViewController.dispose();
    _contentController.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('帖子详情'),
        actions: <Widget>[
          IconButton(
            icon: Icon(Icons.more_horiz),
            onPressed: () {
              _showActionMenu('ellipsis');
            }
          ),
        ],
      ),
      body: SafeArea(
        child: LoadingContainer(
          isLoading: _loading  ,
          child: ConstrainedBox(
            constraints: BoxConstraints.expand(),
            child: Stack(
              children: <Widget>[
                ListView(
                  controller: _listViewController,
                  children: <Widget>[
                    // 文章详情内容
                    Container(
                      padding: EdgeInsets.fromLTRB(10, 10, 10, 10),
                      child: Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: <Widget>[
                          // title
                          Padding(
                            child: Text(
                              _articleDetail.title, 
                              style: TextStyle(fontSize: 22, fontWeight: FontWeight.w700),
                              maxLines: 2,
                              softWrap: false, overflow: TextOverflow.ellipsis,
                            ),
                            padding: EdgeInsets.only(bottom: 15)
                          ),
                          // count
                          Padding(
                            child: Row(
                              crossAxisAlignment: CrossAxisAlignment.center,
                              children: <Widget>[
                                Text(_articleDetail.forumTitle, style: TextStyle(fontSize: 14, color: Colors.black45),),
                                Padding(
                                  child: Row(
                                    crossAxisAlignment: CrossAxisAlignment.center,
                                    children: <Widget>[
                                      Icon(MyIcon.view, size: 20, color: Colors.black45),
                                      Padding(
                                        child: Text(_articleDetail.viewCount.toString(), style: TextStyle(color: Colors.black54),),
                                        padding: EdgeInsets.fromLTRB(5, 0, 15, 0),
                                      ),
                                      Icon(MyIcon.talk, size: 16, color: Colors.black54),
                                      Padding(
                                        child: Text(_articleDetail.replyCount.toString(), style: TextStyle(color: Colors.black54),),
                                        padding: EdgeInsets.fromLTRB(5, 0, 0, 0),
                                      ),
                                    ],  
                                  ),
                                  padding: EdgeInsets.symmetric(horizontal: 15)
                                ),
                              ],
                            ),
                            padding: EdgeInsets.only(bottom: 20),
                          ),
                          // author info
                          Padding(
                            child: Row(
                              crossAxisAlignment: CrossAxisAlignment.center,
                              children: <Widget>[
                                _articleDetail.authorInfo.avatar128 != null ? Padding(
                                  child: ClipOval(
                                    child: Image.network(_articleDetail.authorInfo.avatar128, 
                                      fit: BoxFit.fill, 
                                      height: 35,
                                      width: 35,
                                    ),
                                  ),
                                  padding: EdgeInsets.only(right: 12),
                                ) : Text(''),
                                Column(
                                  crossAxisAlignment: CrossAxisAlignment.start,
                                  children: <Widget>[
                                    Row(
                                      mainAxisAlignment: MainAxisAlignment.start,
                                      children: <Widget>[
                                        LimitedBox(
                                          child: Text(
                                            _articleDetail.authorInfo.nickname,
                                            style: TextStyle(
                                              fontSize: 14,
                                              fontWeight: FontWeight.w500,
                                            ),
                                            overflow: TextOverflow.ellipsis,
                                            maxLines: 1,
                                          ),
                                          maxWidth: 80,
                                        ),
                                        Padding(
                                          child: Image.network(
                                            _articleDetail.authorInfo.level.icon,
                                            fit: BoxFit.fill, 
                                            height: 12,
                                            width: 30,
                                          ),
                                          padding: EdgeInsets.only(left: 10),
                                        )
                                      ],
                                    ),
                                    Text(
                                      _articleDetail.createTimeFormat,
                                      style: TextStyle(color: Colors.black54, fontSize: 13),
                                    ),
                                  ],
                                ),
                                // 按钮
                                Expanded(
                                  flex:1 ,
                                  child: Row(
                                    mainAxisAlignment: MainAxisAlignment.end,
                                    crossAxisAlignment: CrossAxisAlignment.center,
                                    children: <Widget>[
                                      Container(
                                        width: 50,
                                        height: 25,
                                        child: OutlineButton(
                                          child: Text('只看楼主', style: TextStyle(fontSize: 10)),
                                          padding: EdgeInsets.all(0),
                                          textColor: _watchPosterOnlyFlag ? Colors.blue : Colors.red,
                                          highlightedBorderColor: _watchPosterOnlyFlag ?  Colors.blue : Colors.red,
                                          borderSide: BorderSide(
                                            color: _watchPosterOnlyFlag ?  Colors.blue : Colors.red,
                                          ),
                                          onPressed: () {
                                            _watchPosterOnlyFun();
                                          },
                                        ),
                                      ),
                                      _articleDetail.uid.toString() != _uid ?
                                      Container(
                                        width: _articleDetail.authorInfo.followStatus != 1 ? 75 : 65,
                                        height: 25,
                                        child: OutlineButton(
                                          padding: EdgeInsets.fromLTRB(2, 0, 2, 0),
                                          borderSide: BorderSide(
                                            color: Colors.red,
                                          ),
                                          child: Row(
                                            children: <Widget>[
                                              Padding(
                                                child: Icon(
                                                  _articleDetail.authorInfo.followStatus == 1 ? Icons.done: Icons.add, 
                                                  size: 18
                                                ),
                                                padding: EdgeInsets.only(right: 2),
                                              ),
                                              Text(
                                                _articleDetail.authorInfo.followStatus != 1 ? '已关注' : '关注', 
                                                style: TextStyle(fontSize: 10))
                                            ],
                                          ),
                                          textColor: Colors.red,
                                          highlightedBorderColor: Colors.red,
                                          onPressed: () {
                                            _watch(_articleDetail);
                                          },
                                        ),
                                        padding: EdgeInsets.only(left: 10),
                                      ) : Text('')
                                    ],
                                  ),
                                )
                              ],
                            ),
                            padding: EdgeInsets.only(bottom: 10),
                          ),
                          // 文章详情
                          Container(
                            /* child: HtmlView(
                              data: _articleDetail.content,
                              baseURL: "", // optional, type String
                              onLaunchFail: (url) { // optional, type Function
                                print("launch $url failed");
                              },
                              scrollable: false, //false to use MarksownBody and true to use Marksown
                            ) */
                            child: Html(
                              data: _articleDetail.content,
                              defaultTextStyle: TextStyle(
                                fontSize: 16,
                                color: Colors.black,
                              ),
                            ),
                          ),
                        ],
                      ),
                    ),
                    // 分享操作按钮
                    Container(
                      padding: EdgeInsets.only(top: 20),
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.spaceAround,
                        children: <Widget>[
                          Column(
                            children: <Widget>[
                              GestureDetector(
                                child: CircleAvatar(
                                  radius: 24.0,
                                  backgroundColor: _articleDetail.currentUserPraised ? Colors.white10 : Colors.red[100],
                                  child: Icon(MyIcon.like, color: _articleDetail.currentUserPraised ? Colors.red : Colors.white, size: 24),
                                ),
                                onTap: () {
                                  _praiseItemAsync(_articleDetail);
                                }
                              ),
                              Padding(
                                child: Text(_articleDetail.praiseCount.toString()),
                                padding: EdgeInsets.only(top: 8),
                              ),
                            ],
                          ),
                          Column(
                            children: <Widget>[
                              CircleAvatar(
                                radius: 24.0,
                                backgroundColor: Color.fromARGB(100,210,160,100),
                                child: Icon(MyIcon.chat, color: Colors.white, size: 24),
                              ),
                              Padding(
                                child: Text('联系楼主', style: TextStyle(fontSize: 14),),
                                padding: EdgeInsets.only(top: 8),
                              )
                            ],
                          ),
                          GestureDetector(
                            onTap: () {
                              _showActionMenu('share');
                            },
                            child: Column(
                              children: <Widget>[
                                CircleAvatar(
                                  radius: 24.0,
                                  backgroundColor: Color.fromARGB(91,140,235,1),
                                  child: Icon(MyIcon.upload, color: Colors.white, size: 30),
                                ),
                                Padding(
                                  child: Text('分享', style: TextStyle(fontSize: 14),),
                                  padding: EdgeInsets.only(top: 8),
                                )
                              ],
                            ),
                          )
                        ],
                      ),
                    ),
                    // 评论专区
                    Container(
                      margin: EdgeInsets.fromLTRB(0, 30, 0, 100),
                      padding: EdgeInsets.fromLTRB(15, 15, 15, 0),
                      decoration: BoxDecoration(
                        border: Border(
                          top: BorderSide(
                            width: 0.5,
                            color: Colors.black26,
                          )
                        )
                      ),
                      child: Column(
                        children:  _generateCommentList()
                      ),
                    ),
                  ],
                ),   
                Positioned(
                  bottom: 0,
                  left: 0,
                  right: 0,
                  child: Container(
                    height: 45,
                    color: Colors.grey[300],
                    padding: EdgeInsets.fromLTRB(20, 10, 0, 5),
                    child: GestureDetector(
                      child: Text(
                        '我也说一句...',
                        style: TextStyle(
                          fontSize: 16,
                          color: Colors.red,
                        )
                      ),
                      onTap: () {
                        _createCommentModal(context);
                      },
                    ),
                  )
                ),
              ],
            )
          )
        ),
      ),
      // floatingActionButton: !_showToTopBtn ? null : _createFloatButton,
      // floatingActionButtonLocation: FloatingActionButtonLocation.centerDocked,
    );
  }

  // 没有内容的提示
  Widget get _noCommentsNode {
    return Center(
      child: Padding(
        padding: EdgeInsets.symmetric(vertical: 5),
        child: Text('没有更多回复', style: TextStyle(color: Colors.grey),),
      )
    );
  }

  // 生成评论列表
  _generateCommentList() {
    List<Widget> items = [];
    var _items;
    _items = _commentListArr.map((CommentModel item) => Container(
        margin: EdgeInsets.only(bottom: 20),
        decoration: BoxDecoration(
          border: Border(
            bottom: BorderSide(
              width: 0.3,
              color: Colors.black26,
            )
          ),
        ),
        padding: EdgeInsets.only(bottom: 10),
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          crossAxisAlignment: CrossAxisAlignment.start,
          children: <Widget>[
            // 评论人信息
            Row(
              mainAxisAlignment: MainAxisAlignment.start,
              children: <Widget>[
                ClipOval(
                  child: Image.network(
                    item.userAvatar, 
                    fit: BoxFit.fill, 
                    height: 35,
                    width: 35,
                  ),
                ),
                LimitedBox(
                  maxWidth: 200,
                  child: Padding(
                    padding: EdgeInsets.symmetric(horizontal: 10),
                    child: Text(
                      item.authorName,
                      maxLines: 1, softWrap: false, 
                      overflow: TextOverflow.ellipsis,
                      style: TextStyle(
                        fontSize: 16,
                        color: Colors.grey
                      )
                    ),
                  ),
                ),
                Image.network(
                  item.userLevelIcon,
                  fit: BoxFit.fill, 
                  height: 12,
                  width: 30,
                ),
              ],
            ),
            // 评论内容
            Padding(
              padding: EdgeInsets.symmetric(vertical: 10),
              child: Column(
                children: <Widget>[
                  Html(
                    data: item.content,
                    defaultTextStyle: TextStyle(
                      fontSize: 18,
                      color: Colors.black,
                    ),
                  ),
                  Container(
                    decoration: BoxDecoration(
                      border: Border(
                        top: BorderSide(
                          width: 0.5,
                          color: Colors.black12,
                        )
                      )
                    ),
                    child: _getSubComments(item),
                  ) 
                ],
              )
            ),
            Row(
              crossAxisAlignment: CrossAxisAlignment.center,
              mainAxisAlignment: MainAxisAlignment.center,
              children: <Widget>[
                Text(
                  '第${item.position}楼',
                  style: TextStyle(color: Colors.grey, fontSize: 14),
                ),
                Padding(
                  padding: EdgeInsets.symmetric(horizontal: 10),
                  child: Text(
                    item.createTimeFormat,
                    style: TextStyle(color: Colors.grey, fontSize: 14),
                  ),
                ),
                Expanded(
                  flex: 1,
                  child: Row(
                    crossAxisAlignment: CrossAxisAlignment.center,
                    mainAxisAlignment: MainAxisAlignment.end,
                    children: <Widget>[ 
                      GestureDetector(
                        child: Icon(MyIcon.recommend, size: 20, color: item.currentUserPraised ? Colors.red : Colors.grey),
                        onTap: () {
                          _praiseReply(item);
                        },
                      ),
                      Padding(
                        padding: EdgeInsets.fromLTRB(5, 0, 15, 0),
                        child: Text(
                          item.praiseCount.toString(),
                          style: TextStyle(color: Colors.grey, fontSize: 16),
                        ),
                      ),
                      GestureDetector(
                        child: Icon(MyIcon.talk, size: 16),
                        onTap: () {
                          _createReply(context, item);
                        },
                      )
                    ],
                  )
                )
              ],
            ),
          ],
        ),
      ) 
    ).toList();
    items.insertAll(0, _items);
    items.insert(items.length, _noCommentsNode);
    return items;
  }

  // 生成评论的评论
  _getSubComments(CommentModel item) {
    List<Widget> items = [];
    if (item.comments.length == 0) {
      return Container(
        width: 0,
        height: 0
      );
    }
    var _subContent = item.comments.map((SubCommentsModel subItem) => Column(
      children: <Widget>[
        Padding(
          padding: EdgeInsets.fromLTRB(0, 10, 0, 5),
          child: Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: <Widget>[
              Text(
                subItem.userInfo.nickname,
                style: TextStyle(
                  color: Colors.grey,
                ),
              ),
              Container(
                alignment: Alignment.centerRight,
                child: Text(
                  subItem.createTimeFormat,
                  style: TextStyle(
                    color: Colors.grey,
                  ),
                ),
              )
            ],
          ),
        ),
        Html(
          data: subItem.content,
          defaultTextStyle: TextStyle(
            fontSize: 14,
            color: Colors.black,
          ),
        ),
      ],
    ));
    items.insertAll(0, _subContent.toList());
    return Column(
      children: items,
    );
  }

  // 监听滚动距离
  void _listenerScroll() {
    _listViewController.addListener(() {
      if (_listViewController.offset < 100 && _showToTopBtn) {
        if (!mounted) return;
        setState(() {
          _showToTopBtn = false;
        });
      } else if (_listViewController.offset >= 1000 && _showToTopBtn == false) { // 根据滚动距离来显示置顶按钮
        if (!mounted) return;
        setState(() {
          _showToTopBtn = true;
        });
      }
      if (_listViewController.position.pixels == _listViewController.position.maxScrollExtent) {
        if (_commentListArr.length >= _replyCountNumber) {
          return;
        }
        _initPage++;
        _getComments({
          'id': widget.articleId.toString(),
          'page': _initPage,
          'author_only': _watchPosterOnlyFlag == true ? '1' : '0',
        });
      }
    });
  }

  // 置顶按钮
  /* Widget get _createFloatButton {
    return FloatingActionButton(
      heroTag: 'toViewTop',
      child: Icon(Icons.arrow_upward),
      onPressed: () {
        _listViewController.animateTo(
          0,
          duration: Duration(milliseconds: 200),
          curve: Curves.ease
        );
      },
    );
  } */

  // action meun
  _showActionMenu(String flag) {
    List<Map<String, dynamic>> actionMeun;
    if (flag == 'ellipsis') {
      actionMeun = [
        {
          'text': Text(_hasCollect ? '取消收藏' : '收藏', style: TextStyle(fontSize: 14)),
          'icon': Icon(_hasCollect ?  Icons.star : Icons.star_border, size: 30, color: Colors.red), //_hasCollect ?  Icons.star : Icons.star_border,
          'callBack': () => _addCollect({'post_id': _articleDetail.id.toString()}),
        },
      ];
      if (_articleDetail.uid.toString() == _uid) {
        actionMeun.addAll([
          {
            'text': Text('编辑', style: TextStyle(fontSize: 14)),
            'icon': Icon(Icons.edit, size: 30, color: Colors.red),
            'callBack': () => {
              print('edit')
            },
          },
          {
            'text': Text('删除', style: TextStyle(fontSize: 14)),
            'icon': Icon(Icons.delete_outline, size: 30, color: Colors.red),
            'callBack': () => _confirmDel(),
          },
        ]);
      }
      showModalBottomSheet(
        context: context, 
        builder: (BuildContext context) {
          return ActionMeunSheet(actionMeun: actionMeun, hasTopNav: false);
        }
      );
      return;
    } else if (flag == 'share') {
      actionMeun = [
        {
          'text': Text('', style: TextStyle(fontSize: 14)),
          'height': 50.0,
          'width': 50.0,
          'color': Colors.green,
          'icon': Icon(MyIcon.wechat_icon, color: Colors.white, size: 30),
          'callBack': () => print(''),
        },
        {
          'text': Text('', style: TextStyle(fontSize: 14)),
          'height': 50.0,
          'width': 50.0,
          'color': Colors.orange,
          'icon': Icon(MyIcon.qq_zone, color: Colors.white, size: 30),
          'callBack': () => print(''),
        },
        {
          'text': Text('', style: TextStyle(fontSize: 14)),
          'height': 50.0,
          'width': 50.0,
          'color': Colors.blue,
          'icon': Icon(MyIcon.qq_icon, color: Colors.white, size: 30),
          'callBack': () => print(''),
        },
        {
          'text': Text('', style: TextStyle(fontSize: 14)),
          'height': 50.0,
          'width': 50.0,
          'color': Colors.white,
          'icon': Icon(MyIcon.wechat_zone, color: Colors.green, size: 30),
          'callBack': () => print(''),
        }
      ];
      showModalBottomSheet(
        context: context, 
        builder: (BuildContext context) {
          return ActionMeunSheet(actionMeun: actionMeun, containerHeight: 200.0, hasTopNav: true);
        }
      );
    }
  }

  // 收藏帖子
  Future<Null> _addCollect(Map<String, dynamic>dataObj) async {
    var dataModel = await WorkService.addCollect(dataObj);
    Fluttertoast.showToast(
      gravity: ToastGravity.CENTER,
      backgroundColor: Colors.black,
      fontSize: 16,
      msg: dataModel['message'],
    );
    if (dataModel['code'] == 0) {
      setState(() {
        _hasCollect = !_hasCollect;
      });
      Navigator.of(context).pop(); // 关闭弹窗
    }
  }

  Future<void> _confirmDel() async {
    Navigator.of(context).pop(); // 关闭弹窗
    return ConfirmModelUtil.confirmModelFun(context, {
      'tipNav': '提示',
      'tipContent': '确认删除帖子吗？',
      'cancelMap': {
        'text': '取消',
        'callBack': null,
      },
      'sureMap': {
        'text':'确定',
        'callBack': () => _deletePost({'post_id': _articleDetail.id.toString()}),
      }
    });
  }

  // 删除帖子
  Future<Null> _deletePost(Map<String, dynamic>dataObj) async {
    var dataModel = await WorkService.deletePost(dataObj);
    Fluttertoast.showToast(
      gravity: ToastGravity.CENTER,
      backgroundColor: Colors.black,
      fontSize: 16,
      msg: dataModel['message'],
    );
    Future.delayed(const Duration(microseconds: 500), () {
      Navigator.of(context).pop(true); // 返回上一个页面
    });
  }

  // 回复评论
  _createReply(context, CommentModel item) {
    showModalBottomSheet(
      isScrollControlled:true,
      context: context, 
      builder: (BuildContext context) {
        return _replyBottomSheet(context, item);
      }
    );
  }

  // 回复评论的遮罩层_replyBottomSheet
  Widget _replyBottomSheet(BuildContext context, CommentModel item) {
    return SafeArea(
      child: SingleChildScrollView(
        child: Container(
          height: 45,
          padding: EdgeInsets.fromLTRB(10, 0, 10, 0),
          margin: EdgeInsets.only(bottom: MediaQuery.of(context).viewInsets.bottom), // 设置键盘的高度
          child: Row(
            mainAxisAlignment: MainAxisAlignment.center,
            crossAxisAlignment: CrossAxisAlignment.center,
            children: <Widget>[
              Padding(
                padding: EdgeInsets.only(right: 10),
                child: Icon(MyIcon.emoji, size: 30), 
              ),
              Expanded(
                flex: 1,
                child: TextField(
                  controller: _contentController,
                  // autofocus: true,
                  cursorColor: Colors.blue,
                  cursorWidth: 1,
                  decoration: InputDecoration(
                    border: InputBorder.none, // 无下边框
                    hintText: '请输入内容',
                    hintStyle: TextStyle(
                      color: Colors.grey,
                      fontSize: 15,
                    ),
                  ),
                ), 
              ),
              Container(
                width: 65,
                margin: EdgeInsets.only(left: 10),
                child: FlatButton(
                  child: GestureDetector(
                    child: Text('确定', style: TextStyle(fontSize: 14, color: Colors.white)),
                    onTap: () {
                      _showReplyDialog(item);
                    },
                  ),
                  onPressed: () {},
                  color: Colors.blue,
                  shape: RoundedRectangleBorder(
                    borderRadius: BorderRadius.circular(4)
                  ),
                )
              )
            ],
          ),
        )
      )
    );
  }

  // 评论帖子
  _createCommentModal(context) {
    showModalBottomSheet(
      isScrollControlled: true,
      context: context, 
      builder: (BuildContext context) {
        return _commentPostBottomSheet(context);
      }
    );
  }

  _commentPostBottomSheet(BuildContext context) {
    return CommentPostModal(
      id: widget.articleId, 
      addCommentCallBack: (dataObj) => _addComment(dataObj),
    );
  }

  Future<Null> _addComment(Map<String, dynamic>dataObj) async {
    var dataModel = await WorkService.addComments(dataObj);
    Fluttertoast.showToast(
      gravity: ToastGravity.CENTER,
      backgroundColor: Colors.black,
      fontSize: 16,
      msg: dataModel['message'],
    );
    if (dataModel['code'] == 0) {
      _getComments({'id': widget.articleId.toString()});
    } 
  }
 
  // 回复评论confirm modal 
  Future<void> _showReplyDialog(CommentModel item) async {
    return ConfirmModelUtil.confirmModelFun(context, {
      'tipNav': '提示',
      'tipContent': '是否匿名发布？',
      'cancelMap': {
        'text': '匿名',
        'callBack': () => _replyComments(item, 1),
      },
      'sureMap': {
        'text':'实名',
        'callBack': () => _replyComments(item, 0),
      }
    });
  }

  // 回复评论接口
  Future<Null> _replyComments(CommentModel item, int isAnon) async {
    Map<String, dynamic> dataObj = {
      'content': _contentController.text,
      'is_anon': isAnon.toString(),
      'reply_id': item.id.toString(),  
    };
    var dataModel = await WorkService.replyComments(dataObj);
    Navigator.of(context).pop();
    Fluttertoast.showToast(
      gravity: ToastGravity.CENTER,
      backgroundColor: Colors.black,
      fontSize: 16,
      msg: dataModel['message']
    );
    if (dataModel['code'] == 0) {
      _getComments({'id': widget.articleId.toString()});
      if (!mounted) return;
      setState(() {});
    } 
  }

  // 获取文章详情
  Future<Null> _getArticleDetail() async {
    _uid = await StorageUtil.getStringItem('uid');
    if (widget.articleId == null) {
      return;
    }
    Map<String, dynamic> dataObj = {
      'id': widget.articleId
    };
    var model = await WorkService.getArticleDetail(dataObj);
    if (!mounted) return;
    setState(() {
      _articleDetail = ArticleDetailModel.fromJson(model['post']);
      _loading = false;
      _hasCollect = model['post']['current_user_collected'];
    });  
  }

  // 点赞接口调用
  Future<Null> _praiseItemAsync(ArticleDetailModel item) async {
    Map<String, dynamic> postData = {
      'post_id': item.id.toString()
    };
    var dataModel = await WorkService.praiseItem(postData);
    if (dataModel['code'] == 0) {
      item.currentUserPraised = !item.currentUserPraised;
      Fluttertoast.showToast(
        gravity: ToastGravity.CENTER,
        backgroundColor: Colors.black,
        fontSize: 16,
        msg: item.currentUserPraised ? '点赞成功' : '取消点赞'
      );
      if (!mounted) return;
      setState(() {});
    }
  }

  // 加关注
  Future<Null> _watch(ArticleDetailModel item) async {
    Map<String, dynamic> postData = {
      'uid': item.id.toString()
    };
    var dataModel = await WorkService.watch(postData);
    if (dataModel['code'] == 0) {
      Fluttertoast.showToast(
        gravity: ToastGravity.CENTER,
        backgroundColor: Colors.black,
        fontSize: 16,
        msg: item.authorInfo.followStatus == 1 ? '关注成功' : '取消关注'
      );
      if (item.authorInfo.followStatus == 1) {
        item.authorInfo.followStatus = 0;
      } else {
        item.authorInfo.followStatus = 1;
      }
      if (!mounted) return;
      setState(() {});
    }
  }

  // 获取评论信息
  Future<Null> _getComments(Map<String, dynamic>queryObj) async {
    Map<String, dynamic> postData = {
      'id': queryObj['id'] ?? '',
      'page': queryObj['page'] ?? _initPage,
      'author_only': queryObj['author_only'] ?? '0'
    };
    var dataModel = await WorkService.getComments(postData);
    if (!mounted) return;
    var _tmpList = (dataModel['replys'] as List).map((item) => CommentModel.fromJson(item)).toList();
    queryObj['page'].toString() == '1' ? _commentListArr = _tmpList : _commentListArr.addAll(_tmpList);
    setState(() {
      _commentListArr = _commentListArr;
      _replyCountNumber = dataModel['total'];
    });  
  }

  // 点赞回复
  Future<Null> _praiseReply(CommentModel item) async {
    Map<String, dynamic> postData = {
      'reply_id': item.id.toString(),
    };
    var dataModel = await WorkService.praiseReply(postData);
    if (dataModel['code'] == 0) {
      item.currentUserPraised = !item.currentUserPraised;
      Fluttertoast.showToast(
        gravity: ToastGravity.CENTER,
        backgroundColor: Colors.black,
        fontSize: 16,
        msg: item.currentUserPraised ? '点赞成功' : '取消点赞'
      );
      setState(() {});
    }
  }

  // 只看楼主
  _watchPosterOnlyFun() {
    setState(() {
      _watchPosterOnlyFlag = !_watchPosterOnlyFlag;
    });
    _getComments({
      'id': widget.articleId.toString(),
      'page': _initPage,
      'author_only': _watchPosterOnlyFlag == true ? '1' : '0',
    });
    Fluttertoast.showToast(
      gravity: ToastGravity.CENTER,
      backgroundColor: Colors.black,
      fontSize: 16,
      msg: _watchPosterOnlyFlag == true ? '进入只看楼主模式' : '退出只看楼主模式'
    );
  }
}