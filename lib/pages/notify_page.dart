import 'package:flutter/material.dart';

class NotifyMessage extends StatefulWidget {
  @override 
  _NotifyMessage createState() => _NotifyMessage();
}

class _NotifyMessage extends State<NotifyMessage> {
  @override 
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(title: Text('消息通知')),
      body: _bodyContent,
    );
  }

  Widget get _bodyContent {
    return Center(
      child: Text('没有跟多信息')
    );
  }
}