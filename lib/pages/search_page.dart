import 'package:flutter/material.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:shequ/model/blog_model.dart';
import 'package:shequ/model/seach_user_info_model.dart';
import 'package:shequ/pages/view_datail.dart';
import 'package:shequ/service/search_service.dart';
import 'package:shequ/widget/dropdown.dart';
import 'package:shequ/widget/list_data_pull_loading.dart';

class SearchPage extends StatefulWidget {
  @override 
  _Search createState() => _Search();
}

class _Search extends State<SearchPage> {
  final _searchContentController = TextEditingController();
  final ScrollController _listViewController = new ScrollController();
  bool _showToTopBtn = false;
  var selectValue = 'post';
  int initPage = 1;
  List searchResult = [];
  int resultTotalNumber = 0;
  List dropItemList = [
    {'text': '贴子', 'value': 'post'},
    {'text': '用户', 'value': 'user'},
  ];

  @override 
  void initState() {
    _listenerScroll();
    super.initState();
  }

  @override 
  void dispose() {
    _searchContentController.dispose();
    _listViewController.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: SafeArea(
        top: false,
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          mainAxisAlignment: MainAxisAlignment.start,
          children: <Widget>[
            _searchTopTab,
            _bodyContent(context),
          ],
      )
      ),
      floatingActionButton: !_showToTopBtn ? null : _createFloatButton,
    );
  }

  _bodyContent(BuildContext context) {
    if (searchResult.length == 0) {
      return noContent;
    } else {
      return MediaQuery.removePadding(
        removeTop: true,
        context: context,
        child: Expanded(
          flex: 1,
          child: ListView.separated(
            controller: _listViewController,
            separatorBuilder:(context, index) {
              return Divider(color: Colors.black12);
            },
            itemCount: searchResult.length,
            itemBuilder: (context, index) {
              if (searchResult.length == 0 ) {  // 没有数据显示加载
                return ListDataPullLoading();
              }
              if (index >= searchResult.length -1 ) { // 显示完数组最后一条数据
                if (searchResult.length < resultTotalNumber) { // 但是还没加载完数据库所有数据
                  _getSearchResult({'page': searchResult.length > 0 ? ++initPage : '1'});
                  return ListDataPullLoading();
                } else {
                  return Container(
                    alignment: Alignment.center,
                    padding: EdgeInsets.all(16),
                    child: Text('没有更多了', style: TextStyle(color: Colors.grey),),
                  );
                }
              } 
              return selectValue == 'post' ? 
                generateBlogContent(searchResult[index]) 
                : 
                generateUserContent(searchResult[index]);
            }, 
          ),
        )
      );
    }
  }

  // 置顶按钮
  Widget get _createFloatButton {
    return FloatingActionButton(
      heroTag: 'toViewTop',
      child: Icon(Icons.arrow_upward),
      onPressed: () {
        _listViewController.animateTo(
          0,
          duration: Duration(milliseconds: 200),
          curve: Curves.ease
        );
      },
    );
  }

  // 生成帖子内容
  generateBlogContent(BlogModel item) {
    return Container(
      padding: EdgeInsets.fromLTRB(10, 5, 10, 5),
      child: GestureDetector(
        onTap: () {
          Navigator.push(
            context,
            MaterialPageRoute(builder: (BuildContext context) => ViewDetail(articleId: item.id.toString())),
          );
        },
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          mainAxisAlignment: MainAxisAlignment.center,
          children: <Widget>[
            Padding(
              child: Text(
                item.title,
                maxLines: 2,
                softWrap: false, 
                overflow: TextOverflow.ellipsis,
                style: TextStyle(
                  fontSize: 16,
                  fontWeight: FontWeight.w500
                ),
              ),
              padding: EdgeInsets.only(bottom: 5)
            ),
            Row(
              children: <Widget>[
                Expanded(
                  flex: 1,
                  child: Text(
                    "板块：${item.forumTitle}", 
                    style: TextStyle(fontSize: 15, color: Colors.black54),),
                ),
                Text(item.createTime),
              ],
            )
          ],
        ),
      )
    );
  }

  // 生成用户内容
  generateUserContent(SearchUserInfoModel item) {
    return Container(
      padding: EdgeInsets.fromLTRB(10, 5, 10, 5),
      child: Row(
        children: <Widget>[
          Padding(
            padding: EdgeInsets.only(right: 10),
            child: ClipOval(
              child: Image.network(item.avatar128, fit: BoxFit.fill, 
                height: 40,
                width: 40,
              ),
            ),
          ),
          Column(
            mainAxisAlignment: MainAxisAlignment.center,
            crossAxisAlignment: CrossAxisAlignment.start,
            children: <Widget>[
              Row(
                mainAxisAlignment: MainAxisAlignment.start,
                crossAxisAlignment: CrossAxisAlignment.center,
                children: <Widget>[
                  Padding(
                    padding: EdgeInsets.fromLTRB(0, 0, 0, 5),
                    child: Text(item.nickname, style: TextStyle(fontSize: 16, fontWeight: FontWeight.w500)),
                  ),
                  Image.network(
                    item.level.icon, 
                    fit: BoxFit.fill, 
                    height: 14,
                    width: 35,)
                ],
              ),
              Text(item.signature),
            ],
          )
        ],
      ),
    );
  }

  // 无搜索内容
  Widget get noContent {
    return Column(
      mainAxisAlignment: MainAxisAlignment.start,
      children: <Widget>[
        Image.asset(
          'images/no_result.png', 
          fit: BoxFit.fill
        ),
        Padding(
          child: Text(
            '没找到搜索结果',
            style: TextStyle(
              color: Colors.black,
              fontSize: 22,
            ),
          ),
          padding: EdgeInsets.only(bottom: 10)
        ),
        Text('请检查关键字后重试', 
          style: TextStyle(
            fontSize: 16,
            color: Colors.black38
          )
        ),
      ],
    );
  }

  // 顶部操作tab
  Widget get _searchTopTab {
    return Container(
      height: 80,
      padding: EdgeInsets.fromLTRB(20, 34, 20, 5),
      decoration: BoxDecoration(
        color: Colors.white,
        border: Border(
          bottom: BorderSide(
            width: 0.5,
            color: Colors.black26,
          )
        )
      ),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.center,
        crossAxisAlignment: CrossAxisAlignment.center,
        children: <Widget>[
          Container(
            width: 70,
            margin: EdgeInsets.only(right: 10),
            child: Dropdown(
              defaultValue: selectValue, 
              dropItemList: dropItemList,
              selectCallBack: getSelectKey,
            ),
          ),
          Expanded(
            flex: 1,
            child: TextField(
              controller: _searchContentController,
              autofocus: true,
              cursorColor: Colors.blue,
              cursorWidth: 1,
              // stextAlign: TextAlign.center,
              decoration: InputDecoration(
                border: InputBorder.none,
                contentPadding: EdgeInsetsDirectional.fromSTEB(15, 5, 0, 5),
                hintText: '请输入搜索内容',
                hintStyle: TextStyle(
                  color: Colors.grey,
                  fontSize: 16,
                ),
              ),
              onEditingComplete: () {
                _getSearchResult({});
              },
            ), 
          ),
          Padding(
            padding: EdgeInsets.only(left: 10),
            child: GestureDetector(
              child: Text('取消',
                style: TextStyle(
                  color: Colors.blue,
                  fontSize: 18
                ),
              ),
              onTap: () {
                Navigator.pop(context);
              },
            ),
          )
        ],
      ),
    );
  }

  // 下拉框的回调
  getSelectKey(String key) {
    if (key != selectValue) {
      if (!mounted) return;
      setState(() {
        selectValue = key;
        searchResult = [];
        _showToTopBtn = false;
      });
    }
    if (_searchContentController.text == '') {
      return;
    }
    _getSearchResult({
      'type': selectValue,
      'page': initPage.toString(),
    });
  }

  // 监听滚动距离
  void _listenerScroll() {
    _listViewController.addListener(() {
      if (_listViewController.offset < 100 && _showToTopBtn) {
        if (!mounted) return;
        setState(() {
          _showToTopBtn = false;
        });
      } else if (_listViewController.offset >= 1000 && _showToTopBtn == false) {
        if (!mounted) return;
        setState(() {
          _showToTopBtn = true;
        });
      }
    });
  }

  // 获取搜索结果
  Future<Null> _getSearchResult(Map<String, dynamic>data) async {
    Map<String, dynamic> postData = {
      'type': data['type']??selectValue,
      'page': data['page']??initPage.toString(),
      'key': _searchContentController.text,
    };
    var dataModel = await SearchService.getSearchResult(postData);
    if (dataModel['code'] != 0) {
      Fluttertoast.showToast(
        gravity: ToastGravity.CENTER,
        backgroundColor: Colors.black,
        fontSize: 16,
        msg: dataModel['msg']??'查询参数有误！'
      );
    } else {
      var _searchResult = (dataModel['data']['rows'] as List).map(
        (item) => selectValue == 'user' ?  SearchUserInfoModel.fromJson(item) : 
          BlogModel.fromJson(item)
        ).toList();
      searchResult.insertAll(
        searchResult.length, 
        _searchResult
      );
      if (!mounted) return;
      setState(() {
        searchResult = searchResult;
        resultTotalNumber = dataModel['data']['total'];
      });
    }
  }
}