import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:image_picker/image_picker.dart';
import 'package:shequ/model/community_model.dart';
import 'package:shequ/model/tab_arr_model.dart';
import 'package:shequ/service/community_service.dart';
import 'package:shequ/utils/upload_image.dart';
import 'package:shequ/widget/bottom_picker.dart';

class AddCommunity extends StatefulWidget {
  @override 
  _AddCommunity createState() => _AddCommunity();
}

class _AddCommunity extends State<AddCommunity> {
  List _tabModelList = [];
  int selectedCommunityTypeValue;
  TabArrItemModel selectedCommunityType;
  CommunityModel selectedCommunityModule;
  List<CommunityModel> _communityModelList = [];
  TextEditingController _communityNameControl = new TextEditingController();
  TextEditingController _communityDesControl = new TextEditingController();
  Map<String, dynamic> imageObj = {};

  @override 
  void initState() {
    _getClubTypeList();
    _getClubModelList();
    super.initState();
  }

  @override
  void dispose() {
    _communityNameControl.dispose();
    _communityDesControl.dispose();
    super.dispose();
  }

  Widget build(BuildContext context) {
    return Scaffold(
      appBar: _appBar(context),
      body: SingleChildScrollView(
        child: Column(
        crossAxisAlignment: CrossAxisAlignment.center,
        children: <Widget>[
          Container(
            margin: EdgeInsets.fromLTRB(20, 15, 10, 0),
            padding: EdgeInsets.only(bottom: 10),
            decoration: BoxDecoration(
              border: Border(
                bottom: BorderSide(
                  width: 0.5,
                  color: Colors.black26,
                ),
              )
            ),
            child: Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: <Widget>[
                Text('社团分类', style: TextStyle(fontSize: 18, fontWeight: FontWeight.w500)),
                GestureDetector(
                  child: Row(
                    children: <Widget>[
                      Text(selectedCommunityType?.text??'请选择类型', style: TextStyle(fontSize: 16, color: Colors.black45)),
                      Icon(Icons.keyboard_arrow_right, color: Colors.black45, size:24),
                    ],
                  ),
                  onTap: () {
                    _getCommunityType();
                  },
                ),
              ],
            ),
          ),
          Container(
            padding: EdgeInsets.fromLTRB(20, 10, 10, 10),
            margin: EdgeInsets.only(bottom: 20),
            decoration: BoxDecoration(
              border: Border(
                bottom: BorderSide(
                  width: 0.5,
                  color: Colors.black26,
                ),
              )
            ),
            child: Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: <Widget>[
                Text('社团模块', style: TextStyle(fontSize: 18, fontWeight: FontWeight.w500)),
                GestureDetector(
                  child: Row(
                    children: <Widget>[
                      Text(selectedCommunityModule?.title??'请选择模块', style: TextStyle(fontSize: 16, color: Colors.black45)),
                      Icon(Icons.keyboard_arrow_right, color: Colors.black45, size:24),
                    ],
                  ),
                  onTap: () {
                    _getCommunityModule();
                  },
                ),
              ],
            ),
          ),
          _addCommunityImage,
          Container(
            padding: EdgeInsets.symmetric(vertical: 20, horizontal: 20),
            child: Column(
              children: <Widget>[
                Container(
                  decoration: BoxDecoration(
                    border: Border(
                      bottom: BorderSide(
                        width: 0.5,
                        color: Colors.black26,
                      ),
                    )
                  ),
                  child: TextField(
                    controller: _communityNameControl,
                    autofocus: true,
                    maxLines: 1,
                    decoration: InputDecoration(
                      hintText: '给社团起个名字吧',
                      border: InputBorder.none,
                      hintStyle: TextStyle(
                        color: Colors.grey,
                        fontSize: 16,
                      ),
                    ),
                  ),
                ),
                TextField(
                  controller: _communityDesControl,
                  autofocus: true,
                  maxLines: 5,
                  decoration: InputDecoration(
                    hintText: '请填写社团简介',
                    hintStyle: TextStyle(
                      color: Colors.grey,
                      fontSize: 16,
                    ),
                    border: InputBorder.none,
                  ),
                ),
              ],
            ),
          ),
        ],
      ),
      ),
    );
  }

  Widget _appBar(BuildContext context) {
    return AppBar(
      title: Text('创建社团'),
      actions: <Widget>[
        IconButton(
          icon: Icon(
            Icons.save_alt,
            semanticLabel: 'search',
          ),
          onPressed: () {
            _addCommunity();
          },
        ),
      ],
    );
  }

  void _getCommunityType() {
    showModalBottomSheet(
      context: context, 
      builder: (BuildContext context) {
        return BottomPickerWidget(
          tabModelList: _tabModelList.map((data) => data.text).toList(),
          sureCallBack: communityTypeSelected,
        );
      }
    );
  }

  // 社团分类选择回调
  void communityTypeSelected(int selectedKey) {
    if (!mounted) return;
    setState(() {
      selectedCommunityType = _tabModelList[selectedKey];
    });
  }

  void _getCommunityModule() {
    showModalBottomSheet(
      context: context, 
      builder: (BuildContext context) {
        return BottomPickerWidget(
          tabModelList: _communityModelList.map((data) => data.title).toList(),
          sureCallBack: communityMoudleSelected,
        );
      }
    );
  }

  // 社团模块选择回调
  void communityMoudleSelected(int selectedKey) {
    if (!mounted) return;
    setState(() {
      selectedCommunityModule = _communityModelList[selectedKey];
    });
  }
  
  Widget get _addCommunityImage {
    if (imageObj['src'] == '' || imageObj['src'] == null) {
      return Container(
        width: MediaQuery.of(context).size.width,
        decoration: BoxDecoration(
          border: Border(
            bottom: BorderSide(
              width: 0.5,
              color: Colors.black26,
            ),
          )
        ),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.center,
          children: <Widget>[
            Container(
              width: 100,
              height: 100,
              padding: EdgeInsets.all(10),
              margin: EdgeInsets.only(bottom: 10),
              decoration: BoxDecoration(
                border: Border(
                  bottom: BorderSide(
                    width: 0.5,
                    color: Colors.black26,
                  ),
                  top: BorderSide(
                    width: 0.5,
                    color: Colors.black26,
                  ),
                  left: BorderSide(
                    width: 0.5,
                    color: Colors.black26,
                  ),
                  right: BorderSide(
                    width: 0.5,
                    color: Colors.black26,
                  ),
                )
              ),
              child: IconButton(
                icon: Icon(
                  Icons.camera_enhance,
                  semanticLabel: 'camera',
                  color: Colors.grey[300],
                  size:40
                ),
                onPressed: () {
                  getImage();
                },
              ),
            ),
            Padding(
              child: Text('社团头像', style: TextStyle(fontSize: 16)),
              padding: EdgeInsets.only(bottom: 20)
            ),
          ],
        ),
      );
    } else {
      return Container(
        padding: EdgeInsets.only(top: 20),
        width: 100,
        height: 100,
        child: Stack(
          overflow: Overflow.visible,
          children: <Widget>[
            Image.network(imageObj['src'], fit: BoxFit.cover, height: 100, width: 100,),
            Positioned(
              right: -8,
              top: -8,
              child: PhysicalModel(
                color: Colors.grey,
                clipBehavior: Clip.antiAlias,
                borderRadius: BorderRadius.circular(12),
                child: GestureDetector(
                  child: Icon(Icons.close, size: 18, color: Colors.red),
                  onTap: () {
                    setState(() {
                      imageObj = { 'src': ''};
                    });
                  },
                ),
                // IconButton(
                //   // padding: EdgeInsets.all(4.0),
                //   // iconSize: 10.0,
                //   icon: Icon(Icons.close, size: 18, color: Colors.red),
                //   onPressed: () {},
                // ),
              ),
            ),
          ],
        ),
      );
    }
  }

  // 上传图片
  Future getImage() async {
    var image = await ImagePicker.pickImage(source: ImageSource.gallery);
    var dataModel = await UploadData.uploadImage(image);
    if (!mounted) return;
    if (dataModel['code'] == 0) {
      setState(() {
        imageObj = dataModel['data'];
      });
      return;
    }
    Fluttertoast.showToast(
      gravity: ToastGravity.CENTER,
      backgroundColor: Colors.black,
      fontSize: 16,
      msg: dataModel['message'],
    );
  }

  // 社团类型
  Future<Null> _getClubTypeList() async {
    var dataModel = await CommunityService.clubTypeList();
    List tabList = dataModel['data'].map(
      (item) => TabArrItemModel(key: item['id'].toString(), text: item['name'])
    ).toList();
    if (!mounted) return;
    setState(() {
      _tabModelList = tabList;
    });
  }

  // 社团板块
  Future<Null> _getClubModelList() async {
    var dataModel = await CommunityService.communityModel();
    var communityModelList = (dataModel['data'] as List).map((item) => CommunityModel.fromJson(item)).toList();
    if (!mounted) return;
    setState(() {
      _communityModelList = communityModelList;
    });
  }

  // 添加社团
  Future<Null> _addCommunity() async {
    if (selectedCommunityType?.key == null || selectedCommunityModule?.id == null
        || _communityNameControl.text == null || _communityDesControl.text == null) {
          Fluttertoast.showToast(
            gravity: ToastGravity.CENTER,
            backgroundColor: Colors.black,
            fontSize: 16,
            msg: '请完善信息'
          );
          return;
    }
    Map<String, dynamic> dataObj = {
      'club_type': selectedCommunityType.key,
      'forum_id': selectedCommunityModule.id.toString(),
      'intro': _communityDesControl.text,
      'logo':  imageObj['path'],
      'name': _communityNameControl.text,
    };
    var dataModel = await CommunityService.addCommunity(dataObj);
    Fluttertoast.showToast(
      gravity: ToastGravity.CENTER,
      backgroundColor: Colors.black,
      fontSize: 16,
      msg: dataModel['message']
    );
    if (dataModel['code'] == 0) {
      Navigator.pushNamed(context, 'CommunityPage');
    }
  }
}