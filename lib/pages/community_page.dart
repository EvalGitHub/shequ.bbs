import 'package:flutter/material.dart';
import 'package:shequ/model/community_club_model.dart';
import 'package:shequ/model/tab_item_model.dart';
import 'package:shequ/pages/add_community.dart';
import 'package:shequ/service/community_service.dart';
import 'package:shequ/widget/loading_container.dart';
import 'package:shequ/widget/scroll_cross_tab.dart';

import 'community_detail.dart';

class CommunityPage extends StatefulWidget {
  @override
  _CommunityPage createState() => _CommunityPage();
}
class _CommunityPage extends State<CommunityPage> with AutomaticKeepAliveClientMixin {
  bool _loading = true;
  List<TabItemModel>tabModelList = [];
  Map<String, List<CommunityClubModel>>clubItem = {'1': []};
  String clubIndex = '1';

  @override
  bool get wantKeepAlive => true;

  @override 
  void initState() {
    _getClubTypeList();
    _getClubsList();
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    super.build(context);
    return Scaffold(
      backgroundColor: Color(0xfff2f2f2),
      appBar: _appBar,
      body: LoadingContainer(
        isLoading: _loading,
        child: Column(
          children: <Widget>[
            // 中部导航
            _centerTab,
            _addCommunity,
            // 社区列表
            _communityList
          ],
        ),
      ),
    );
  }

 // 顶部bar
  Widget get _appBar {
    return AppBar(
      title: Text('社区广场'),
      actions: <Widget>[
        IconButton(
          icon: Icon(
            Icons.search,
            semanticLabel: 'search',
          ),
          onPressed: () {
            Navigator.pushNamed(context, 'SearchPage');
          },
        ),
      ],
    );
  }

  // 中间的tab
  Widget get _centerTab {
    return ScrollCrossTab(
      defaultKey: 1,
      defaultBgColor: Colors.white,
      tabArr: tabModelList,
      tabCallBack: (TabItemModel item) {
        if (!mounted) return;
        setState(() {
          clubIndex = item.id.toString();
        });
      },
    );
  }

  // 添加社区按钮
  Widget get _addCommunity {
    return Container(
      height: 40,
      margin: EdgeInsets.only(bottom: 10),
      alignment: Alignment.center,
      width: MediaQuery.of(context).size.width,
      color: Colors.white,
      child: GestureDetector(
        child: Text('+创建社团', style: TextStyle(fontSize: 18),),
        onTap: () {
          _createCommunity();
        },
      ),
    );
  }

  // 社区表表
  Widget get _communityList {
    return MediaQuery.removePadding(
      context: context,
      removeTop: true,
      child: Expanded(
        flex: 1,
        child: ListView.separated(
          itemBuilder: (context, index) {
            return _createClubItem(context, index);
          }, 
          separatorBuilder: (context, index) {
            return Divider(color: Colors.black12, height: 1,);
          },
          itemCount: clubItem[clubIndex.toString()].length + 1,
        )
      )
    );
  }

  _createClubItem(BuildContext context, int index) {
    if (index == clubItem[clubIndex.toString()].length) {
      return Container(
        alignment: Alignment.center,
        padding: EdgeInsets.all(16),
        child: Text('没有更多了', style: TextStyle(color: Colors.grey),),
      );
    }
    return GestureDetector(
      child: Container(
        color: Colors.white,
        padding: EdgeInsets.symmetric(vertical: 13, horizontal: 10),
        child: Column(
          children: <Widget>[
            Row(
              mainAxisAlignment: MainAxisAlignment.start,
              // crossAxisAlignment: CrossAxisAlignment.start,
              children: <Widget>[
                Padding(
                  child: ClipOval(
                    child: Image.network(
                      clubItem[clubIndex.toString()][index].logoUrl, 
                      fit: BoxFit.cover, 
                      width: 40, 
                      height: 40,)
                  ),
                  padding: EdgeInsets.only(right: 10),
                ),
                Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: <Widget>[
                    Text(clubItem[clubIndex.toString()][index].name),
                    Text.rich(
                      TextSpan(
                        children: [
                          TextSpan(text: '共'),
                          TextSpan(text: clubItem[clubIndex.toString()][index].postCount.toString() + '帖子/'),
                          TextSpan(text: clubItem[clubIndex.toString()][index].memberCount.toString() + '人员'),
                        ] 
                      )
                    )
                  ],
                ),
              ],
            ),
          ],
        )
      ),
      onTap: () {
        _jumpToCommunityDetail(clubItem[clubIndex.toString()][index]);
      },
    );
  }

  // 跳转社团详情页
  void _jumpToCommunityDetail(CommunityClubModel item) {
    Navigator.push(
      context,
      MaterialPageRoute(builder: (BuildContext context) => CommunityDetail(clubItem: item)),
    );
  }

  // 新增社团
  void _createCommunity() {
    Navigator.push(
      context,
      MaterialPageRoute(builder: (BuildContext context) => AddCommunity()),
    );
  }

  // 社团类型
  Future<Null> _getClubTypeList() async {
    var dataModel = await CommunityService.clubTypeList();
    List<TabItemModel> _tabList = (dataModel['data'] as List).map(
      (item) => TabItemModel.fromJson(item)).toList();
    if (!mounted) return;
    setState(() {
      tabModelList = _tabList;
    });
  }

  // 社团列表
  Future<Null> _getClubsList() async {
    var dataModel = await CommunityService.clubList();
    Map<String, List<CommunityClubModel>>_clubItem = {};
    for (var i = 0; i< dataModel['data'].length; i++) {
      var _tmp = (dataModel['data'][i]['rows'] as List).map((item) => CommunityClubModel.fromJson(item)).toList();
      _clubItem[dataModel['data'][i]['id'].toString()] = _tmp;
    }
    if (!mounted) return;
    setState(() {
      _loading = false;
      clubItem = _clubItem;
    });
  }
}