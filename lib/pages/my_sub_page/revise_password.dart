import 'package:flutter/material.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:shequ/font_class/font_icons.dart';
import 'package:shequ/service/community_service.dart';

class RevisePassword extends StatefulWidget {
  @override
  _RevisePassword createState() => _RevisePassword();
}

class _RevisePassword extends State<RevisePassword> {
  final TextEditingController _originalPwdController = new TextEditingController();
  FocusNode _originalPwdFocusNode = FocusNode();
  final TextEditingController _newPwdController = new TextEditingController();
  FocusNode _newPwdFocusNode = FocusNode();
  final TextEditingController _surePwdController = new TextEditingController();
  FocusNode _surePwdFocusNode = FocusNode();
  bool _isObscureText = true;

  bool _showOriPwdBtn = false;
  bool _showNewPwdDelBtn = false;
  bool _showSurePwdDelBtn = false;

  @override 
  void initState() {
    _originalPwdFocusNode.addListener(() {
      setState(() { // 获得焦点
        _showOriPwdBtn = _originalPwdFocusNode.hasFocus && _originalPwdController.text != '' ? true : false;
      });
    });
    
    _newPwdFocusNode.addListener(() {
      setState(() { // 获得焦点
        _showNewPwdDelBtn = _newPwdFocusNode.hasFocus && _newPwdController.text != '' ? true : false;
      });
    });

    _surePwdFocusNode.addListener(() {
      setState(() { // 获得焦点
        _showSurePwdDelBtn = _surePwdFocusNode.hasFocus && _surePwdController.text != '' ? true : false;
      });
    });
    super.initState();
  }

  @override
  void dispose() {
    _originalPwdController.dispose();
    _newPwdController.dispose();
    _surePwdController.dispose();
    _originalPwdFocusNode.dispose();
    _newPwdFocusNode.dispose();
    _surePwdFocusNode.dispose();
    super.dispose();
  }
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      resizeToAvoidBottomPadding: false,
      appBar: AppBar(title: Text('修改密码')),
      body: Container(
        padding: EdgeInsets.fromLTRB(20, 40, 20, 0),
        child: Column(
          children: <Widget>[
            Stack(
              children: <Widget>[
                TextFormField(
                  autofocus: true,
                  controller: _originalPwdController,
                  obscureText: _isObscureText,
                  focusNode: _originalPwdFocusNode,
                  decoration: InputDecoration(
                    // contentPadding: EdgeInsets.only(left: 0),
                    // border: InputBorder.none, // 无下边框
                    labelText: '原密码',
                    // icon: Icon(Icons.person_outline,  size: 28)
                  ),
                ),
                Positioned(
                  right: 0,
                  bottom: 5,
                  child: _showOriPwdBtn ? GestureDetector(
                    child: Icon(Icons.close, size: 18),
                    onTap: () {
                      _originalPwdController.text = '';
                    },
                  ) : Text(''),
                ),
              ],
            ),
            Stack(
              children: <Widget>[
                TextFormField(
                  controller: _newPwdController,
                  obscureText: _isObscureText,
                  focusNode: _newPwdFocusNode,
                  decoration: InputDecoration(
                    // contentPadding: EdgeInsets.only(left: 0),
                    // border: InputBorder.none, // 无下边框
                    labelText: '新密码',
                    // prefixText: '新密码',
                  ),
                ),
                Positioned(
                  right: 0,
                  bottom: 5,
                  child: _showNewPwdDelBtn ? GestureDetector(
                    child: Icon(Icons.close, size: 18),
                    onTap: () {
                      _newPwdController.text = '';
                    },
                  ) : Text(''),
                ),
              ],
            ),
            Stack(
              children: <Widget>[
                TextFormField(
                  controller: _surePwdController,
                  obscureText: _isObscureText,
                  focusNode: _surePwdFocusNode,
                  decoration: InputDecoration(
                    // contentPadding: EdgeInsets.only(left: 0),
                    // border: InputBorder.none, // 无下边框
                    // hintText: '确认密码',
                    labelText: '确认密码'
                    // icon: Icon(Icons.person_outline,  size: 28)
                  ),
                ),
                Positioned(
                  right: 0,
                  bottom: 5,
                  child: _showSurePwdDelBtn ? GestureDetector(
                    child: Icon(Icons.close, size: 18),
                    onTap: () {
                      _surePwdController.text = '';
                    },
                  ) : Text(''),
                ),
              ],
            ),
            GestureDetector(
              child: Container(
                margin: EdgeInsets.only(top: 30),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.end,
                  children: <Widget>[
                    Padding(
                      child: Text('点击查看密码'),
                      padding: EdgeInsets.only(right: 5),
                    ),
                    Icon(MyIcon.view, size: 20)
                  ],
                ),
              ),
              onTap: () {
                setState(() {
                  _isObscureText = !_isObscureText;
                });
              },
            ),
            Container(
              width: MediaQuery.of(context).size.width,
              height: 50,
              margin: EdgeInsets.only(top: 15),
              child: RaisedButton(
                color: Colors.red,
                shape: RoundedRectangleBorder(
                  borderRadius: BorderRadius.circular(4),
                  side: BorderSide(color: Colors.red)
                ),
                onPressed: () {
                  _revisePwd();
                },  
                child: Text('提交', style: TextStyle(fontSize: 20, color: Colors.white),),
              ),
            ),
          ],
        ),
      ),
    );
  }

  _revisePwd() {
    if (_originalPwdController.text == '' || _surePwdController.text == '' || _newPwdController.text == '') {
      Fluttertoast.showToast(
        gravity: ToastGravity.CENTER,
        backgroundColor: Colors.black,
        fontSize: 16,
        msg: '请填写完整密码信息！'
      );
      return;
    }
    // 获取焦点
    // FocusScope.of(context).requestFocus(_originalPwdFocusNode); 
    
    // 失去焦点(创建一个新的元素去获取唯一的焦点)
    FocusScope.of(context).requestFocus(FocusNode());

    // 失去焦点
    // _originalPwdFocusNode.unfocus();
    // _newPwdFocusNode.unfocus();
    // _surePwdFocusNode.unfocus();
    _revisePwdAsync({
      'oldPass': _originalPwdController.text,
      'newPass': _newPwdController.text,
      'checkPass': _surePwdController.text,
    });
  }

  Future<Null> _revisePwdAsync(Map<String, dynamic>dataObj) async {
    var dataModel = await CommunityService.revisePwd(dataObj);
    Fluttertoast.showToast(
      gravity: ToastGravity.CENTER,
      backgroundColor: Colors.black,
      fontSize: 16,
      msg: dataModel['message']
    );
  }
}