import 'package:flutter/material.dart';
import 'package:shequ/model/author_info_model.dart';
import 'package:shequ/service/community_service.dart';
import 'package:shequ/widget/list_data_pull_loading.dart';
import 'package:shequ/widget/loading_container.dart';

class MyFans extends StatefulWidget {
  final int uid;

  const MyFans({Key key, this.uid}) : super(key: key);
  _MyFans createState() => _MyFans();
}

class _MyFans extends State<MyFans> {
  bool _isLoading = true;
  int _listCount = 0;
  ScrollController _listViewController = new ScrollController();
  List<AuthorInfoModel> _watcherUserList = [];
  @override 
  void initState() {
    _getWatcher();
    super.initState();
  }

  @override
  void dispose() {
    _listViewController.dispose();
    super.dispose();
  }
  
  @override 
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(title: Text('粉丝')),
      body: SafeArea(
        child: LoadingContainer(
          isLoading: _isLoading,
          child: ListView.separated(
            shrinkWrap: true, 
            separatorBuilder: (context, index) {
              return Divider(color: Colors.black12);
            },
            controller: _listViewController,
            itemCount: _watcherUserList.length + 1,
            itemBuilder: (BuildContext context, int index) {
              if (_watcherUserList.length == 0) {  // 没有数据显示加载
                return _noManyItems;
              }
              if (index >= _watcherUserList.length) { // 显示完数组最后一条数据
                if (_watcherUserList.length < _listCount) { // 但是还没加载完数据库所有数据
                  _getWatcher();
                  return ListDataPullLoading();
                } else {
                  return _noManyItems;
                }
              }
              return _createContentItem(_watcherUserList[index]);
            }
          ),
        ),
      ),
    );
  }

  Widget _createContentItem(AuthorInfoModel item) {
    return Container(
      padding: EdgeInsets.fromLTRB(15, 5, 15, 0),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        crossAxisAlignment: CrossAxisAlignment.center,
        children: <Widget>[
          Row(
            children: <Widget>[
              ClipOval(
                child: Image.network(
                  item.avatar32,
                  fit: BoxFit.cover, 
                  width: 40, 
                  height: 40,
                ),
              ),
              Padding(
                child: Text(item.nickname),
                padding: EdgeInsets.symmetric(horizontal: 10)
              ),
              Image.network(item.level.icon, width: 40, height: 16),
            ],
          ),
        ],
      ),
    );
  }

  // 没有更多
  Widget get _noManyItems {
    return Container(
      alignment: Alignment.center,
      padding: EdgeInsets.all(16),
      child: Text('没有更多了', style: TextStyle(color: Colors.grey),),
    );
  }

  Future<Null> _getWatcher() async {
    var dataModel = await CommunityService.getWatcher({
      'type': 'fans',
      'uid': widget.uid,
    });
    List<AuthorInfoModel> _tmpList = (dataModel['data']['users'] as List).map((item) => AuthorInfoModel.fromJson(item)).toList();
    setState(() {
      _watcherUserList.addAll(_tmpList);
      _listCount = dataModel['data']['users'].length;
      _isLoading = false;
    });
  }
}