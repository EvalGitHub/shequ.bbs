import 'package:flutter/material.dart';

class MyCommunity extends StatefulWidget {
  final int uid;
  const MyCommunity({Key key, this.uid}) : super(key: key);
  @override 
  _MyCommunity createState() => _MyCommunity();
}

class _MyCommunity extends State<MyCommunity> with SingleTickerProviderStateMixin {
  TabController _tabController;

  @override 
  void initState() {
    _tabController = new TabController(initialIndex: 0, length: 2, vsync: this);
    super.initState();
  }

  @override
  void dispose() {
    _tabController.dispose();
    super.dispose();
  }

  @override 
  Widget build(BuildContext build) {
    return Scaffold(
      appBar: AppBar(
        title: Text('我的社团'),
        bottom: TabBar(
          tabs: <Widget>[
            new Tab(
              child: Text('我加入的', style: TextStyle(fontSize: 16),)
            ),
            new Tab(
              child: Text('我创建的', style: TextStyle(fontSize: 16),)
            ),
          ],
          controller: _tabController,
          onTap: (index) {
            print(index);
          },
        ),
      ),
      body: new TabBarView(
        controller: _tabController,
        children: <Widget>[
          _noManyItems,
          _noManyItems,
        ],
      ),
    );
  }

  Widget get _noManyItems {
    return Container(
      alignment: Alignment.center,
      padding: EdgeInsets.all(16),
      child: Text('没有更多了', style: TextStyle(color: Colors.grey),),
    );
  }
}