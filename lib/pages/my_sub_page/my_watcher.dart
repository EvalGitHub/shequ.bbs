import 'package:flutter/material.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:shequ/font_class/font_icons.dart';
import 'package:shequ/model/author_info_model.dart';
import 'package:shequ/service/community_service.dart';
import 'package:shequ/widget/list_data_pull_loading.dart';
import 'package:shequ/widget/loading_container.dart';

class MyWatcher extends StatefulWidget {
  final int uid;

  const MyWatcher({Key key, this.uid}) : super(key: key);
  _MyWatcher createState() => _MyWatcher();
}

class _MyWatcher extends State<MyWatcher> {
  bool _isLoading = true;
  int _listCount = 0;
  ScrollController _listViewController = new ScrollController();
  List<AuthorInfoModel> _watcherUserList = [];
  @override 
  void initState() {
    _getWatcher();
    super.initState();
  }

  @override
  void dispose() {
    _listViewController.dispose();
    super.dispose();
  }
  
  @override 
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(title: Text('关注')),
      body: SafeArea(
        child: LoadingContainer(
          isLoading: _isLoading,
          child: ListView.separated(
            shrinkWrap: true, 
            separatorBuilder: (context, index) {
              return Divider(color: Colors.black12);
            },
            controller: _listViewController,
            itemCount: _watcherUserList.length + 1,
            itemBuilder: (BuildContext context, int index) {
              if (_watcherUserList.length == 0) {  // 没有数据显示加载
                return _noManyItems;
              }
              if (index >= _watcherUserList.length) { // 显示完数组最后一条数据
                if (_watcherUserList.length < _listCount) { // 但是还没加载完数据库所有数据
                  _getWatcher();
                  return ListDataPullLoading();
                } else {
                  return _noManyItems;
                }
              }
              return _createContentItem(_watcherUserList[index]);
            }
          ),
        ),
      ),
    );
  }

  Widget _createContentItem(AuthorInfoModel item) {
    return Container(
      padding: EdgeInsets.fromLTRB(15, 5, 15, 0),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        crossAxisAlignment: CrossAxisAlignment.center,
        children: <Widget>[
          Row(
            children: <Widget>[
              ClipOval(
                child: Image.network(
                  item.avatar32,
                  fit: BoxFit.cover, 
                  width: 40, 
                  height: 40,
                ),
              ),
              Padding(
                child: Text(item.nickname),
                padding: EdgeInsets.symmetric(horizontal: 10)
              ),
              Image.network(item.level.icon, width: 40, height: 16),
            ],
          ),
          GestureDetector(
            onTap: () {
              _cancelOrAddFollow(item);
            },
            child: Column(
              children: <Widget>[
                item.followStatus == 1 ? Icon(MyIcon.person_done_outline) : Icon(MyIcon.person_add_outline, color: Colors.blue,),
                Padding(
                  child: item.followStatus == 1 ? Text('已关注') : Text('加关注', style: TextStyle(color: Colors.blue)),
                  padding: EdgeInsets.only(top: 5),
                ),
              ],
            ),
          ),
        ],
      ),
    );
  }

  // 没有更多
  Widget get _noManyItems {
    return Container(
      alignment: Alignment.center,
      padding: EdgeInsets.all(16),
      child: Text('没有更多了', style: TextStyle(color: Colors.grey),),
    );
  }

  Future<Null> _cancelOrAddFollow(AuthorInfoModel item) async {
    var dataModel = await CommunityService.cancelOrAddFollow({
      'uid': item.uid.toString(),
    });
    print(dataModel);
    Fluttertoast.showToast(
      msg: dataModel['message'],
      gravity: ToastGravity.CENTER,
      backgroundColor: Colors.black,
      fontSize: 16,
    );
    item.followStatus = item.followStatus == 1 ? 0 : 1;
    setState(() {});
  }

  Future<Null> _getWatcher() async {
    var dataModel = await CommunityService.getWatcher({
      'type': 'follow',
      'uid': widget.uid,
    });
    List<AuthorInfoModel> _tmpList = (dataModel['data']['users'] as List).map((item) => AuthorInfoModel.fromJson(item)).toList();
    setState(() {
      _watcherUserList.addAll(_tmpList);
      _listCount = dataModel['data']['users'].length;
      _isLoading = false;
    });
  }
}