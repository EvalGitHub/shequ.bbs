import 'package:flutter/material.dart';
import 'package:shequ/model/reply_model.dart';
import 'package:shequ/pages/view_datail.dart';
import 'package:shequ/service/community_service.dart';
import 'package:shequ/widget/list_data_pull_loading.dart';

class MyReply extends StatefulWidget {
  final int uid;
  const MyReply({Key key, this.uid}) : super(key: key);
  @override 
  _MyReply createState() => _MyReply();
}

class _MyReply extends State<MyReply> {
  ScrollController _listViewController = new ScrollController();
  List<ReplyModel> _replyList = [];
  bool _hasLoadingList = false;
  int _currentPageNumber = 1;
  int _replyTotalCount;
  @override 
  void initState() {
    _getAllReply();
    super.initState();
  }

  @override
  void dispose() {
    _listViewController.dispose();
    super.dispose();
  }

  @override 
  Widget build(BuildContext build) {
    return Scaffold(
      appBar: AppBar(title: Text('回复')),
      body: Container(
        child: ListView.separated(
          shrinkWrap: true, 
          controller: _listViewController,
          itemBuilder: (context, index) {
            if (_replyList.length == 0 && _hasLoadingList == true) { // 没有数据显示加载
              return ListDataPullLoading();
            }
            if (index == _replyList.length && _hasLoadingList == false) {
              return _noManyItems;
            }
            if (_replyList.length > _replyTotalCount && index == _replyList.length) {
              _currentPageNumber +=1;
              _getAllReply();
            }
            return _listItem(_replyList[index]);
          }, 
          separatorBuilder: (context, index) {
            return Divider(color: Colors.black12, height: 1,);
          }, 
          itemCount: _replyList.length + 1,
        ),
      ),
    );
  }

  Widget _listItem(ReplyModel item) {
    return GestureDetector(
      onTap: () {
        Navigator.push(
          context,
          MaterialPageRoute(builder: (BuildContext context) => ViewDetail(articleId: item.postId.toString())),
        );
      },
      child: Container(
        padding: EdgeInsets.symmetric(horizontal: 20, vertical: 10),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          mainAxisAlignment: MainAxisAlignment.center,
          children: <Widget>[
            Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: <Widget>[
                Text.rich(TextSpan(children: [
                  TextSpan(
                    text: '来自：', style: TextStyle(color: Colors.grey[500])
                  ),
                  TextSpan(text: item.forumTitle),
                ])),
                Container(
                  child: Text(item.createTime, style: TextStyle(color: Colors.grey[600])),
                ),
              ],
            ),
            Padding(
              padding: EdgeInsets.symmetric(vertical: 10),
              child: Text('回复内容：' + item.content),
            ),
            Container(
              alignment: Alignment.centerLeft,
              height: 25,
              width: MediaQuery.of(context).size.width,
              padding: EdgeInsets.symmetric(vertical: 2, horizontal: 5),
              color: Colors.grey[200],
              child: Text(item.postTitle, maxLines: 1, overflow: TextOverflow.ellipsis),
            ),
          ],
        ),
      )
    );
  }

  Widget get _noManyItems {
    return Container(
      alignment: Alignment.center,
      padding: EdgeInsets.all(16),
      child: Text('没有更多了', style: TextStyle(color: Colors.grey),),
    );
  }

  // 获取所有帖子
  Future<Null> _getAllReply() async {
    setState(() {
      _hasLoadingList = true;
    });
    Map<String, dynamic> postData = {
      'uid': widget.uid.toString(),
      'limit': '10',
      'page': _currentPageNumber.toString(),
    };
    var dataModel = await CommunityService.getAllReply(postData);
    var _temList = (dataModel['data']['replys'] as List).map((item) => ReplyModel.fromJson(item)).toList();
    setState(() {
      _replyTotalCount = dataModel['data']['total'];
      _hasLoadingList = false;
      _replyList.insertAll(_replyList.length, _temList);
    });
  }
}