import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:shequ/font_class/font_icons.dart';
import 'package:shequ/model/blog_model.dart';
import 'package:shequ/pages/view_datail.dart';
import 'package:shequ/service/community_service.dart';
import 'package:shequ/utils/confirm_model.dart';
import 'package:shequ/widget/list_data_pull_loading.dart';
import 'package:shequ/widget/loading_container.dart';
class MyViewRecord extends StatefulWidget {
  final int uid;
  const MyViewRecord({Key key, this.uid}) : super(key: key);
  @override 
  _MyViewRecord createState() => _MyViewRecord();
}
class _MyViewRecord extends State<MyViewRecord> {
  static const int limit = 10;
  int page = 1;
  bool _isLoading = true;
  int _viewRecordCount = 0;
  List<BlogModel> _viewRecordlist = [];
  ScrollController _listViewController = new ScrollController();
  @override 
  initState() {
    _getViewRecord({
      'limit': limit,
      'page': 1
    });
    super.initState();
  }

  @override
  dispose() {
    _listViewController.dispose();
    super.dispose();
  }

  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('浏览记录'),
        actions: <Widget>[
          IconButton(
            icon: Icon(
              Icons.delete,
              semanticLabel: '清空记录',
            ),
            onPressed: () {
              if (_viewRecordCount == 0) {
                Fluttertoast.showToast(
                  gravity: ToastGravity.CENTER,
                  backgroundColor: Colors.black,
                  fontSize: 16,
                  msg: '暂无待删除记录！'
                );
                return;
              }
              _showDialog();
            },
          ),
        ],
      ),
      body: SafeArea(
        child: LoadingContainer(
          isLoading: _isLoading,
          child: ListView.separated(
            shrinkWrap: true, 
            separatorBuilder: (context, index) {
              return Divider(color: Colors.black12);
            },
            controller: _listViewController,
            itemCount: _viewRecordlist.length + 1,
            itemBuilder: (BuildContext context, int index) {
              if (_viewRecordlist.length == 0) {  // 没有数据显示加载
                return _noManyItems;
              }
              if (index >= _viewRecordlist.length) { // 显示完数组最后一条数据
                if (_viewRecordlist.length < _viewRecordCount) { // 但是还没加载完数据库所有数据
                  _getViewRecord({
                    'limit': limit,
                    'page': ++page,
                  });
                  return ListDataPullLoading();
                } else {
                  return _noManyItems;
                }
              }
              return _createContentItem(_viewRecordlist[index]);
            }
          ),
        ),
      ),
    );
  }

  // 没有更多
  Widget get _noManyItems {
    return Container(
      alignment: Alignment.center,
      padding: EdgeInsets.all(16),
      child: Text('没有更多了', style: TextStyle(color: Colors.grey),),
    );
  }

  // 创建内容item
  Widget _createContentItem(BlogModel item) {
    return GestureDetector(
      onTap: () {
        Navigator.push(
          context,
          MaterialPageRoute(builder: (BuildContext context) => ViewDetail(articleId: item.id.toString())),
        );
      },
      child: Container(
        padding: EdgeInsets.symmetric(horizontal: 20, vertical: 10),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: <Widget>[
            Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: <Widget>[
                Text.rich(
                  TextSpan(
                    children: [
                      TextSpan(text: '来自  ', style: TextStyle(color: Colors.grey[500])),
                      TextSpan(text: item.forumTitle + '  '),
                    ]
                  )
                ),
                Text(item.createTime, style: TextStyle(color: Colors.grey[600])),
              ],
            ),
            Padding(
              padding: EdgeInsets.symmetric(vertical: 10),
              child: Text(item.title, style: TextStyle(fontSize: 16)),
            ),
            Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: <Widget>[
                Row(
                  children: <Widget>[
                    Text(item.viewCount.toString(), style: TextStyle(color: Colors.black54)),
                    Padding(
                      child: Text('阅读', style: TextStyle(color: Colors.black54)),
                      padding: EdgeInsets.symmetric(horizontal: 5),
                    ),
                  ],
                ),
                Row(
                  children: <Widget>[
                    Icon(MyIcon.msg, size: 16),
                    Text(' ' + item.replyCount.toString(), style: TextStyle(color: Colors.black54)),
                  ],
                ),
              ],
            ),
          ],
        ),
      )
    );
  }

  // confirm modal
  Future<void> _showDialog() async {
    return ConfirmModelUtil.confirmModelFun(context, {
      'tipNav': '提示',
      'tipContent': '确认删除所有浏览记录？',
      'cancelMap': {
        'text': '取消',
        'callBack': null,
      },
      'sureMap': {
        'text':'确定',
        'callBack': () => _deleteViewRecord(0),
      }
    });
  }

  Future<Null> _deleteViewRecord(int postId) async {
    var dataModel = await CommunityService.deleteViewRecord(postId);
    if (dataModel['code'] == 0) {
      Fluttertoast.showToast(
        gravity: ToastGravity.CENTER,
        backgroundColor: Colors.black,
        fontSize: 16,
        msg: dataModel['message']
      );
    } 
    setState(() {
      _viewRecordlist = [];
      _viewRecordCount = 0;
    });
  }

  // 获取浏览记录
  Future<Null> _getViewRecord(Map<String, dynamic>dataObj) async {
    var dataModel = await CommunityService.getViewRecord({
      'limit': dataObj['limit'],
      'page': dataObj['page'],
      'uid': widget.uid,
    });
    List<BlogModel> _tmpList = (dataModel['data']['rows'] as List).map((item) => BlogModel.fromJson(item)).toList();
    // _viewRecordlist.insertAll(_viewRecordlist.length, _tmpList);
    setState(() {
      _viewRecordCount = dataModel['data']['total'];
      _isLoading = false;
      _viewRecordlist.addAll(_tmpList);
    });
  }
}