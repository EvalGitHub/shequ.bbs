import 'package:flutter/material.dart';
import 'package:shequ/font_class/font_icons.dart';
import 'package:shequ/model/post_detail_model.dart';
import 'package:shequ/pages/view_datail.dart';
import 'package:shequ/service/community_service.dart';
import 'package:shequ/widget/list_data_pull_loading.dart';

class MyPost extends StatefulWidget {
  final int uid;
  const MyPost({Key key, this.uid}) : super(key: key);
  @override 
  _MyPost createState() => _MyPost();
}

class _MyPost extends State<MyPost> {
  ScrollController _listViewController = new ScrollController();
  List<PostDetailModel> _postList = [];
  bool _hasLoadingList = false;
  int _currentPageNumber = 1;
  int _postTotalCount;
  @override 
  void initState() {
    _getAllPost();
    super.initState();
  }

  @override
  void dispose() {
    _listViewController.dispose();
    super.dispose();
  }

  @override 
  Widget build(BuildContext build) {
    return Scaffold(
      appBar: AppBar(title: Text('所有帖子')),
      body: Container(
        child: ListView.separated(
          shrinkWrap: true, 
          controller: _listViewController,
          itemBuilder: (context, index) {
            if (_postList.length == 0 && _hasLoadingList == true) { // 没有数据显示加载
              return ListDataPullLoading();
            }
            if (index == _postList.length && _hasLoadingList == false) {
              return _noManyItems;
            }
            if (_postList.length > _postTotalCount && index == _postList.length) {
              _currentPageNumber +=1;
              _getAllPost();
            }
            return _postItem(_postList[index]);
          }, 
          separatorBuilder: (context, index) {
            return Divider(color: Colors.black12, height: 1,);
          }, 
          itemCount: _postList.length + 1,
        ),
      ),
    );
  }

  Widget _postItem(PostDetailModel item) {
    return GestureDetector(
      onTap: () {
        Navigator.push(
          context,
          MaterialPageRoute(builder: (BuildContext context) => ViewDetail(articleId: item.id.toString())),
        ).then((val) => val == true ? _refresh() : print('没做处理'));
      },
      child: Container(
        padding: EdgeInsets.symmetric(horizontal: 20, vertical: 10),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: <Widget>[
            Text.rich(
              TextSpan(
                children: [
                  TextSpan(text: '来自  ', style: TextStyle(color: Colors.grey[500])),
                  TextSpan(text: item.forumTitle + '  '),
                  TextSpan(text: item.createTime, style: TextStyle(color: Colors.grey[600])),
                ]
              )
            ),
            Padding(
              padding: EdgeInsets.symmetric(vertical: 10),
              child: Text(item.content),
            ),
            Row(
              children: <Widget>[
                Text(item.viewCount.toString(), style: TextStyle(color: Colors.black54)),
                Padding(
                  child: Text('阅读'),
                  padding: EdgeInsets.symmetric(horizontal: 10),
                ),
                Icon(MyIcon.msg, size: 16),
                Text(' ' + item.replyCount.toString()),
              ],
            ),
          ],
        ),
      )
    );
  }

  Widget get _noManyItems {
    return Container(
      alignment: Alignment.center,
      padding: EdgeInsets.all(16),
      child: Text('没有更多帖子了', style: TextStyle(color: Colors.grey),),
    );
  }

  Future<void> _refresh() async {
    _postList = [];
    Future.delayed(const Duration(microseconds: 500), () {
      _getAllPost();
    });
  }

  // 获取所有帖子
  Future<Null> _getAllPost() async {
    setState(() {
      _hasLoadingList = true;
    });
    Map<String, dynamic> postData = {
      'uid': widget.uid.toString(),
      'limit': '10',
      'page': _currentPageNumber.toString(),
    };
    var dataModel = await CommunityService.getAllPost(postData);
    var _temPostList = (dataModel['data']['posts'] as List).map((item) => PostDetailModel.fromJson(item)).toList();
    setState(() {
      _postTotalCount = dataModel['data']['total'];
      _hasLoadingList = false;
      _postList.insertAll(_postList.length, _temPostList);
    });
  }
}