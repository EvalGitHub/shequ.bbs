import 'package:flutter/material.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:image_picker/image_picker.dart';
import 'package:shequ/font_class/font_icons.dart';
import 'package:shequ/model/community_model.dart';
import 'package:shequ/service/community_service.dart';
import 'package:shequ/service/work_service.dart';
import 'package:shequ/utils/upload_image.dart';
import 'package:shequ/widget/bottom_picker.dart';


class AddPost extends StatefulWidget {
  @override 
  _AddPost createState() => _AddPost();
}

class _AddPost extends State<AddPost> {
  List<CommunityModel> _communityModelList = [];
  List<Map<String, dynamic>> imageList = [];
  CommunityModel selectedCommunityModule;
  final _titleTextCtrl = new TextEditingController();
  final _contentTextCtrl = new TextEditingController();
  final _listViewController =  new ScrollController();
  String _imgContent = '';
  @override 
  void initState() {
    _getClubModelList();
    super.initState();
  }

  @override
  void dispose() {
    _listViewController.dispose();
    _titleTextCtrl.dispose();
    _contentTextCtrl.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      resizeToAvoidBottomPadding: false,
      appBar: AppBar(title: Text('发布帖子')),
      body: SafeArea(
        child: Container(
          child: Column(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: <Widget>[
                Column(
                  children: <Widget>[
                    _communityModule,
                    _getWordContent,
                    _imageContent,
                    _actionBtnList,
                  ],
                ),
                Container(
                  height: 40,
                  decoration: BoxDecoration(
                    boxShadow: [
                      BoxShadow(
                        color: Color(0xf2f2f2), 
                        blurRadius: 5.0,
                      ),
                    ],
                    border: Border(
                      top: BorderSide(
                        width: 0.5,
                        color: Colors.black12,
                      ),
                  )
                  ),
                  child: GestureDetector(
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: <Widget>[
                        Icon(MyIcon.release_airplane, size: 24, color: Color(0x9b9b9b00),),
                        Text('  发布', style: TextStyle(fontSize: 16, color:Color(0x9b9b9b00)),),
                      ],
                    ),
                    onTap: () {
                      _releasePost();
                    }
                  ),
                ),
              ],
            ),
        ),
      ),
    );
  }

  // 输入标题
  Widget get _getWordContent {
    return Column(
      mainAxisAlignment: MainAxisAlignment.start,
      children: <Widget>[
        Container(
          decoration: BoxDecoration(
            border: Border(
              bottom: BorderSide(
                width: 0.5,
                color: Colors.black26,
              ),
            )
          ),
          height: 60,
          padding: EdgeInsets.fromLTRB(0, 0, 20, 5),
          margin: EdgeInsets.fromLTRB(20, 0, 0, 0),
          child: TextFormField(
            controller: _titleTextCtrl,
            maxLength: 30,
            maxLengthEnforced: true,
            decoration: InputDecoration(
              hintText: '请输入标题',
              border: InputBorder.none, 
            ),
          ),
        ),
        Container(
          height: 130,
          margin: EdgeInsets.fromLTRB(20, 0, 0, 0),
          padding: EdgeInsets.fromLTRB(0, 5, 20, 0),
          child: TextFormField(
            controller: _contentTextCtrl,
            maxLines: 5,
            decoration: InputDecoration(
              hintText: '请输入内容',
              border: InputBorder.none, 
            ),
          ),
        )
      ],
    );
  }

  // 图片内容展示
  Widget get _imageContent {
    return Container(
      height: 120,
      padding: EdgeInsets.fromLTRB(15, 0, 15, 10),
      child: imageList.length > 0 ? GridView.count(
        controller: _listViewController,
        shrinkWrap: true,
        crossAxisCount: 4,
        mainAxisSpacing: 0,
        crossAxisSpacing: 10,
        children: _showImage
      ) : Text(''),
    );
  }

  // 操作按钮列表
  Widget get _actionBtnList {
    return Container(
      margin: EdgeInsets.fromLTRB(20, 0, 0, 0),
      padding: EdgeInsets.fromLTRB(0, 0, 15, 10),
      decoration: BoxDecoration(
        border: Border(
          bottom: BorderSide(
            width: 0.5,
            color: Colors.black26,
          ),
        )
      ),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: <Widget>[
          Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: <Widget>[
              Padding(
                padding: EdgeInsets.only(right: 10),
                child: Icon(MyIcon.emoji, size: 25, color: Colors.black38), 
              ),
              Padding(
                padding: EdgeInsets.only(right: 20),
                child: GestureDetector(
                  child: Icon(MyIcon.picture, size: 24, color: Colors.black26,), 
                  onTap: () {
                    getImage();
                  }
                ),
              ),
            ],
          ),
          Text('最少20个字', style: TextStyle(color: Colors.grey),),
        ],
      ),
    );
  }

  // 社团模块
  Widget get _communityModule {
    return Container(
      padding: EdgeInsets.fromLTRB(20, 10, 10, 10),
      margin: EdgeInsets.only(bottom: 20),
      decoration: BoxDecoration(
        border: Border(
          bottom: BorderSide(
            width: 0.5,
            color: Colors.black26,
          ),
        )
      ),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: <Widget>[
          Text.rich(
            TextSpan(
              children: [
                TextSpan(text: '选择模块', style: TextStyle(fontSize: 18,)),
                TextSpan(text: '*', style: TextStyle(color: Colors.red)),
              ]
            ),
          ),
          GestureDetector(
            child: Row(
              children: <Widget>[
                Padding(
                  child: selectedCommunityModule?.logo != null ? Image.network(selectedCommunityModule?.logo, width: 40, height: 40, fit: BoxFit.cover) : Text(''),
                  padding: EdgeInsets.only(right: 10),
                ),
                Text(selectedCommunityModule?.title??'请选择模块', style: TextStyle(fontSize: 18, color: Colors.black45)),
                Icon(Icons.keyboard_arrow_right, color: Colors.black45, size:24),
              ],
            ),
            onTap: () {
              _getCommunityModule();
            },
          ),
        ],
      ),
    );
  }
  
  void _getCommunityModule() {
    showModalBottomSheet(
      context: context, 
      builder: (BuildContext context) {
        return BottomPickerWidget(
          tabModelList: _communityModelList.map((data) => data.title).toList(),
          sureCallBack: communityMoudleSelected,
        );
      }
    );
  }

  // 社团模块选择回调
  void communityMoudleSelected(int selectedKey) {
    if (!mounted) return;
    setState(() {
      selectedCommunityModule = _communityModelList[selectedKey];
    });
  }

  // 社团板块
  Future<Null> _getClubModelList() async {
    var dataModel = await CommunityService.communityModel();
    var communityModelList = (dataModel['data'] as List).map((item) => CommunityModel.fromJson(item)).toList();
    if (!mounted) return;
    setState(() {
      _communityModelList = communityModelList;
      selectedCommunityModule = communityModelList[0];
    });
  }

  // 上传图
  Future getImage() async {
    var image = await ImagePicker.pickImage(source: ImageSource.gallery);
    var dataModel = await UploadData.uploadImage(image);
    if (!mounted || dataModel == null) return;
    if (dataModel['code'] == 0) {
      setState(() {
        imageList.add(dataModel['data']);
      });
      return;
    } 
    Fluttertoast.showToast(
      gravity: ToastGravity.CENTER,
      backgroundColor: Colors.black,
      fontSize: 16,
      msg: dataModel['message'],
    );
  }

  // image list
  List<Widget> get _showImage {
    return imageList.map(
      (item) => Container(
        padding: EdgeInsets.fromLTRB(0, 10, 5, 0),
        margin: EdgeInsets.only(bottom: 15),
        child: Stack(
          overflow: Overflow.visible,
          children: <Widget>[
            // Image.network(item['src'], fit: BoxFit.fill),

            Image.network(item['src'], height: 120, width: (MediaQuery.of(context).size.width - 50) / 4, fit: BoxFit.cover),
            Positioned(
              top: -10,
              right: -5,
              child: GestureDetector(
                child: Container(
                  child: Icon(Icons.close, color: Colors.white, size:24),
                  decoration: BoxDecoration(
                    color: Colors.red,
                    borderRadius: BorderRadius.circular(13),
                  ),
                ),
                onTap: () {
                  _deleteUploadImage(item['image_id']);
                },
              ),
            ),
          ],
        ) 
      )
    ).toList();
  }

   // 删除图片
  _deleteUploadImage(String imageId) {
    List<Map<String, dynamic>> _tmpList = []; 
    imageList.forEach((item) {
      if (item['image_id'] != imageId) {
        _tmpList.add(item);
      }
    });
    setState(() {
      imageList = _tmpList;
    });
  }

  _releasePost() {
    if(_titleTextCtrl.text == '' || _titleTextCtrl.text == null) {
      Fluttertoast.showToast(
        gravity: ToastGravity.CENTER,
        backgroundColor: Colors.black,
        fontSize: 16,
        msg: '标题不能为空！',
      );
      return;
    }
    if(_contentTextCtrl.text.length < 20) {
      Fluttertoast.showToast(
        gravity: ToastGravity.CENTER,
        backgroundColor: Colors.black,
        fontSize: 16,
        msg: '内容不少于20个字！',
      );
      return;
    }
    _addPost();
  }

  Future<Null> _addPost() async {
    if (imageList.length > 0) {
      imageList.forEach((item) {
        _imgContent += '<p><img src="${item['src']}"></p>';
      });
    }
    Map<String, dynamic> dataObj = {
      'anon_name': '',
      'title': _titleTextCtrl.text,
      'content': '<p>${_contentTextCtrl.text}</p>' + _imgContent,
      'type': '0',
      'poll_image': '[]',
      'poll_option': '[]', 
      'is_anon': '0', // 是（1）否（0）匿名
      'f': selectedCommunityModule.id.toString(),
    };
    var dataModel = await WorkService.addPost(dataObj);
    if (dataModel == null) return;
    Fluttertoast.showToast(
      gravity: ToastGravity.CENTER,
      backgroundColor: Colors.black,
      fontSize: 16,
      msg: dataModel['message'],
    );
    if (dataModel['code'] == 0) {
      Future.delayed(const Duration(microseconds: 500), () {
        Navigator.of(context).pop(true); // 返回上一个页面
      });
    } 
  }
}