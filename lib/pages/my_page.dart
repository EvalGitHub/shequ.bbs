import 'dart:ui';

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:shequ/font_class/font_icons.dart';
import 'package:shequ/pages/my_sub_page/my_community.dart';
import 'package:shequ/pages/my_sub_page/my_post.dart';
import 'package:shequ/pages/my_sub_page/my_reply.dart';
import 'package:shequ/pages/my_sub_page/my_watcher.dart';
import 'package:shequ/service/login_service.dart';
import 'package:shequ/utils/confirm_model.dart';
import 'package:shequ/utils/storage_util.dart';

import 'my_sub_page/my_collection.dart';
import 'my_sub_page/my_fans.dart';
import 'my_sub_page/my_view_record.dart';
import 'my_sub_page/revise_password.dart';

class MyPage extends StatefulWidget {
  @override 
  _MyPage createState() => _MyPage();
}
class _MyPage extends State<MyPage> with AutomaticKeepAliveClientMixin {
  int _uid;
  String _username = '';
  String _avatar = 'https://bbs.codemao.cn/src/static/img/default_avatar_64_64.jpg';
  List<Map<String, dynamic>> _actionList = [
    { 'actionText': '浏览记录', 'targetPage': 'MyViewRecord',},
    { 'actionText': '我的关注', 'targetPage': 'MyWatcher'},
    { 'actionText': '我的粉丝', 'targetPage': 'MyFans'},
    { 'actionText': '我的收藏', 'targetPage': 'MyCollection'},
    // { 'actionText': '意见反馈', 'targetPage': ''},
    { 'actionText': '修改密码', 'targetPage': 'RevisePassword'},
    { 'actionText': '退出登录', 'targetPage': 'LoginOut'},
  ];

  @override 
  void initState() {
    getAvatarAndUid();
    super.initState();
  }

  @override
  bool get wantKeepAlive => true;

  void getAvatarAndUid() async {
    String uid = await StorageUtil.getStringItem('uid');
    String avatar = await StorageUtil.getStringItem('avatar');
    String username = await StorageUtil.getStringItem('username');
    if (!mounted) return;
    setState(() {
      _uid = int.parse(uid??'11243');
      _avatar = avatar??'https://bbs.codemao.cn/src/static/img/default_avatar_64_64.jpg';
      _username = username;
    });
  }

  @override
  Widget build(BuildContext context) {
    super.build(context);
    return Scaffold(
      appBar: AppBar(
        title: Text('个人主页'),
      ),
      body: Container(
        child: ListView(
          children: <Widget>[
            _userInfo(context),
            _userMessage(context),
            _actionItem,
          ],
        ),
      ),
    );
  }

  // 用户头像信息
  Widget _userInfo(BuildContext context) {
    return Container(
        height: 180,
        child: Stack(
          children: <Widget>[
            Image.network(
              _avatar,
              width: MediaQuery.of(context).size.width,
              height: 180,
              fit: BoxFit.cover,
            ),
            BackdropFilter(
              filter: new ImageFilter.blur(sigmaX: 8, sigmaY: 8),
              child: new Container(
                color: Colors.white.withOpacity(0.1),
                width: MediaQuery.of(context).size.width,
                height: 180,
              ),
            ),
            Positioned(
              top: 40,
              left: MediaQuery.of(context).size.width / 2 - 50, 
              child: Column(
                children: <Widget>[
                  ClipOval(
                    child: Image.network(
                      _avatar,
                      width: 70,
                      height: 70,
                      fit: BoxFit.fill,
                    ),
                  ),
                  Padding(
                    child: Text(_username, style: TextStyle(fontSize: 18)),
                    padding: EdgeInsets.only(top: 20),
                  ),
                ],
              ),
            ),
          ],
        ),
      );
  }

  // 我的帖子，回复，社团
  Widget _userMessage(BuildContext context) {
    return Container(
      height: 80,
      width: MediaQuery.of(context).size.width,
      decoration: BoxDecoration(
        border: Border(
          bottom: BorderSide(
            width: 0.8,
            color: Colors.black12,
          ),
        )
      ),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.center,
        crossAxisAlignment: CrossAxisAlignment.center,
        children: <Widget>[
          Expanded(
            flex: 1,
            child: GestureDetector(
              onTap: () {
                Navigator.push(
                  context,
                  MaterialPageRoute(builder: (BuildContext context) => MyPost(uid: _uid)),
                );
              },
              child: Column(
                mainAxisAlignment: MainAxisAlignment.center,
                children: <Widget>[
                  Icon(
                    Icons.mode_edit,
                    color: Colors.black45,
                    size: 20
                  ),
                  Padding(
                    child: Text('我的帖子', style: TextStyle(fontSize: 16)),
                    padding: EdgeInsets.only(top: 10), 
                  ),
                ],
              )
            ),
          ),
          Expanded(
            flex: 1,
            child: Container(
              decoration: BoxDecoration(
                border: Border(
                  left: BorderSide(
                    width: 0.8,
                    color: Colors.black12,
                  ),
                  right: BorderSide(
                    width: 0.8,
                    color: Colors.black12,
                  ),
                )
              ),
              child: GestureDetector(
                onTap: () {
                  Navigator.push(
                    context,
                    MaterialPageRoute(builder: (BuildContext context) => MyReply(uid: _uid)),
                  );
                },
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: <Widget>[
                    Icon(
                      MyIcon.msg,
                      color: Colors.black87,
                      size: 25
                    ),
                    Padding(
                      child: Text('我的回复', style: TextStyle(fontSize: 16)),
                      padding: EdgeInsets.only(top: 10), 
                    ),
                  ],
                )
              )
            ),
          ),
          Expanded(
            flex: 1,
            child: GestureDetector(
              onTap: () {
                Navigator.push(
                  context,
                  MaterialPageRoute(builder: (BuildContext context) => MyCommunity(uid: _uid)),
                );
              },
              child: Column(
                mainAxisAlignment: MainAxisAlignment.center,
                children: <Widget>[
                  Icon(
                    Icons.group, color: Colors.black45,
                    size: 25
                  ),
                  Padding(
                    child: Text('我的社团', style: TextStyle(fontSize: 16)),
                    padding: EdgeInsets.only(top: 10), 
                  ),
                ],
              )
            ),
          ),
        ],
      )
    );
  }

  // 操作item 
  Widget get _actionItem {
    return Container(
      margin: EdgeInsets.fromLTRB(0, 20, 0, 40),
      decoration: BoxDecoration(
        border: Border(
          top: BorderSide(
            width: 0.8,
            color: Colors.black12,
          ),
        )
      ),
      child: Column(
        children: _createActionList(),
      ),
    );
  }

  // 生成列表
  List<Widget> _createActionList() {
    return _actionList.map((item) => _actionListItem(item)).toList();
  }
  // 列表item
  Widget _actionListItem(Map<String, dynamic>item) {
    return Container(
      height: 50,
      margin: EdgeInsets.fromLTRB(15, 0, 0, 0),
      decoration: BoxDecoration(
        border: Border(
          bottom: BorderSide(
            width: 0.8,
            color: Colors.black12,
          ),
        )
      ),
      child: GestureDetector(
        child: Row(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: <Widget>[
            Text(item['actionText'], style: TextStyle(fontSize: 15, fontWeight: FontWeight.w500, color: Colors.grey[600])),
            Icon(Icons.keyboard_arrow_right, size: 26, color: Colors.grey[400])
          ],
        ),
        onTap: () {
          if (item['targetPage'] == 'MyViewRecord') {
            Navigator.push(
              context,
              MaterialPageRoute(builder: (BuildContext context) => MyViewRecord(uid: _uid)),
            );
          }
          if (item['targetPage'] == 'MyWatcher') {
            Navigator.push(
              context,
              MaterialPageRoute(builder: (BuildContext context) => MyWatcher(uid: _uid)),
            );
          } 
          if (item['targetPage'] == 'MyFans') {
            Navigator.push(
              context,
              MaterialPageRoute(builder: (BuildContext context) => MyFans(uid: _uid)),
            );
          } 
          if (item['targetPage'] == 'MyCollection') {
            Navigator.push(
              context,
              MaterialPageRoute(builder: (BuildContext context) => MyCollection(uid: _uid)),
            );
          } 
          if (item['targetPage'] == 'RevisePassword') {
            Navigator.push(
              context,
              MaterialPageRoute(builder: (BuildContext context) => RevisePassword()),
            );
          } 
          if (item['targetPage'] == 'LoginOut') {
            _loginOutDialog();
          }
        },
      ),
    );
  }

  Future<void> _loginOutDialog() async {
    return ConfirmModelUtil.confirmModelFun(context, {
      'tipNav': '提示',
      'tipContent': '确认退出登录吗？',
      'cancelMap': {
        'text': '取消',
        'callBack': null,
      },
      'sureMap': {
        'text':'确定',
        'callBack': () => _loginOutAsync(),
      }
    });
  }
  // 退出登录
  Future<Null> _loginOutAsync() async {
    var dataModel = await LoginService.loginOut();
    if (dataModel['code'] == 0) {
      Navigator.pushNamed(context, 'LoginPage');
    } else {
      Fluttertoast.showToast(
        gravity: ToastGravity.CENTER,
        backgroundColor: Colors.black,
        fontSize: 16,
        msg: dataModel['message'],
      );
    }
  }
}