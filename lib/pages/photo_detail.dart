import 'package:flutter/material.dart';

class PhotoDetail extends StatefulWidget {
  final String photoDes;
  final String photoName;
  final String updateTime;
  final List photoList;
  PhotoDetail({Key key, this.photoDes, this.photoName, this.updateTime, this.photoList}) : super(key: key);
  @override
  _PhotoDetail createState() => _PhotoDetail();
}

class _PhotoDetail extends State<PhotoDetail> {
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(title: Text('相册详情')),
      body: Container(
        child: Column(
          children: <Widget>[
            Container(
              height: 60,
              decoration: BoxDecoration(
                border: Border(
                  bottom: BorderSide(
                    width: 0.5,
                    color: Colors.black26,
                  ),
                )
              ),
              padding: EdgeInsets.symmetric(horizontal: 10,vertical: 5),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: <Widget>[
                  Text('相册名称', style: TextStyle(fontSize: 20, fontWeight: FontWeight.w500),),
                  Text(widget.photoName, style: TextStyle(fontSize: 18, color: Colors.grey)),
                ],
              )
            ),
            Container(
              height: 60,
              decoration: BoxDecoration(
                border: Border(
                  bottom: BorderSide(
                    width: 0.5,
                    color: Colors.black26,
                  ),
                )
              ),
              padding: EdgeInsets.symmetric(horizontal: 10,vertical: 5),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: <Widget>[
                  Text('相册描述', style: TextStyle(fontSize: 20, fontWeight: FontWeight.w500),),
                  Text(widget.photoDes, style: TextStyle(fontSize: 18, color: Colors.grey)),
                ],
              )
            ),
            Padding(
              padding: EdgeInsets.symmetric(vertical: 10),
              child: Text('最近更新于' + widget.updateTime),
            ),
            Container(
              padding: EdgeInsets.symmetric(horizontal: 5),
              child: GridView(
                shrinkWrap: true,
                gridDelegate: SliverGridDelegateWithFixedCrossAxisCount(
                  crossAxisCount: 4, 
                  childAspectRatio: 1.0,
                ),
                children: _createGridView(widget.photoList),
              )
            )
          ],
        )
      )
    );
  }

  List<Widget> _createGridView(List photoList) {
    return photoList.map((item) => Image.network(item, fit: BoxFit.fill)).toList();
  }


}