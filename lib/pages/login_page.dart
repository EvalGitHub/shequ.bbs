import 'package:flutter/material.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:shequ/service/login_service.dart';
import 'package:shequ/utils/storage_util.dart';

class LoginPage extends StatefulWidget {
  @override
  _LoginPageState createState() => _LoginPageState();
}

class _LoginPageState extends State<LoginPage> {
  final TextEditingController _userController = new TextEditingController();
  final TextEditingController _pwdController = new TextEditingController();
  bool isObscurePwd = true;

  @override
  void dispose() {
    _userController.dispose();
    _pwdController.dispose();
    super.dispose();
  }
  @override 
  Widget build(BuildContext context) {
    return Scaffold(
     // resizeToAvoidBottomPadding: false,
      backgroundColor: Colors.white,
      body: Container(
          width: MediaQuery.of(context).size.width,
          child: SingleChildScrollView(
            child: Column(
            crossAxisAlignment: CrossAxisAlignment.center,
              children: <Widget>[
                Padding(
                  child: Image.asset('images/login_logo.jpeg',
                    fit: BoxFit.fill,
                    width: 150,
                    height: 150,),
                  padding: EdgeInsets.only(top: 80)
                ),
                Container(
                  width: 300,
                  child: TextFormField(
                    autofocus: true,
                    controller: _userController,
                    decoration: InputDecoration(
                      // contentPadding: EdgeInsets.only(left: 0),
                      border: InputBorder.none, // 无下边框
                      hintText: '用户名',
                      icon: Icon(Icons.person_outline,  size: 28)
                    ),
                  ),
                  padding: EdgeInsets.only(left: 5),
                  margin: EdgeInsets.symmetric(vertical: 20),
                  decoration: BoxDecoration(
                    color: Colors.grey[200],
                    borderRadius: BorderRadius.circular(10)
                  ),
                ),
                Stack(
                  children: <Widget>[
                    Container(
                      width: 300,
                      padding: EdgeInsets.only(left: 5),
                      decoration: BoxDecoration(
                        color: Colors.grey[200],
                        borderRadius: BorderRadius.circular(10)
                      ),
                      child: TextFormField(
                        controller: _pwdController,
                        decoration: InputDecoration(
                          border: InputBorder.none, // 无下边框
                          hintText: '登录密码',
                          icon: Icon(Icons.lock_outline, size: 28)
                        ),
                        obscureText: isObscurePwd,
                      ),
                    ),
                    Positioned(
                      top: 14,
                      right: 10,
                      child: GestureDetector(
                        child: Icon(Icons.remove_red_eye, size: 20, color: Colors.grey,),
                        onTap: () {
                          if (!mounted) return;
                          setState(() {
                            isObscurePwd = !isObscurePwd;
                          });
                        },
                      )
                    )
                  ],
                ),
                Container(
                  width: 220,
                  height: 45,
                  margin: EdgeInsets.only(top: 40),
                  child: RaisedButton(
                    shape: RoundedRectangleBorder(
                      borderRadius: BorderRadius.circular(28),
                      side: BorderSide(color: Colors.red)
                    ),
                    child: Text('登录' , style: TextStyle(color: Colors.white, fontSize: 16, letterSpacing: 30),), 
                    color: Colors.red,
                    // elevation: 8.0,
                    onPressed: () {
                      _loginHandle();
                    },
                  ),
                )
              ],
            ),
          ),
        )
    );
  }

  _loginHandle() {
    if (_userController.text == null || _pwdController.text == null) {
      return;
    }
    _loginAsync({
      'account' : _userController.text,
      'password': _pwdController.text,
      'client': '1'
    });
  }

  Future<Null> _loginAsync(Map<String, dynamic>data) async {
    var dataModel = await LoginService.login(data);
    if (dataModel['code'] != 0) {
      Fluttertoast.showToast(
        gravity: ToastGravity.CENTER,
        backgroundColor: Colors.black,
        fontSize: 16,
        msg: dataModel['message']
      );
      return;
    } 
    StorageUtil.setStringItem('token', dataModel['data']['auth_token']);
    StorageUtil.setStringItem('uid', dataModel['data']['user']['uid'].toString());
    StorageUtil.setStringItem('avatar', dataModel['data']['user']['avatar64']);
    StorageUtil.setStringItem('username', dataModel['data']['user']['username']);
    Future.delayed(const Duration(milliseconds: 500), () {
      Navigator.pushReplacementNamed(context, 'TabNavigator');
    }); 
  }
}