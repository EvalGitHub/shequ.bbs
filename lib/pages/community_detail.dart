import 'package:flutter/gestures.dart';
import 'package:flutter/material.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:shequ/font_class/font_icons.dart';
import 'package:shequ/model/data_item_model.dart';
import 'package:shequ/model/phote_model.dart';
import 'package:shequ/pages/photo_detail.dart';
import 'package:shequ/pages/view_datail.dart';
import 'package:shequ/service/community_service.dart';
import 'package:shequ/model/community_club_model.dart';
import 'package:shequ/service/work_service.dart';
import 'package:shequ/widget/list_data_pull_loading.dart';
// import 'package:shequ/model/data_item_model.dart';

class CommunityDetail extends StatefulWidget {
  final CommunityClubModel clubItem;
  const CommunityDetail({Key key, this.clubItem}) : super(key: key);
  @override 
  _CommunityDetail createState() => _CommunityDetail();
}

class _CommunityDetail extends State<CommunityDetail> {
  ScrollController _listViewController = new ScrollController();
  TapGestureRecognizer _tapGestureRecognizer = new TapGestureRecognizer();
  TextEditingController _applyReasonControl =  new TextEditingController();
  String _currentTab = 'talk';
  int _talkDesCount = 0;
  int initPage = 1;
  bool _hasLoadingList = false;
  List<DataItemModel> _talkArr = [];
  List<PhotoInfoModel> _photoList = [];
  @override 
  void initState() {
    // _getClubPhoto(widget.clubItem.id);
    _getClubComment();
    super.initState();
  }

  @override
  void dispose() {
    _listViewController.dispose();
    _tapGestureRecognizer.dispose();
    _applyReasonControl.dispose();
    super.dispose();
  }

  Widget build(BuildContext context) {
    return Scaffold(
      resizeToAvoidBottomPadding: false,
      appBar: AppBar(title: Text('社区详情')),
      body: SafeArea(
        child: ListView(
          controller: _listViewController,
          children: <Widget>[
            Column(
              crossAxisAlignment: CrossAxisAlignment.center,
              children: <Widget>[
                // logo
                Container(
                  margin: EdgeInsets.only(top: 20),
                  child: ClipOval(
                    child: Image.network(widget.clubItem.logoUrl,
                      fit: BoxFit.cover, 
                      width: 60, 
                      height: 60,
                    ),
                  ),
                ),
                // name
                Padding(
                  padding: EdgeInsets.only(top: 10),
                  child: Text.rich(
                    TextSpan(
                      children: [
                        TextSpan(text: widget.clubItem.name, style: TextStyle(fontSize: 18)),
                        TextSpan(
                          text: '  申请加入', 
                          style: TextStyle(
                            fontSize: 18, color: Colors.blue, fontStyle: FontStyle.italic
                          ),
                          recognizer: _tapGestureRecognizer
                            ..onTap = () {
                              _showListDialog(widget.clubItem.id, context);
                            }
                        ),
                      ]
                    ))
                ),
                // des info
                Padding(
                  child: Text(
                    widget.clubItem.intro, 
                    textAlign: TextAlign.center,
                    style: TextStyle(fontSize: 16, color: Colors.grey[600])),
                  padding: EdgeInsets.symmetric(horizontal: 14, vertical: 10)
                ),
                Container(
                  height: 50,
                  decoration: BoxDecoration(
                    color: Colors.white,
                    border: Border(
                      top: BorderSide(
                        width: 0.5,
                        color: Colors.black26,
                      ),
                      bottom: BorderSide(
                        width: 0.5,
                        color: Colors.black26,
                      ),
                    )
                  ),
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: <Widget>[
                      Expanded(
                        flex:1, 
                        child: Container(
                          height: 50,
                          alignment: Alignment.center,
                          child: GestureDetector(
                            child: Text(
                              '讨论交流', 
                              textAlign: TextAlign.center, 
                              style: TextStyle(
                                fontSize: 16, 
                                fontWeight: FontWeight.w500,
                                color: _currentTab == 'talk' ? Colors.red : Colors.black,
                              )),
                            onTap: () {
                              _handleTabAction('talk');
                            },
                          ),
                          decoration: BoxDecoration(
                            border: Border(
                              bottom: BorderSide(
                                width: 2,
                                color: _currentTab == 'talk' ? Colors.red : Colors.transparent,
                              ),
                            )
                          ),
                        )
                      ),
                      Expanded(
                        flex:1, 
                        child: Container(
                          height: 50,
                          alignment: Alignment.center,
                          child: GestureDetector(
                            child: Text(
                              '社区相册', 
                              textAlign: TextAlign.center, 
                              style: TextStyle(
                                fontSize: 16, fontWeight: FontWeight.w500,
                                color: _currentTab == 'photo' ? Colors.red : Colors.black,
                              )),
                            onTap: () {
                              _handleTabAction('photo');
                            },
                          ),
                          decoration: BoxDecoration(
                            border: Border(
                              bottom: BorderSide(
                                width: 2,
                                color: _currentTab == 'photo' ? Colors.red : Colors.transparent,
                              ),
                            )
                          ),
                        ),
                      ),
                    ],
                  ),
                ),
              ],
            ),
            // 评论信息
            Container(
              child:  _currentTab == 'talk' ? _talkListView : _photoListView,
            ),
          ],
        ),
      ),
    );
  } 
  
  Widget get _talkListView {
    return ListView.builder(
      shrinkWrap: true, 
      controller: _listViewController,
      itemCount: _talkArr.length + 1,
      itemBuilder: (BuildContext context, int index) {
        if (_talkArr.length == 0 && _hasLoadingList == false) { // 没有数据显示加载
          return ListDataPullLoading();
        }
        if (index == _talkArr.length && _talkArr.length < _talkDesCount) {
          ++initPage;
          _getClubComment();
        }
        if (index == _talkArr.length && _talkArr.length == _talkDesCount) {
          return _noManyItems;
        }
        return _talkContentItem(_talkArr[index]);
      }
    );
  }

  Widget _talkContentItem(DataItemModel item) {
    return Container(
      decoration: BoxDecoration(
        color: Colors.white,
        border: Border(
          bottom: BorderSide(
            width: 0.5,
            color: Colors.black26,
          ),
        )
      ),
      padding: EdgeInsets.fromLTRB(10, 20, 10, 10),
      child: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        crossAxisAlignment: CrossAxisAlignment.start,
        children: <Widget>[
          Row(
            mainAxisAlignment: MainAxisAlignment.center,
            crossAxisAlignment: CrossAxisAlignment.start,
            children: <Widget>[
              ClipOval(
                child: Image.network(item.userAvatar, fit: BoxFit.fill, 
                  height: 40,
                  width: 40,
                ),
              ),
              Expanded(
                flex: 1,
                child: Padding(
                  padding: EdgeInsets.only(left: 10),
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: <Widget>[
                      Text(
                        item.annoName, maxLines: 1, 
                        softWrap: false, overflow: TextOverflow.ellipsis,
                        style: TextStyle(fontWeight: FontWeight.w600, fontSize: 16),
                      ),
                      Padding(
                        padding: EdgeInsets.only(top: 5),
                        child: Text.rich(
                          TextSpan(
                            children: [
                              TextSpan(text: '发表于  ', style: TextStyle(color: Colors.black54, fontSize: 14)),
                              TextSpan(
                                text: item.createTimeFormat,
                                style: TextStyle(color: Colors.black54, fontSize: 14)
                              ),
                            ]
                          ),
                        )
                      )
                    ],
                  )
                )
              ),
            ],
          ),
          Row(
            mainAxisAlignment: MainAxisAlignment.start,
            crossAxisAlignment: CrossAxisAlignment.start,
            children: <Widget>[
              Expanded(
                flex: 1,
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: <Widget>[
                    Padding(
                      child: GestureDetector(
                        child: Text(
                          item.title, 
                          maxLines: 1, softWrap: false, 
                          overflow: TextOverflow.ellipsis,
                          style: TextStyle(fontWeight: FontWeight.w500, fontSize: 16),
                        ),
                        onTap: () {
                          _jumpToViewDetail(item);
                        },
                      ),
                      padding: EdgeInsets.symmetric(vertical: 5),
                    ),
                    Padding(
                      child: GestureDetector(                        
                        child: Text(item.content, maxLines: 2, softWrap: false, overflow: TextOverflow.ellipsis,),
                        onTap: () {
                          _jumpToViewDetail(item);
                        },
                      ),
                      padding: EdgeInsets.symmetric(vertical: 5),
                    ),
                    Padding(
                      padding: EdgeInsets.only(top: 5),
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: <Widget>[
                          Padding(
                            padding: EdgeInsets.only(right: 10),
                            child: Row(
                              children: <Widget>[
                                GestureDetector(                        
                                  child: Row(
                                    children: <Widget>[
                                      Icon(MyIcon.view, size: 20),
                                      Padding(
                                        child: Text(item.viewCount.toString()),
                                        padding: EdgeInsets.symmetric(horizontal: 5),
                                      )
                                    ],
                                  ),
                                  onTap: () {
                                    _jumpToViewDetail(item);
                                  },
                                ),
                              ],
                            ),
                          ),
                          Row(
                            children: <Widget>[
                              Padding(
                                padding: EdgeInsets.only(right: 10),
                                child: Row(
                                  children: <Widget>[
                                    GestureDetector(
                                      child: Icon(MyIcon.like, size: 18, color: item.currentUserPraised ? Colors.red : Colors.black26),
                                      onTap: () {
                                        _praiseItem(item);
                                      },
                                    ),
                                    Padding(
                                      child: Text(item.praiseCount.toString()),
                                      padding: EdgeInsets.symmetric(horizontal: 5),
                                    )
                                  ],
                                ),
                              ),
                              Padding(
                                padding: EdgeInsets.zero,
                                child: Row(
                                  children: <Widget>[
                                    Icon(MyIcon.talk, size: 18),
                                    Padding(
                                      child: Text(item.replyCount.toString()),
                                      padding: EdgeInsets.symmetric(horizontal: 5),
                                    )
                                  ],
                                ),
                              ),
                            ],
                          ),
                        ],
                      ),
                    ),
                  ],
                ),
              ),
            ],
          )
        ],
      )
    ); 
  }

  Widget get _photoListView {
    return ListView.separated(
      shrinkWrap: true, 
      controller: _listViewController,
      itemBuilder: (context, index) {
        if (_photoList.length == 0 && _hasLoadingList == false) { // 没有数据显示加载
          return ListDataPullLoading();
        }
        if (index == _photoList.length) {
          return _noManyItems;
        }
        return _photoItem(_photoList[index]);
      }, 
      separatorBuilder: (context, index) {
        return Divider(color: Colors.black12, height: 1,);
      }, 
      itemCount: _photoList.length + 1,
    );
  }

  Widget _photoItem(PhotoInfoModel photoItem) {
    return GestureDetector(
      child: Container(
        padding: EdgeInsets.symmetric(horizontal: 20, vertical: 20),
        child: Row(
          children: <Widget>[
            Image.network(photoItem.imagePaths[0], fit: BoxFit.fill, width: 44, height: 44,),
            Padding(
              child: Text(photoItem.name, style: TextStyle(fontSize: 16),),
              padding: EdgeInsets.only(left: 10),
            ),
          ],
        ),
      ),
      onTap: () {
        Navigator.push(
          context,
          MaterialPageRoute(builder: (BuildContext context) => 
            PhotoDetail(
              photoDes: photoItem.desc,
              photoName: photoItem.name,
              updateTime: photoItem.updateTime,
              photoList: photoItem.imagePaths,
            )
          ),
        );
      },
    );
  }

  Widget get _noManyItems {
    return Container(
      alignment: Alignment.center,
      padding: EdgeInsets.all(16),
      child: Text('没有更多了', style: TextStyle(color: Colors.grey),),
    );
  }

  void _handleTabAction(String tabName) {
    if (!mounted) return;
    setState(() {
      initPage = 1;
      _currentTab = tabName;
      _hasLoadingList = false;
    });
    if (tabName == 'talk') {
      _getClubComment();
    }
    if (tabName == 'photo') {
      _getClubPhoto(widget.clubItem.id);
    }
  }

  // 跳转详情页
  void _jumpToViewDetail(DataItemModel item) {
    Navigator.push(
      context,
      MaterialPageRoute(builder: (BuildContext context) => ViewDetail(articleId: item.id.toString())),
    );
  }

   // 点赞
  void _praiseItem(DataItemModel item) async {
    _praiseItemAsync(item);
  }

  // 申请弹窗
  Future<void> _showListDialog(int id, BuildContext context) async {
    return await showDialog<int>(
      context: context,
      builder: (BuildContext context) {
        var child = Container(
          padding: EdgeInsets.symmetric(horizontal: 10, vertical: 20),
          height: 340,
          width: 400,
          child: SingleChildScrollView(
            child: Column(
              children: <Widget>[
                Padding(
                  child: Text('填写申请理由', style: TextStyle(fontSize: 18),),
                  padding: EdgeInsets.only(bottom: 15),
                ),
                TextField(
                  controller: _applyReasonControl,
                  // autofocus: true,
                  maxLines: 3,
                  decoration: InputDecoration(
                    hintText: '输入内容',
                    border: OutlineInputBorder(
                      borderSide: BorderSide(
                        color: Colors.grey,
                        width: 0.5
                      )
                    ),
                  ),
                ),
                Container(
                  width: 400,
                  height: 40,
                  margin: EdgeInsets.fromLTRB(0, 30, 0, 15),
                  child: OutlineButton(
                    child: Text('申请加入', style: TextStyle(fontSize: 16)), 
                    borderSide: BorderSide(
                      color: Colors.green,
                      width: 0.5,
                    ),
                    textColor: Colors.green,
                    onPressed: () {
                      _applyJoinCommunity(id, context);
                    },
                  ),
                ),
                Container(
                  width: 400,
                  height: 40,
                  child: OutlineButton(
                    borderSide: BorderSide(
                      color: Colors.red,
                      width: 0.5,
                    ),
                    textColor: Colors.red,
                    child: Text('取消', style: TextStyle(fontSize: 16)), 
                    onPressed: () {
                      _applyReasonControl.clear();
                      Navigator.of(context).pop();
                    } 
                  ),
                ),
              ],
            )
          ),
        );
        return Dialog(child: child);
      }
    );
  }

  // 点赞接口调用
  Future<Null> _praiseItemAsync(DataItemModel item) async {
    Map<String, dynamic> postData = {
      'post_id': item.id.toString()
    };
    var dataModel = await WorkService.praiseItem(postData);
    if (dataModel['code'] == 0) {
      item.currentUserPraised = !item.currentUserPraised;
      Fluttertoast.showToast(
        gravity: ToastGravity.CENTER,
        backgroundColor: Colors.black,
        fontSize: 16,
        msg: item.currentUserPraised ? '点赞成功' : '取消点赞'
      );
      if (!mounted) return;
      setState(() {});
    }
  }

  // 相册
  Future<Null> _getClubPhoto(int clubId) async {
    var dataModel = await CommunityService.clubPhoto(clubId);
    var photoList = (dataModel['data']['rows'] as List).map((item) => PhotoInfoModel.fromJson(item)).toList();
    if (!mounted) return;
    setState(() {
      _photoList = photoList;
      _hasLoadingList = true;
    });
  }
  
  // 讨论信息
  Future<Null> _getClubComment() async {
    Map<String, dynamic>dataPost = {
      'page': initPage,
      'clubId': widget.clubItem.id
    };
    var dataModel = await CommunityService.clubComment(dataPost);
    if (!mounted) return;
    setState(() {
      _hasLoadingList = true;
      _talkDesCount = dataModel['data']['total'];
      _talkArr = (dataModel['data']['rows'] as List).map((item) => DataItemModel.fromJson(item)).toList();
    });
  }

  // 加入社区
  Future<Null> _applyJoinCommunity(int id, BuildContext context) async {
    Map<String, dynamic>dataObj = {
      'club_id': id.toString(),
      'reason': _applyReasonControl.text,
    };
    if (_applyReasonControl.text.trim().length < 5) {
      Fluttertoast.showToast(
        gravity: ToastGravity.CENTER,
        backgroundColor: Colors.black,
        fontSize: 16,
        msg: '申请理由内容不少于五个字！'
      );
      return;
    }
    var dataModel = await CommunityService.joinCommunity(dataObj);
    Navigator.of(context).pop();
    _applyReasonControl.clear();
    Fluttertoast.showToast(
      gravity: ToastGravity.CENTER,
      backgroundColor: Colors.black,
      fontSize: 16,
      msg: dataModel['message']
    );
  }
}