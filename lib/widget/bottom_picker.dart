import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
class BottomPickerWidget extends StatefulWidget {
  final List tabModelList;
  final String cancleText;
  final String sureText;
  final Function cancleCallBack;
  final Function sureCallBack;
  final double pickHeight;
  const BottomPickerWidget({
    Key key, 
    @required this.tabModelList, 
    this.cancleText, 
    this.sureText,
    this.pickHeight,
    @required this.sureCallBack, 
    this.cancleCallBack
  }) : super(key: key);
  
  @override 
  _BottomPickerWidget createState() => _BottomPickerWidget();
}

class _BottomPickerWidget extends State<BottomPickerWidget> {
  int selectedCommunityTypeValue = 0;
  @override 
  Widget build(BuildContext context) {
    return Container(
      height: widget.pickHeight??350,
      child: Column(
        children: <Widget>[
          Container(
            padding: EdgeInsets.fromLTRB(20, 10, 20, 10),
            color: Colors.grey[200],
            child: Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: <Widget>[
                GestureDetector(
                  child: Text(widget.cancleText??'取消', style: TextStyle(fontSize: 17, color: Colors.black54)),
                  onTap: () {
                    Navigator.of(context).pop();
                    if (widget.cancleCallBack != null) widget.cancleCallBack();
                  },
                ),
                GestureDetector(
                  child: Text(widget.sureText??'确定', style: TextStyle(fontSize: 17, color: Colors.green),),
                  onTap: () {
                    Navigator.of(context).pop();
                    widget.sureCallBack(selectedCommunityTypeValue);
                  },
                ),
              ],
            ),
          ),
          Expanded(
            flex: 1,
            child: DefaultTextStyle(
              style: TextStyle(
                color: Colors.black,
                fontSize: 20,
              ),
              child: CupertinoPicker(
                itemExtent: 40,
                backgroundColor: Colors.white,
                onSelectedItemChanged: (value) {
                  setState(() {
                    selectedCommunityTypeValue = value;
                  });
                },
                children: widget.tabModelList.map((data) {
                  return Container(
                    child: Text(data, style: TextStyle(fontSize: 20,)),
                    alignment: Alignment.center,
                  );
                }).toList(),
              ),
            ),
          ),
        ],
      ),
    );
  }
} 