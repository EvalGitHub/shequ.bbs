import 'package:flutter/material.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:shequ/font_class/font_icons.dart';
import 'package:image_picker/image_picker.dart';
import 'package:shequ/utils/upload_image.dart';
class CommentPostModal extends StatefulWidget{
  final String id;
  final Function addCommentCallBack;

  const CommentPostModal({Key key, @required this.id, @required this.addCommentCallBack}) : super(key: key);
  
  @override 
  _CommentPost createState() => _CommentPost();
}

class _CommentPost extends State<CommentPostModal> {
  final _contentController = TextEditingController();
  final _listViewController =  new ScrollController();
  List<Map<String, dynamic>> imageList = [];
  bool isAnon = false;
  @override 
  void dispose() {
    _contentController.dispose();
    _listViewController.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: Container(
        height: 480,
        child: ConstrainedBox(
          constraints: BoxConstraints.expand(),
          child: Stack(
            children: <Widget>[
              Column(
                children: <Widget>[
                  // 顶部文字描述
                  Container(
                    height: 50,
                    // alignment: Alignment.center,
                    width: MediaQuery.of(context).size.width,
                    decoration: BoxDecoration(
                      border: Border(
                        bottom: BorderSide(
                          width: 0.5,
                          color: Colors.black26,
                        )
                      )
                    ),
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: <Widget>[
                        GestureDetector(
                          child: Text("取消", style: TextStyle(fontSize: 18, fontWeight: FontWeight.w500),),
                          onTap: () {
                            Navigator.of(context).pop(); // 关闭弹窗
                          },
                        ),
                        Expanded(
                          flex: 1,
                          child: Container(
                            alignment: Alignment.center,
                            child: Text("回帖", style: TextStyle(fontSize: 18, fontWeight: FontWeight.w500,),),
                          ),
                        ),
                        GestureDetector(
                          child: Text("发送", style: TextStyle(fontSize: 18, fontWeight: FontWeight.w500, color: Colors.blue),),
                          onTap: () {
                            _addComments(widget.id);
                          },
                        )
                      ],
                    ),
                    padding: EdgeInsets.fromLTRB(15, 10, 15, 10),
                  ),
                  Expanded(
                    flex: 1,
                    child:  Container(
                      height: 200,
                      child: ListView(
                        shrinkWrap: true,
                        controller: _listViewController,
                        children: <Widget>[
                          // 文本输入，文件上传
                          Container(
                            height: 64,
                            // color: Colors.red,
                            padding: EdgeInsets.fromLTRB(15, 0, 0, 0),
                            child: TextField(
                              controller: _contentController,
                              autofocus: false,
                              cursorColor: Colors.blue,
                              cursorWidth: 1,
                              maxLines: 2,
                              decoration: InputDecoration(
                                border: InputBorder.none, // 无下边框
                                hintText: '点击此处编辑~~',
                                hintStyle: TextStyle(
                                  color: Colors.grey,
                                  fontSize: 15,
                                ),
                              ),
                            ),
                          ),
                          Container(
                            padding: EdgeInsets.fromLTRB(15, 0, 15, 50),
                            child: imageList.length > 0 ? GridView.count(
                              controller: _listViewController,
                              shrinkWrap: true,
                              crossAxisCount: 4,
                              mainAxisSpacing: 0,
                              crossAxisSpacing: 10,
                              children: _showImage
                            ) : Text('暂未选择图片~~'),
                          )
                        ],
                      ),
                    ),
                  ),
                ],
              ),
              // 操作按钮
              Positioned(
                bottom: 0,
                child: Container(
                  // alignment: Alignment.centerLeft,
                  margin: EdgeInsets.only(bottom: 0),
                  padding: EdgeInsets.fromLTRB(20, 0, 0, 0),
                  height: 50,
                  width: MediaQuery.of(context).size.width,
                  color: Colors.grey[100],
                  child: Row(
                    crossAxisAlignment: CrossAxisAlignment.center,
                    children: <Widget>[
                      Padding(
                        padding: EdgeInsets.only(right: 25),
                        child: Icon(MyIcon.emoji, size: 30, color: Colors.black38), 
                      ),
                      Padding(
                        padding: EdgeInsets.only(right: 20),
                        child: GestureDetector(
                          child: Icon(MyIcon.picture, size: 30, color: Colors.black38,), 
                          onTap: () {
                            getImage();
                          }
                        ),
                      ),
                      Row(
                        crossAxisAlignment: CrossAxisAlignment.center,
                        children: <Widget>[
                          Padding(
                            padding: EdgeInsets.only(right: 0),
                            child: GestureDetector(
                              child: Icon(MyIcon.choice, size: 40, color: isAnon ? Colors.blue : Colors.black38,),
                              onTap: () {
                                setState(() {
                                  isAnon = !isAnon;
                                });
                              },
                            )
                          ),
                          Text('是否匿名', style: TextStyle(fontSize: 18),),
                        ],
                      ),
                    ],
                  ),
                ),
              )  
            ],
          ),
        )
      )
    );
  }

  // image list
  List<Widget> get _showImage {
    return imageList.map(
      (item) => Container(
        padding: EdgeInsets.fromLTRB(0, 10, 5, 0),
        margin: EdgeInsets.only(bottom: 15),
        child: Stack(
          overflow: Overflow.visible,
          children: <Widget>[
            Image.network(item['src'], fit: BoxFit.cover, height: 74, width: (MediaQuery.of(context).size.width - 50) / 4),
            Positioned(
              top: -10,
              right: -5,
              child: GestureDetector(
                child: Container(
                  child: Icon(Icons.close, color: Colors.white, size:24),
                  decoration: BoxDecoration(
                    color: Colors.red,
                    borderRadius: BorderRadius.circular(13),
                  ),
                ),
                onTap: () {
                  _deleteUploadImage(item['image_id']);
                },
              ),
            ),
          ],
        ) 
      )
    ).toList();
  }

  // 添加评论
  _addComments(String id) async {
    String imageStr = '';
    imageList.forEach((item) {
      imageStr += '<p><img src=${item['src']}></p>';
    });
    Map<String, dynamic> dataObj = {
      'id': id.toString(),
      'content': '<div><p>${_contentController.text}</p>$imageStr</div>',
      'is_anon': isAnon ? '1' : '0'
    };
    Navigator.of(context).pop(); // 关闭弹窗
    widget.addCommentCallBack(dataObj);
  }

  // 删除图片
  _deleteUploadImage(String imageId) {
    List<Map<String, dynamic>> _tmpList = []; 
    imageList.forEach((item) {
      if (item['image_id'] != imageId) {
        _tmpList.add(item);
      }
    });
    setState(() {
      imageList = _tmpList;
    });
  }

  // 上传图
  Future getImage() async {
    var image = await ImagePicker.pickImage(source: ImageSource.gallery);
    var dataModel = await UploadData.uploadImage(image);
    if (!mounted) return;
    if (dataModel['code'] == 0) {
      setState(() {
        imageList.add(dataModel['data']);
      });
      return;
    }
    Fluttertoast.showToast(
      gravity: ToastGravity.CENTER,
      backgroundColor: Colors.black,
      fontSize: 16,
      msg: dataModel['message'],
    );
  }
}