import 'package:flutter/material.dart';

class ListDataPullLoading extends StatelessWidget {
  Widget build(BuildContext context) {
    return Container( // 加载loading
      padding: const EdgeInsets.all(16),
      alignment: Alignment.center,
      child: SizedBox(
        width: 24,
        height: 24,
        child: CircularProgressIndicator(
          strokeWidth: 2,
        ),
      ), 
    );
  }
}