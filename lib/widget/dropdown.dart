import 'package:flutter/material.dart';

class Dropdown extends StatefulWidget {
  final List dropItemList;  // 下拉数组
  final String defaultValue; // 默认值
  final Function selectCallBack;
  const Dropdown(
    {
      Key key, 
      @required this.dropItemList, 
      @required this.defaultValue,
      this.selectCallBack
    }) : super(key: key);

  @override 
  _Dropdown createState() => _Dropdown();
}

class _Dropdown extends State<Dropdown> {
  String value ;

  @override
  void initState() {
    value = widget.defaultValue;
    super.initState();
  }
  @override 
  Widget build(BuildContext context) {
    return DropdownButtonHideUnderline(
      child: new DropdownButton(
        items: generateDropdownItems(), 
        hint: new Text(widget.defaultValue), // 当没有初始值时显示
        onChanged: (selectValue) {
          setState(() {
            value = selectValue;
          });
          widget.selectCallBack(selectValue);
        },
        value: value, // 设置初始值，要与列表中的value是相同的
        elevation: 0,//设置阴影
        style: new TextStyle(//设置文本框里面文字的样式
          color: Colors.blue,
          fontSize: 17
        ),
        iconSize: 30,//设置三角标icon的大小
      ),
    );
  }

  List<DropdownMenuItem> generateDropdownItems() {
    var _dropDownList = widget.dropItemList.map(
      (item) => DropdownMenuItem(
        child: Text(item['text'], style: TextStyle(color: item['value'] == value ? Colors.blue : Colors.grey),),
        value: item['value'],
      )
    );
    return _dropDownList.toList();
  }
}