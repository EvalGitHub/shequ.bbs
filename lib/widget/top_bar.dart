import 'package:flutter/material.dart';
import 'package:shequ/font_class/font_icons.dart';

class TopBar extends StatefulWidget {
  final bool hideLeft;
  final bool hideRight;
  final String titleText;

  const TopBar({
    Key key, 
    @required this.hideLeft,
    @required this.hideRight,
    @required this.titleText,
  }) : super(key: key);

  @override 
  _TopBar createState() => _TopBar();
}

class _TopBar extends State<TopBar> {
  @override
  Widget build(BuildContext context) {
    return Container(
      color: Colors.red,
      height: 30,
      padding: EdgeInsets.fromLTRB(15, 0, 20, 10),
      alignment: Alignment.bottomCenter,
      child: Row(
        children: <Widget>[
          Container(
            child: widget.hideLeft ?  Icon(
                Icons.chat,
                size: 25,
                color: Colors.transparent,
              ) :
              GestureDetector(
                child: Icon(
                  MyIcon.msg,
                  size: 22,
                  color: Colors.white,
                ),
                onTap: () {
                  Navigator.pushNamed(context, 'NotifyMessage');
                },
              ),
          ),
          Expanded(
            flex: 1,
            child: Text(
              widget.titleText, 
              textAlign: TextAlign.center,
              style: TextStyle(color: Colors.white, fontSize: 18),),
          ),
          Container(
            child: widget.hideRight ? Icon(
                Icons.search,
                size: 30,
                color: Colors.transparent
              ) : 
              Container(
                child: widget.hideRight ? null :
                  GestureDetector(
                    child: Icon(
                      Icons.search,
                      size: 30,
                      color: Colors.white
                    ),
                    onTap: () {
                      _jumpToSearch();
                    },
                  )
              )
          )
        ],
      ),
    );
  }

  // 跳转至搜索页面
  void _jumpToSearch() {
    Navigator.pushNamed(context, 'SearchPage');
  }
}