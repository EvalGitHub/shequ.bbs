import 'package:flutter/material.dart';
import 'package:shequ/model/tab_item_model.dart';
// import 'package:shequ/model/tab_arr_model.dart';

class ScrollCrossTab extends StatefulWidget {
  final List<TabItemModel> tabArr;
  final Function tabCallBack;
  final int defaultKey;
  final Color defaultBgColor;
  const ScrollCrossTab({
    Key key, 
    @required this.tabArr,
    @required this.tabCallBack,
    @required this.defaultKey,
    this.defaultBgColor,
  }) : super(key: key);

  @override 
  _ScrollCrossTab createState() => _ScrollCrossTab();
}

class _ScrollCrossTab extends State<ScrollCrossTab> {
  int _itemKey = 0;

  @override
  void initState() {
    _itemKey = widget.defaultKey;
    super.initState();
  }

  Widget build(BuildContext context) {
    return Container(
      decoration: BoxDecoration(
        color: widget.defaultBgColor??Colors.white,
        border: Border(
          bottom: BorderSide(
            width: 0.5,
            color: Colors.black26,
          )
        )
      ),
      alignment: Alignment.center,
      child: SingleChildScrollView(
        scrollDirection: Axis.horizontal,
        child: Row(
          children: widget.tabArr.map((item) => tabItem(item)).toList()
        )
      )
    );
  }

  Widget tabItem(TabItemModel item) {
    return Container(
      height: 50,
      padding: EdgeInsets.fromLTRB(5, 10, 10, 10),
      child: GestureDetector(
        child: Padding(
          child: Text(item.title, style: TextStyle(fontSize: 16, color: _itemKey == item.id ? Colors.red : Colors.black),),
          padding: EdgeInsets.all(5),
        ),
        onTap: () {
          setState(() {
            _itemKey = int.parse(item.id.toString());
          });
          widget.tabCallBack(item);
        },
      )
    );
  }
}