import 'package:flutter/material.dart';

class ActionMeunSheet extends StatefulWidget {
  final List<Map<String, dynamic>> actionMeun;
  final double containerHeight;
  final bool hasTopNav;
  const ActionMeunSheet({Key key, this.actionMeun, this.containerHeight, this.hasTopNav}) : super(key: key);

  @override 
  _ActionMeunSheet createState() => _ActionMeunSheet();
}

class _ActionMeunSheet extends State<ActionMeunSheet> {
  final _listViewController =  new ScrollController();
  
  @override 
  dispose() {
    _listViewController.dispose();
    super.dispose();
  }

  Widget build(BuildContext context) {
    return Container(
      color: Colors.grey[200],
      height: widget.containerHeight ?? 160,
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.center,
        children: <Widget>[
          widget.hasTopNav ? Container(
            width: MediaQuery.of(context).size.width,
            padding: EdgeInsets.fromLTRB(10, 15, 10, 15),
            decoration: BoxDecoration(
             border: Border(
                bottom: BorderSide(
                  width: 0.6,
                  color: Colors.grey[400],
                ),
              )
            ),
            child: Stack(
              children: <Widget>[
                Positioned(
                  left: 0,
                  child: GestureDetector(
                    child: Text('取消', style: TextStyle(fontSize: 18),),
                    onTap: () {
                      Navigator.of(context).pop();
                    }
                  )
                ),
                Center(
                  child: Text('分享', style: TextStyle(fontSize: 18)),
                )
              ],
            ),
          ) : Text(''),
          Container(
            // color: Colors.grey[200],
            padding: EdgeInsets.fromLTRB(8, 15, 8, 0),
            child: GridView.count(
              controller: _listViewController,
              shrinkWrap: true,
              crossAxisCount: 4,
              mainAxisSpacing: 0,
              crossAxisSpacing: 10,
              children: _showGridListComponent
            ),
          )
        ],
      )
    );
  }

  List<Widget> get _showGridListComponent {
    return widget.actionMeun.map((item) => Container(
      child: GestureDetector(
        child: Column(
          children: <Widget>[
            ClipOval(
              child: Container(
                height: item['height']??45,
                width: item['width']??45,
                child: item['icon'], // Icon(item['icon'], size: 30, color: Colors.red),
                padding: EdgeInsets.all(6),
                color: item['color'] ?? Colors.white,
              ),
            ),
            Padding(
              child: item['text'] != '' ? item['text'] : '', // Text(item['text'], style: TextStyle(fontSize: 14)),
              padding: EdgeInsets.only(top: 10),
            ),
          ],
        ),
        onTap: () {
          item['callBack']();
        },
      ),
    )).toList();
  }
}