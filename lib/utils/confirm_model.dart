import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class ConfirmModelUtil {
 static Future<void> confirmModelFun(BuildContext context, Map<String, dynamic>actionObj) async {
    return showDialog<void>(
      context: context,
      barrierDismissible: false, // user must tap button!
      builder: (BuildContext context) {
        return CupertinoAlertDialog(
          title: Text(actionObj['tipNav']??'提示', style: TextStyle(fontSize: 20)),
          content: Padding(
            child: Text(actionObj['tipContent']??'点击确认执行操作...', style: TextStyle(fontSize: 16)),
            padding: EdgeInsets.symmetric(vertical: 5),
          ),
          actions: <Widget>[
            CupertinoDialogAction(
              child: Text(actionObj['cancelMap']['text']??'取消', style: TextStyle(color: Colors.black54)),
              onPressed: () {
                Navigator.of(context).pop();
                if (actionObj['cancelMap']['callBack'] != null) {
                  actionObj['cancelMap']['callBack']();
                }
              },
            ),
            CupertinoDialogAction(
              child: Text(actionObj['sureMap']['text']??'确认', style: TextStyle(color: Colors.blue)),
              onPressed: () {
                Navigator.of(context).pop();
                if (actionObj['sureMap']['callBack'] != null) {
                  actionObj['sureMap']['callBack']();
                }
              },
            ),
          ],
        );
      },
    );
  }
}