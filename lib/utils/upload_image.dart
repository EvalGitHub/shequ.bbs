
import 'dart:io';

import 'package:shequ/service/common_service.dart';
import 'dart:async';
import 'dart:convert';
// import 'package:dio/dio.dart';
// Dio dio = new Dio();

class UploadData {
  static Future uploadImage(imageFile) async {
    if (imageFile == null) return;
    String path = imageFile.path;
    // var name = path.substring(path.lastIndexOf("/") + 1, path.length);
    // FormData formData = new FormData.fromMap(<String, dynamic>{
    //   "file": new MultipartFile.fromString(path, filename: name)
    // });
    File file = new File(path);
    List<int> imageBytes = await file.readAsBytes();
    String imgAffix = file.path.substring(file.path.lastIndexOf('.')+1);
    final response = await CommonService.uploadImage({
      'data': 'data:image/$imgAffix;base64,' + base64Encode(imageBytes)
    });
    return response;
  }
}