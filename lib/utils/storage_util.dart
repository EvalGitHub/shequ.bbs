import 'package:shared_preferences/shared_preferences.dart';

class StorageUtil {
  static setStringItem(String key, String value) async {
    final prefs = await SharedPreferences.getInstance();
    prefs.setString(key, value);
  }

  static getStringItem(String key) async {
    //StorageUtil.setStringItem('token', 'accb9a96b9a13e163342450b91a16cddb24979036d2h960quv');
    // StorageUtil.setStringItem('uid', '11243');
    final prefs = await SharedPreferences.getInstance();
    String value = prefs.getString(key);
    return value;
  }

  static setStringListItem(String key, List<String>value) async {
    final prefs = await SharedPreferences.getInstance();
    prefs.setStringList(key, value);
  }

  static getStringListItem(String key) async {
    final prefs = await SharedPreferences.getInstance();
    List<String> value = prefs.getStringList(key);
    return value;
  }

  static remove(String key) async {
    final prefs = await SharedPreferences.getInstance();
    prefs.remove(key);
  }

  static  clear() async {
    final prefs = await SharedPreferences.getInstance();
    prefs.clear();
  }
}