class ReplyModel {
  final String content;
  final String createTime;
  final String createTimeFormat;
  final int forumId;
  final String forumTitle;
  final int id;
  final String postAuthor;
  final String postContent;
  final int postId;
  final String postTitle;
  final int uid;

  ReplyModel({this.content, this.createTime, this.createTimeFormat, this.forumId, this.forumTitle, this.id, this.postAuthor, this.postContent, this.postId, this.postTitle, this.uid});
  factory ReplyModel.fromJson(Map<String, dynamic>json) {
    return ReplyModel(
      content: json['content']??'',
      createTime: json['create_time']??'',
      createTimeFormat: json['create_time_format']??'',
      forumId: json['forum_id']??'',
      forumTitle: json['forum_title']??'',
      id: json['id']??'',
      postAuthor: json['post_author']??'',
      postContent: json['post_content']??'',
      postId: json['post_id']??'',
      postTitle: json['post_title']??'',
      uid: json['uid']??'',
    );
  }
}