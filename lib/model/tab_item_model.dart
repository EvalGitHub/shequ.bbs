
class TabItemModel {
  final int id;
  final String desc;
  final String logo;
  final String title;
  final int postCount;
  final int replyCount;
  const TabItemModel({this.id, this.desc, this.logo, this.title, this.postCount, this.replyCount});

  factory TabItemModel.fromJson(Map<String, dynamic>json) {
    return TabItemModel(
      id: json['id']??0,
      desc: json['desc']??'',
      logo: json['logo']??'',
      title: json['title']??json['name'],
      postCount: json['postCount']??0,
      replyCount: json['replyCount']??0,
    );
  }  
}