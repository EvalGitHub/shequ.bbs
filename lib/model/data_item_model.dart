
class DataItemModel {
  final String annoName;
  final String authorName;
  final String content;
  final String createTime;
  final String createTimeFormat;
  bool currentUserPraised;
  final int essence;
  final String forum;
  final int forumId;
  final int id;
  final List imgs;
  final int isAnon;
  final int praiseCount;
  final int replyCount;
  final String title;
  final int top;
  final int type;
  final int uid;
  final String userAvatar;
  final  userLevelIcon;
  final String username;
  final int viewCount;

  DataItemModel({
    this.annoName,
    this.authorName,
    this.content,
    this.createTime,
    this.createTimeFormat,
    this.currentUserPraised,
    this.essence,
    this.forum,
    this.forumId,
    this.id,
    this.imgs,
    this.isAnon,
    this.praiseCount,
    this.replyCount,
    this.title,
    this.top,
    this.type,
    this.uid,
    this.userAvatar,
    this.userLevelIcon,
    this.username,
    this.viewCount,
  });

  factory DataItemModel.fromJson(Map<String, dynamic>json) {
    return DataItemModel(
      annoName:json['anon_name'],
      authorName:json['author_name'],
      content:json['content'],
      createTime:json['create_time'],
      createTimeFormat:json['create_time_format'],
      currentUserPraised:json['current_user_praised'],
      essence:json['essence'],
      forum:json['forum'],
      forumId:json['forumId'],
      id:json['id'],
      imgs:json['imgs'].length > 0 ? json['imgs'].toList() : [],
      isAnon:json['isAnon'],
      praiseCount:json['praise_count'],
      replyCount:json['reply_count'],
      title:json['title'],
      top:json['top'],
      type:json['type'],
      uid:json['uid'],
      userAvatar:json['user_avatar'],
      userLevelIcon:json['user_level_icon'],
      username: json['user_name']??json['username'],
      viewCount:json['view_count'],
    );
  }
}