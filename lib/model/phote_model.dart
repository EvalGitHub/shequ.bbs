class PhotoInfoModel {
  int clubId;
  String cover;
  String createTime;
  String desc;
  int id;
  List imagePaths;
  List imagePathsTeam;
  String name;
  int photosTotal;
  String updateTime;
  PhotoInfoModel({
    this.clubId, this.cover, this.createTime, 
    this.desc, this.id, this.imagePaths, this.imagePathsTeam,
    this.name,this.photosTotal, this.updateTime
  });
  factory PhotoInfoModel.fromJson(Map<String, dynamic>json) {
    if (json == null) {
      return PhotoInfoModel(
        clubId: 0,
        cover: '',
        createTime: '',
        desc: '',
        id: 0,
        imagePaths: [''],
        imagePathsTeam: [''],
        name: '',
        photosTotal: 0,
        updateTime: '',
      );
    } else {
      return PhotoInfoModel(
        clubId: json['club_id'],
        cover: json['cover'],
        createTime: json['create_time'],
        desc: json['desc'],
        id: json['id'],
        imagePaths: json['image_paths'],
        imagePathsTeam: json['image_paths_team'],
        name: json['name'],
        photosTotal: json['photos_total'],
        updateTime: json['update_time'],
      );
    }
  } 
}