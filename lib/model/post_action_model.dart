class PostActionModel {
  final bool apply;
  final bool delete;
  final bool edit;
  final bool essence;
  final bool top;

  const PostActionModel({this.apply, this.delete, this.edit, this.essence, this.top});

  factory PostActionModel.fromJson(Map<String, dynamic>json) {
    if (json != null) {
      return PostActionModel(
        apply: json['apply'],
        delete: json['delete'],
        edit: json['edit'],
        essence: json['essence'],
        top: json['top'],
      );
    } else {
      return PostActionModel(
        apply: false,
        delete: false,
        edit: false,
        essence: false,
        top: false,
      );
    }
  }
}