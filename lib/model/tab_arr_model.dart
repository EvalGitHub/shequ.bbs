import 'package:flutter/foundation.dart';

class TabArrItemModel {
  final String key;
  final String text;
  const TabArrItemModel({
    @required this.key,
    @required this.text,
  }) : assert(key != null) , assert(text != null);
}