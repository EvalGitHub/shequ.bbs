import 'package:shequ/model/level_info_model.dart';

class AuthorInfoModel {
  final String avatar;
  final String avatar32;
  final String avatar128;
  int followStatus;
  final LevelInfoModel level;
  final String nickname;
  final String number;
  final int postCount;
  final int replayCount;
  final int score1;
  final int sex;
  final String signature;
  final int uid;
  final String userName;
  AuthorInfoModel(
    {
      this.nickname, this.number, this.postCount, 
      this.replayCount, this.score1, this.sex, 
      this.signature, this.uid, this.userName, 
      this.avatar, this.avatar32, this.avatar128, 
      this.followStatus, this.level
    }
  );
  factory AuthorInfoModel.fromJson(Map<String, dynamic>json) {
    if (json != null) {
      return AuthorInfoModel(
        avatar: json['avatar'].toString(),
        avatar128: json['avatar128'],
        avatar32: json['avatar32'],
        followStatus: json['follow_status'],
        level: LevelInfoModel.fromJson(json['level']),
        nickname: json['nickname'],
        number: json['number'].toString(),
        postCount: json['post_count'],
        replayCount: json['replay_count'],
        score1: json['score1'],
        sex: json['sex'],
        signature: json['signature'],
        uid: json['uid'],
        userName: json['user_name'],
      ); 
    } else {
      return AuthorInfoModel(
        avatar: '',
        avatar128: '',
        avatar32: '',
        followStatus: 0,
        level: LevelInfoModel.fromJson(null),
        nickname: '',
        number: '',
        postCount: 0,
        replayCount: 0,
        score1: 0,
        sex: 0,
        signature: '',
        uid: 0,
        userName: '',
      );
    }
  }
}