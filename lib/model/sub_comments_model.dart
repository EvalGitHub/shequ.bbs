import 'package:shequ/model/user_info_model.dart';

class SubCommentsModel {
  final String content;
  final String createTime;
  final String createTimeFormat;
  final bool deletable;
  final int id;
  final int isAnon;
  final int parentId;
  final int parentUid;
  final String toUsername;
  final int uid;
  final UserInfoModel userInfo;

  SubCommentsModel({this.content, this.createTime, this.createTimeFormat, this.deletable, 
    this.id, this.isAnon, this.parentId, this.parentUid, this.toUsername, this.uid, this.userInfo});

  factory SubCommentsModel.fromJson(Map<String, dynamic>json) {
    if (json != null) {
      return SubCommentsModel(
        content: json['content'],
        createTime: json['create_time'],
        createTimeFormat: json['create_time_format'],
        deletable: json['deletable'],
        id: json['id'],
        isAnon: json['is_anon'],
        parentId: json['parent_id'],
        parentUid: json['parent_uid'],
        toUsername: json['to_username'],
        uid: json['uid'],
        userInfo: UserInfoModel.fromJson(json['user_info'])
      );
    } else {
      return SubCommentsModel(
        content: '',
        createTime: '',
        createTimeFormat: '',
        deletable: false,
        id: 0,
        isAnon: 0,
        parentId: 0,
        parentUid: 0,
        toUsername: '',
        uid: 0,
        userInfo: UserInfoModel.fromJson(null),
      );
    }
  }
}