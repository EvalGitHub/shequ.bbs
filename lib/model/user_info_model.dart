class UserInfoModel {
  var avatar;
  final String avatar32;
  final String nickname;
  final int sex;
  final String username;

  UserInfoModel({this.avatar, this.avatar32, this.nickname, this.sex, this.username});

  factory UserInfoModel.fromJson(Map<String, dynamic>json) {
    if (json == null) {
      return UserInfoModel(
        avatar: '',
        avatar32: '',
        nickname: '',
        sex: 0,
        username: ''
      );
    } else {
      return UserInfoModel(
        avatar: json['avatar'],
        avatar32: json['avatar32'],
        nickname: json['nickname'],
        sex: json['sex'],
        username: json['usename']
      );
    }
  }
  
}