class CommunityModel {
  final String desc;
  final String forumTypeTitle;
  final int id;
  final String logo;
  final String title;
  const CommunityModel({this.desc, this.forumTypeTitle, this.id, this.logo, this.title});
  
  factory CommunityModel.fromJson(Map<String, dynamic>json) {
    if (json == null) {
      return CommunityModel(
        desc: '',
        forumTypeTitle: '',
        id: 0,
        logo: '',
        title: '',
      );
    } else {
      return CommunityModel(
        desc: json['desc'],
        forumTypeTitle: json['forum_type_title'],
        id: json['id'],
        logo: json['logo'],
        title: json['title'],
      );
    }
  }
}