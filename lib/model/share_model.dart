class ShareModel {
  final String content;
  final String img;
  final String title;

  const ShareModel({this.content, this.img, this.title});

  factory ShareModel.fromJson(Map<String, dynamic>json) {
    if (json != null) {
      return ShareModel(
        content: json['content'],
        img: json['img'],
        title: json['title'],
      );
    } else {
      return ShareModel(
        content: '',
        img: '',
        title: '',
      );
    }
  }
}