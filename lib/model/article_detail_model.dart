import 'package:shequ/model/author_info_model.dart';
import 'package:shequ/model/post_action_model.dart';
import 'package:shequ/model/share_model.dart';

class ArticleDetailModel {
  final String anonName;
  final AuthorInfoModel authorInfo;
  final String content;
  final String createTime;
  final String createTimeFormat;
  final bool currentUserCollected;
  bool currentUserPraised;
  final int device;
  final int essence;
  final int forumId;
  final String forumModerator;
  final String forumTitle;
  final int id;
  final int isAnon;
  final PostActionModel postAction;
  final int praiseCount;
  final int replyCount;
  final ShareModel share;
  final String title;
  final int top;
  final int type;
  final int uid;
  final int viewCount;

  ArticleDetailModel(
    {
      this.anonName, this.authorInfo, this.content, 
      this.createTime, this.createTimeFormat, this.currentUserCollected, 
      this.currentUserPraised, this.device, this.essence, this.forumId, 
      this.forumModerator, this.forumTitle, this.id, this.isAnon, this.postAction, 
      this.praiseCount, this.replyCount, this.share, this.title, this.top, this.type,
      this.uid, this.viewCount
    }
  );
 
  factory ArticleDetailModel.fromJson(Map<String, dynamic>json) {
    if (json != null) {
      return ArticleDetailModel(
        anonName: json['anon_name'],
        authorInfo: AuthorInfoModel.fromJson(json['author_info']),
        content: json['content'],
        createTime: json['create_time'],
        createTimeFormat: json['create_time_format'],
        currentUserCollected: json['current_user_collected'],
        currentUserPraised: json['current_user_praised'],
        device: json['device'],
        essence: json['essence'],
        forumId: json['forum_id'],
        forumModerator: json['forum_moderator'],
        forumTitle: json['forum_title'],
        id: json['id'],
        isAnon: json['is_anon'],
        postAction: PostActionModel.fromJson(json['post_action']),
        praiseCount: json['praise_count'],
        replyCount: json['reply_count'],
        share: ShareModel.fromJson(json['share']),
        title: json['title'],
        top: json['top'],
        type: json['type'],
        uid: json['uid'],
        viewCount: json['view_count'],
      );
    } else {
      return ArticleDetailModel(
        anonName: '',
        authorInfo: AuthorInfoModel.fromJson(null),
        content: '',
        createTime: '',
        createTimeFormat: '',
        currentUserCollected: false,
        currentUserPraised: false,
        device: 0,
        essence: 0,
        forumId: 0,
        forumModerator: '',
        forumTitle: '',
        id: 0,
        isAnon: 0,
        postAction: PostActionModel.fromJson(null),
        praiseCount: 0,
        replyCount: 0,
        share: ShareModel.fromJson(null),
        title: '',
        top: 0,
        type: 0,
        uid: 0,
        viewCount: 0,
      );
    }
  }
}