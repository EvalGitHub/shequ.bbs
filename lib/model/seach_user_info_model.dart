import 'package:shequ/model/level_info_model.dart';

class SearchUserInfoModel {
  final String avatar128;
  final LevelInfoModel level;
  final String nickname;
  final int postCount;
  final int replyCount;
  final int score1;
  final int sex;
  final String signature;
  final int uid;
  final String username;

  SearchUserInfoModel({this.avatar128, this.level, this.nickname, this.postCount, this.replyCount, this.score1, this.sex, this.signature, this.uid, this.username});
  
  factory SearchUserInfoModel.fromJson(Map<String, dynamic>json) {
    return SearchUserInfoModel(
      avatar128: json['avatar128']??'',
      level: LevelInfoModel.fromJson(json['level']),
      nickname: json['nickname']??'',
      postCount: json['post_count']??0,
      replyCount: json['reply_count']??0,
      score1: json['score1']??0,
      sex: json['sex']??0,
      signature: json['signature'] != ''? json['signature'] : '还没想好说些什么...',
      uid: json['uid']??0,
      username: json['username']??'',
    );
  }
}