class CommunityClubModel {
  final int id;
  final String intro;
  final String logoUrl;
  final int memberCount;
  final String name;
  final int postCount;
  final int uid;

  CommunityClubModel({this.id, this.intro, this.logoUrl, this.memberCount, this.name, this.postCount, this.uid});
  factory CommunityClubModel.fromJson(Map<String, dynamic>json) {
    if (json == null) {
      return CommunityClubModel(
        id: 0,
        intro: null,
        logoUrl: null,
        memberCount: 0,
        name: null,
        postCount: 0,
        uid: 0,
      );
    } else {
      return CommunityClubModel(
        id: json['id'],
        intro: json['intro'],
        logoUrl: json['logo_url'],
        memberCount: json['member_count'],
        name: json['name'],
        postCount: json['post_count'],
        uid: json['uid'],
      );
    }
  }
}