import 'package:shequ/model/sub_comments_model.dart';

class CommentModel {
  final String authorName;
  final List<SubCommentsModel> comments;
  final String content;
  final String createTime;
  final String createTimeFormat;
  bool currentUserPraised;
  final bool deletable;
  final String deleteTime;
  final int device;
  final int floor;
  final int id;
  final int isAnon;
  final int position;
  final int postId;
  final int praiseCount;
  final int status;
  final int uid;
  final String updateTime;
  final String userAvatar;
  var userLevelIcon;
  final int userPostCount;
  final int userReplyCount;
  final int userScore;
  final String username;

  CommentModel({this.authorName, this.comments, this.content, this.createTime,
    this.createTimeFormat, this.currentUserPraised, this.deletable, this.deleteTime, this.device, this.floor,
    this.id, this.isAnon, this.position, this.postId, this.praiseCount, this.status, 
    this.uid, this.updateTime, this.userAvatar, this.userLevelIcon, this.userPostCount, 
    this.userReplyCount, this.userScore, this.username});
  
  factory CommentModel.fromJson(Map<String, dynamic>json) {
    if (json != null) {
      return CommentModel(
        authorName: json['author_name']??'',
        comments: json['comments'].length > 0 ? (json['comments'] as List).map((item) => SubCommentsModel.fromJson(item)).toList() : [],
        content: json['content'],
        createTime: json['create_time'],
        createTimeFormat: json['create_time_format'],
        currentUserPraised: json['current_user_praised'],
        deletable: json['deletable'],
        deleteTime: json['delete_time'],
        device: json['device'],
        floor: json['floor'],
        id: json['id'],
        isAnon: json['is_anon']?? 0,
        position: json['position']??'',
        postId: json['postId']??0,
        praiseCount: json['praise_count']??0,
        status: json['status']??'',
        uid: json['uid']??0,
        updateTime: json['update_time']??'',
        userAvatar: json['user_avatar']??'',
        userLevelIcon: json['user_level_icon'].toString(),
        userPostCount: json['user_post_count']??0,
        userReplyCount: json['user_reply_count']??0,
        userScore: json['user_score']??0,
        username: json['username']??'',
      );
    } else {
      return CommentModel(
        authorName: '',
        comments: [null],
        content: '',
        createTime: '',
        createTimeFormat: '',
        deletable: false,
        deleteTime: '',
        device: 0,
        floor: 0,
        id: 0,
        isAnon: 0,
        position: 0,
        postId: 0,
        praiseCount: 0,
        status: 0,
        uid: 0,
        updateTime: '',
        userAvatar: '',
        userLevelIcon: '',
        userPostCount: 0,
        userReplyCount: 0,
        userScore: 0,
        username: '',
      );
    }
  }
}