// 帖子

class PostDetailModel {
  final String anonName;
  final String content;
  final String createTime;
  final String createTimeFormat;
  final bool currentUserPraised;
  final int forumId;
  final String forumTitle;
  final int id;
  final int isAnon;
  final int praiseCount;
  final int replyCount;
  final String title;
  final int uid;
  final int viewCount;

  PostDetailModel({this.anonName, this.content, this.createTime, this.createTimeFormat, this.currentUserPraised, this.forumId, this.forumTitle, this.id, this.isAnon, this.praiseCount, this.replyCount, this.title, this.uid, this.viewCount});
  factory PostDetailModel.fromJson(Map<String, dynamic>json) {
    return PostDetailModel(
      anonName: json['anon_name']??'',
      content: json['content']??'',
      createTime: json['create_time']??'',
      createTimeFormat: json['create_time_format']??'',
      currentUserPraised: json['current_user_praised']?? false,
      forumId: json['forum_id']??0,
      forumTitle: json['forum_title']??'',
      id: json['id']??0,
      isAnon: json['is_anon']??0,
      praiseCount: json['praise_count']??0,
      replyCount: json['reply_count']??0,
      title: json['title']??'',
      uid: json['uid']??0,
      viewCount: json['view_count']??0,
    );
  }
}