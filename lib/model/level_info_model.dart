class LevelInfoModel {
  final String icon;
  final String title;
  const LevelInfoModel({this.icon, this.title});

  factory LevelInfoModel.fromJson(Map<String, dynamic>json) {
    if (json != null) {
      return LevelInfoModel(
        icon: json['icon'].toString(),
        title: json['title'], 
      );
    } else {
      return LevelInfoModel(
        icon: '',
        title: '', 
      );
    }
  }
}