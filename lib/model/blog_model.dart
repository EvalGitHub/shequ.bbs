class BlogModel {
  final String anonName;
  final String authorName;
  final String content;
  final String createTime;
  final String createTimeFormat;
  final int essence;
  final int forumId;
  final String forumTitle;
  final int id;
  final int isAnon;
  final int praiseCount;
  final int replyCount;
  final String title;
  final int top;
  final int type;
  final int uid;
  final String username;
  final int viewCount;

  BlogModel({
    this.anonName, this.authorName, this.content, this.createTime,
    this.createTimeFormat, this.essence, this.forumId, this.forumTitle, 
    this.id, this.isAnon, this.praiseCount, this.replyCount, this.title, 
    this.top, this.type, this.uid, this.username, this.viewCount
  });

  factory BlogModel.fromJson(Map<String, dynamic>json) {
    return BlogModel(
      anonName: json['anon_name']??'',
      authorName: json['author_name']??'',
      content: json['content']??'',
      createTime: json['create_time']??'',
      createTimeFormat: json['create_time_format']??'',
      essence: json['essence']??0,
      forumId: json['forum_id']??0,
      forumTitle: json['forum_title']??json['forum']??'',
      id: json['id']??0,
      isAnon: json['is_anon']??0,
      praiseCount: json['praise_count']??0,
      replyCount: json['reply_count']??0,
      title: json['title']??'',
      top: json['top']??0,
      type: json['type']??0,
      uid: json['uid']??0,
      username: json['username']??'',
      viewCount: json['view_count']??'',
    );
  }
}