import 'package:flutter/widgets.dart';
class MyIcon {
  static const IconData talk = const IconData(0xe866, fontFamily: 'IconFont', matchTextDirection: true);
  static const IconData search = const IconData(0xe66e, fontFamily: 'IconFont', matchTextDirection: true);
  static const IconData view = const IconData(0xe6ad, fontFamily: 'IconFont', matchTextDirection: true);
  static const IconData msg = const IconData(0xe866, fontFamily: 'IconFont', matchTextDirection: true);
  static const IconData chat = const IconData(0xe70f, fontFamily: 'IconFont', matchTextDirection: true);
  static const IconData like = const IconData(0xe619, fontFamily: 'IconFont', matchTextDirection: true);
  static const IconData upload = const IconData(0xe6be, fontFamily: 'IconFont', matchTextDirection: true);
  static const IconData recommend = const IconData(0xe62b, fontFamily: 'IconFont', matchTextDirection: true);
  static const IconData emoji = const IconData(0xe646, fontFamily: 'IconFont', matchTextDirection: true);
  static const IconData picture = const IconData(0xe60d, fontFamily: 'IconFont', matchTextDirection: true);
  static const IconData choice = const IconData(0xe682, fontFamily: 'IconFont', matchTextDirection: true);
  static const IconData person_add_outline = const IconData(0xe73b, fontFamily: 'IconFont', matchTextDirection: true);
  static const IconData person_delete_outline = const IconData(0xe6bd, fontFamily: 'IconFont', matchTextDirection: true);
  static const IconData person_done_outline = const IconData(0xe6bf, fontFamily: 'IconFont', matchTextDirection: true);
  static const IconData release_airplane = const IconData(0xe6a9, fontFamily: 'IconFont', matchTextDirection: true);
  static const IconData wechat_icon = const IconData(0xe602, fontFamily: 'IconFont', matchTextDirection: true);
  static const IconData qq_zone = const IconData(0xe671, fontFamily: 'IconFont', matchTextDirection: true);
  static const IconData wechat_zone = const IconData(0xe660, fontFamily: 'IconFont', matchTextDirection: true);
  static const IconData qq_icon = const IconData(0xe616, fontFamily: 'IconFont', matchTextDirection: true); 
}