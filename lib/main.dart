
import 'package:flutter/material.dart';
import 'package:shequ/pages/add_post.dart';
import 'package:shequ/pages/community_page.dart';
import 'package:shequ/pages/login_page.dart';
import 'package:shequ/pages/my_sub_page/my_post.dart';
import 'package:shequ/pages/notify_page.dart';
import 'package:shequ/pages/search_page.dart';
import 'package:shequ/pages/view_datail.dart';

import 'navigator/tab_navigator.dart';

void main() => runApp(MyApp());

class MyApp extends StatelessWidget {
  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: '社区',
      theme: ThemeData(
        primarySwatch: Colors.red,
      ),
      home: TabNavigator(),
      // initialRoute: 'LoginPage',
      routes: {
        'LoginPage': (context) => LoginPage(),
        'TabNavigator': (context) => TabNavigator(),
        'SearchPage': (context) => SearchPage(),
        'ViewDetail': (context) => ViewDetail(articleId: '',),
        'NotifyMessage': (context) => NotifyMessage(),
        'CommunityPage': (context) => CommunityPage(),
        'MyPost': (context) => MyPost(uid: 0),
        'AddPost': (context) => AddPost(),
      }
    );
  }
}
