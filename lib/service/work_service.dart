import 'dart:async';
import 'dart:convert';
import 'package:flutter/material.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:http/http.dart' as http;
import 'package:shequ/utils/storage_util.dart';

Map<String, dynamic> workApi = {
  'workList': 'https://bbs.codemao.cn/api/post',
  'praise': 'https://bbs.codemao.cn/api/post/praise',
  'getArticleDetail': 'https://bbs.codemao.cn/api/post/',
  'watch': 'https://bbs.codemao.cn/api/user/follow',
  'getComments': 'https://bbs.codemao.cn/api/post/',
  'praiseReply': 'https://bbs.codemao.cn/api/reply/praise',
  'replyComments': 'https://bbs.codemao.cn/api/reply/comment',
  'addComments': 'https://bbs.codemao.cn/api/post/',
  'addPost': 'https://bbs.codemao.cn/api/post',
  'addCollect': 'https://bbs.codemao.cn/api/post/collect',
  'deletePost': 'https://bbs.codemao.cn/api/post',
};

class WorkService {
  // 获取动态列表
  static Future fetchListData(Map<String, dynamic>queryObj) async {
    String authorToken = await StorageUtil.getStringItem("token");
    final response = await http.get(
      workApi['workList'] + '?f=${queryObj['f']}&page=${queryObj['page']}&sort=${queryObj['sort']}',
      headers: {
        'Auth-Token': authorToken
      }
    );
    if (response.statusCode == 200) {
      Utf8Decoder utf8decoder = Utf8Decoder(); 
      var result = json.decode(utf8decoder.convert(response.bodyBytes));
      return result;
    } else {
      throw Exception('fail to load work list data');
    }
  }

  // 点赞动态
  static Future praiseItem(Map<String, dynamic>dataObj) async {
    String authorToken = await StorageUtil.getStringItem("token");
    final response = await http.post(
      workApi['praise'],
      headers: {
        'Auth-Token': authorToken
      },
      body: dataObj
    );
    if (response.statusCode == 200) {
      Utf8Decoder utf8decoder = Utf8Decoder(); 
      var result = json.decode(utf8decoder.convert(response.bodyBytes));
      return result;
    } else {
      throw Exception('fail to load work list data');
    } 
  }  

  // 点赞回复
  static Future praiseReply(Map<String, dynamic>dataObj) async {
    String authorToken = await StorageUtil.getStringItem("token");
    final response = await http.post(
      workApi['praiseReply'],
      headers: {
        'Auth-Token': authorToken
      },
      body: dataObj
    );
    if (response.statusCode == 200) {
      Utf8Decoder utf8decoder = Utf8Decoder(); 
      var result = json.decode(utf8decoder.convert(response.bodyBytes));
      return result;
    } else {
      throw Exception('fail to load work list data');
    } 
  }

  // 获取文章详情
  static Future getArticleDetail(Map<String, dynamic>dataObj) async {
    String authorToken = await StorageUtil.getStringItem("token");
    final response = await http.get(
      workApi['getArticleDetail'] + dataObj['id'],
      headers: { 'Auth-Token': authorToken}
    );
    if (response.statusCode == 200) {
      Utf8Decoder utf8decoder = Utf8Decoder();
      var result = json.decode(utf8decoder.convert(response.bodyBytes));
      return result['data'];
    } else {
      throw Exception('fail to load work list data');
    }
  }

  // 关注
  static Future watch(Map<String, dynamic>dataObj) async {
    String authorToken = await StorageUtil.getStringItem("token");
    final response = await http.post(
      workApi['watch'],
      headers: {
        'Auth-Token': authorToken
      },
      body: dataObj
    );
    if (response.statusCode == 200) {
      Utf8Decoder utf8decoder = Utf8Decoder(); 
      var result = json.decode(utf8decoder.convert(response.bodyBytes));
      return result;
    } else {
      throw Exception('fail to load work list data');
    } 
  } 

  // 获取评论
  static Future getComments(Map<String, dynamic>queryObj) async {
    String authorToken = await StorageUtil.getStringItem("token");
    final response = await http.get(
      workApi['getComments'] + '${queryObj['id']}/reply?page=${queryObj['page']}&author_only=${queryObj['author_only']}',
      headers: {
        'Auth-Token': authorToken
      }
    );
    if (response.statusCode == 200) {
      Utf8Decoder utf8decoder = Utf8Decoder(); 
      var result = json.decode(utf8decoder.convert(response.bodyBytes));
      return result['data'];
    } else {
      throw Exception('fail to load work list data');
    }
  }

  // 回复评论
  static Future replyComments(Map<String, dynamic>dataObj) async {
    String authorToken = await StorageUtil.getStringItem("token");
    final response = await http.post(
      workApi['replyComments'],
      headers: {
        'Auth-Token': authorToken
      },
      body: dataObj
    );
    if (response.statusCode == 200) {
      Utf8Decoder utf8decoder = Utf8Decoder(); 
      var result = json.decode(utf8decoder.convert(response.bodyBytes));
      return result;
    } else {
      Fluttertoast.showToast(
        gravity: ToastGravity.CENTER,
        backgroundColor: Colors.black,
        fontSize: 16,
        msg: '回复失败'
      );
      throw Exception('fail to reply');
    } 
  }

  // 添加评论
  static Future addComments(Map<String, dynamic>dataObj) async {
    String authorToken = await StorageUtil.getStringItem("token");
    final response = await http.post(
      workApi['addComments'] + dataObj['id'] + '/reply',
      headers: {
        'Auth-Token': authorToken
      },
      body: json.encode(dataObj)
    );
    if (response.statusCode == 200) {
      Utf8Decoder utf8decoder = Utf8Decoder(); 
      var result = json.decode(utf8decoder.convert(response.bodyBytes));
      return result;
    } else {
      throw Exception('fail to load work list data');
    } 
  }

  // 发布帖子
  static Future addPost(Map<String, dynamic>dataObj) async {
    String authorToken = await StorageUtil.getStringItem("token");
    final response = await http.post(
      workApi['addPost'],
      headers: {
        'Auth-Token': authorToken
      },
      body: dataObj
    );
    if (response.statusCode == 200) {
      Utf8Decoder utf8decoder = Utf8Decoder(); 
      var result = json.decode(utf8decoder.convert(response.bodyBytes));
      return result;
    } else {
      throw Exception('fail to relase post');
    } 
  }

  // 收藏帖子
  static Future addCollect(Map<String, dynamic>dataObj) async {
    String authorToken = await StorageUtil.getStringItem("token");
    final response = await http.post(
      workApi['addCollect'],
      headers: {
        'Auth-Token': authorToken
      },
      body: dataObj
    );
    if (response.statusCode == 200) {
      Utf8Decoder utf8decoder = Utf8Decoder(); 
      var result = json.decode(utf8decoder.convert(response.bodyBytes));
      return result;
    } else {
      throw Exception('fail to add collect');
    } 
  }

  // 删除帖子
  static Future deletePost(Map<String, dynamic>dataObj) async {
    String authorToken = await StorageUtil.getStringItem("token");
    final response = await http.delete(
      workApi['deletePost'] + '/${dataObj['post_id']}',
      headers: {
        'Auth-Token': authorToken
      },
    );
    if (response.statusCode == 200) {
      Utf8Decoder utf8decoder = Utf8Decoder(); 
      var result = json.decode(utf8decoder.convert(response.bodyBytes));
      return result;
    } else {
      throw Exception('fail to delete post');
    } 
  }
}