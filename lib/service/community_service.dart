import 'package:http/http.dart' as http;
import 'dart:async';
import 'dart:convert';

import 'package:shequ/utils/storage_util.dart';

Map<String, dynamic> communityServiceApi = {
  'clubTypeList': 'https://bbs.codemao.cn/api/club/types',
  'clubList': 'https://bbs.codemao.cn/api/club',
  'clubInfo': 'https://bbs.codemao.cn/api/club',
  'clubPhoto': 'https://bbs.codemao.cn/api/clubphotos',
  'clubComment': 'https://bbs.codemao.cn/api/club/posts',
  'joinCommunity': 'https://bbs.codemao.cn/api/club/apply',
  'communityModel': 'https://bbs.codemao.cn/api/forum/list',
  'addCommunity': 'https://bbs.codemao.cn/api/club',
  'getAllPost': 'https://bbs.codemao.cn/api/user/post',
  'getAllReply': 'https://bbs.codemao.cn/api/user/reply',
  'getViewRecord': 'https://bbs.codemao.cn/api/user/visit',
  'deleteViewRecord': 'https://bbs.codemao.cn/api/post/visit',
  'getWatcher': 'https://bbs.codemao.cn/api/user/relation',
  'cancelOrAddFollow': 'https://bbs.codemao.cn/api/user/follow',
  'getCollection': 'https://bbs.codemao.cn/api/user/collect',
  'revisePwd': 'https://bbs.codemao.cn/api/user/password',
};

class CommunityService {
  static Future clubTypeList() async {
    String authorToken = await StorageUtil.getStringItem("token");
    final response = await http.get(
      communityServiceApi['clubTypeList'],
      headers: {
        'Auth-Token': authorToken
      }
    );
    if (response.statusCode == 200) {
      Utf8Decoder utf8decoder = Utf8Decoder(); 
      var result = json.decode(utf8decoder.convert(response.bodyBytes));
      return result;
    } else {
      throw Exception('fail to load work list data');
    }
  }

  static Future clubList() async {
    String authorToken = await StorageUtil.getStringItem("token");
    final response = await http.get(
      communityServiceApi['clubList'],
      headers: {
        'Auth-Token': authorToken
      }
    );
    if (response.statusCode == 200) {
      Utf8Decoder utf8decoder = Utf8Decoder(); 
      var result = json.decode(utf8decoder.convert(response.bodyBytes));
      return result;
    } else {
      throw Exception('fail to load work list data');
    }
  }

  static Future clubInfo(int clubId) async {
    String authorToken = await StorageUtil.getStringItem("token");
    final response = await http.get(
      communityServiceApi['clubInfo'] + '/$clubId',
      headers: {
        'Auth-Token': authorToken
      }
    );
    if (response.statusCode == 200) {
      Utf8Decoder utf8decoder = Utf8Decoder(); 
      var result = json.decode(utf8decoder.convert(response.bodyBytes));
      return result;
    } else {
      throw Exception('fail to load work list data');
    }
  }

  static Future clubPhoto(int clubId) async {
    String authorToken = await StorageUtil.getStringItem("token");
    final response = await http.get(
      communityServiceApi['clubPhoto'] + "?club_id=$clubId",
      headers: {
        'Auth-Token': authorToken
      }
    );
    if (response.statusCode == 200) {
      Utf8Decoder utf8decoder = Utf8Decoder(); 
      var result = json.decode(utf8decoder.convert(response.bodyBytes));
      return result;
    } else {
      throw Exception('fail to load work list data');
    }
  }

  static Future clubComment(Map<String, dynamic>dataObj) async {
    String authorToken = await StorageUtil.getStringItem("token");
    final response = await http.get(
      communityServiceApi['clubComment'] + "?page=${dataObj['page']}&club_id=${dataObj['clubId']}",
      headers: {
        'Auth-Token': authorToken
      }
    );
    if (response.statusCode == 200) {
      Utf8Decoder utf8decoder = Utf8Decoder(); 
      var result = json.decode(utf8decoder.convert(response.bodyBytes));
      return result;
    } else {
      throw Exception('fail to load work list data');
    }
  }

  static Future joinCommunity(Map<String, dynamic>dataObj) async {
    String authorToken = await StorageUtil.getStringItem("token");
    final response = await http.post(
      communityServiceApi['joinCommunity'],
      headers: {
        'Auth-Token': authorToken
      },
      body: dataObj
    );
    if (response.statusCode == 200) {
      Utf8Decoder utf8decoder = Utf8Decoder(); 
      var result = json.decode(utf8decoder.convert(response.bodyBytes));
      return result;
    } else {
      throw Exception('fail to load work list data');
    }
  }

  static Future communityModel() async {
    String authorToken = await StorageUtil.getStringItem("token");
    final response = await http.get(
      communityServiceApi['communityModel'],
      headers: {
        'Auth-Token': authorToken
      }
    );
    if (response.statusCode == 200) {
      Utf8Decoder utf8decoder = Utf8Decoder(); 
      var result = json.decode(utf8decoder.convert(response.bodyBytes));
      return result;
    } else {
      throw Exception('fail to load work list data');
    }
  }

  static Future addCommunity(Map<String, dynamic>dataObj) async {
    String authorToken = await StorageUtil.getStringItem("token");
    final response = await http.post(
      communityServiceApi['addCommunity'],
      headers: {
        'Auth-Token': authorToken
      },
      body: dataObj
    );
    if (response.statusCode == 200) {
      Utf8Decoder utf8decoder = Utf8Decoder(); 
      var result = json.decode(utf8decoder.convert(response.bodyBytes));
      return result;
    } else {
      throw Exception('fail to load work list data');
    }
  }

  static Future getAllPost(Map<String, dynamic>dataObj) async {
    String authorToken = await StorageUtil.getStringItem("token");
    final response = await http.get(
      communityServiceApi['getAllPost'] + "?uid=${dataObj['uid']}&page=${dataObj['page']}&limit=${dataObj['limit']}",
      headers: {
        'Auth-Token': authorToken
      },
    );
    if (response.statusCode == 200) {
      Utf8Decoder utf8decoder = Utf8Decoder(); 
      var result = json.decode(utf8decoder.convert(response.bodyBytes));
      return result;
    } else {
      throw Exception('fail to load work list data');
    }
  }

  static Future getAllReply(Map<String, dynamic>dataObj) async {
    String authorToken = await StorageUtil.getStringItem("token");
    final response = await http.get(
      communityServiceApi['getAllReply'] + "?uid=${dataObj['uid']}&page=${dataObj['page']}&limit=${dataObj['limit']}",
      headers: {
        'Auth-Token': authorToken
      },
    );
    if (response.statusCode == 200) {
      Utf8Decoder utf8decoder = Utf8Decoder(); 
      var result = json.decode(utf8decoder.convert(response.bodyBytes));
      return result;
    } else {
      throw Exception('fail to load work list data');
    }
  }

  static Future getViewRecord(Map<String, dynamic>dataObj) async {
    String authorToken = await StorageUtil.getStringItem("token");
    final response = await http.get(
      communityServiceApi['getViewRecord'] + "?uid=${dataObj['uid']}&page=${dataObj['page']}&limit=${dataObj['limit']}",
      headers: {
        'Auth-Token': authorToken
      },
    );
    if (response.statusCode == 200) {
      Utf8Decoder utf8decoder = Utf8Decoder(); 
      var result = json.decode(utf8decoder.convert(response.bodyBytes));
      return result;
    } else {
      throw Exception('fail to load work list data');
    }
  }
  
  static Future deleteViewRecord(int postId) async {
    String authorToken = await StorageUtil.getStringItem("token");
    final response = await http.delete(
      communityServiceApi['deleteViewRecord'] + "?post_id=$postId",
      headers: {
        'Auth-Token': authorToken
      },
    );
    if (response.statusCode == 200) {
      Utf8Decoder utf8decoder = Utf8Decoder(); 
      var result = json.decode(utf8decoder.convert(response.bodyBytes));
      return result;
    } else {
      throw Exception('fail to delete view record data');
    }
  }

  static Future getWatcher(Map<String, dynamic>dataObj) async {
    String authorToken = await StorageUtil.getStringItem("token");
    final response = await http.get(
      communityServiceApi['getWatcher'] + "?type=${dataObj['type']}&uid=${dataObj['uid']}",
      headers: {
        'Auth-Token': authorToken
      },
    );
    if (response.statusCode == 200) {
      Utf8Decoder utf8decoder = Utf8Decoder(); 
      var result = json.decode(utf8decoder.convert(response.bodyBytes));
      return result;
    } else {
      throw Exception('fail to get watcher');
    }
  }

  static Future cancelOrAddFollow(Map<String, dynamic>dataObj) async {
    String authorToken = await StorageUtil.getStringItem("token");
    final response = await http.post(
      communityServiceApi['cancelOrAddFollow'],
      headers: {
        'Auth-Token': authorToken
      },
      body: dataObj
    );
    if (response.statusCode == 200) {
      Utf8Decoder utf8decoder = Utf8Decoder(); 
      var result = json.decode(utf8decoder.convert(response.bodyBytes));
      return result;
    } else {
      throw Exception('fail to cancel or add follow');
    }
  }

  static Future getCollection(Map<String, dynamic>dataObj) async {
    String authorToken = await StorageUtil.getStringItem("token");
    final response = await http.get(
      communityServiceApi['getCollection'] + "?page=${dataObj['page']}",
      headers: {
        'Auth-Token': authorToken
      },
    );
    if (response.statusCode == 200) {
      Utf8Decoder utf8decoder = Utf8Decoder(); 
      var result = json.decode(utf8decoder.convert(response.bodyBytes));
      return result;
    } else {
      throw Exception('fail to get watcher');
    }
  }

  static Future revisePwd(Map<String, dynamic>dataObj) async {
    String authorToken = await StorageUtil.getStringItem("token");
    final response = await http.put(
      communityServiceApi['revisePwd'],
      headers: {
        'Auth-Token': authorToken
      },
      body: dataObj
    );
    if (response.statusCode == 200) {
      Utf8Decoder utf8decoder = Utf8Decoder(); 
      var result = json.decode(utf8decoder.convert(response.bodyBytes));
      return result;
    } else {
      throw Exception('fail to revise pwd');
    }
  }
}