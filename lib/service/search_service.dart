import 'dart:async';

import 'dart:convert';
import 'package:http/http.dart' as http;
import 'package:shequ/utils/storage_util.dart';

Map<String, dynamic> searchApi = {
  'getSearchResult': 'https://bbs.codemao.cn/api/search',
};

class SearchService {
  // 搜索查询
  static Future getSearchResult(Map<String, dynamic>queryObj) async {
    String authorToken = await StorageUtil.getStringItem("token");
    final response = await http.get(
      searchApi['getSearchResult'] + '?key=${queryObj['key']}&page=${queryObj['page']}&type=${queryObj['type']}',
      headers: {
        'Auth-Token': authorToken
      }
    );
    if (response.statusCode == 200) {
      Utf8Decoder utf8decoder = Utf8Decoder(); 
      var result = json.decode(utf8decoder.convert(response.bodyBytes));
      return result;
    } else {
      throw Exception('fail to load work list data');
    }
  }
}