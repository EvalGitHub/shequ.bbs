import 'package:http/http.dart' as http;
import 'dart:async';
import 'dart:convert';
// import 'package:dio/dio.dart';
import 'package:shequ/utils/storage_util.dart';
// Dio dio = new Dio();
Map<String, dynamic> commonServiceApi = {
  'uploadImage': 'https://bbs.codemao.cn/api/upload/uploadimage',
  'getAllForum': 'https://bbs.codemao.cn/api/forum',
};

class CommonService {
  static Future uploadImage(Map<String, dynamic>dataObj) async {
    String authorToken = await StorageUtil.getStringItem("token");
    final response = await http.post(
      commonServiceApi['uploadImage'],
      body: dataObj,
      // options: Options(headers: {
      //   'Auth-Token': authorToken
      // })
      headers: {
        'Auth-Token': authorToken,
        'User-Agent': 'Mozilla/5.0 (iPhone; CPU iPhone OS 11_0 like Mac OS X) AppleWebKit/604.1.38 (KHTML, like Gecko) Version/11.0 Mobile/15A372 Safari/604.1'
      }
    );
    if (response.statusCode == 200) {
      Utf8Decoder utf8decoder = Utf8Decoder(); 
      var result = json.decode(utf8decoder.convert(response.bodyBytes));
      return result;
    } else {
      throw Exception('fail to upload image fail');
    }
  }
  // 获取所有的tab
  static Future getAllForum(Map<String, dynamic>dataObj) async {
    String authorToken = await StorageUtil.getStringItem("token");
    final response = await http.get(
      commonServiceApi['getAllForum'] + '?is_show_all=${dataObj['is_show_all']}',
      headers: {
        'Auth-Token': authorToken,
        'User-Agent': 'Mozilla/5.0 (iPhone; CPU iPhone OS 11_0 like Mac OS X) AppleWebKit/604.1.38 (KHTML, like Gecko) Version/11.0 Mobile/15A372 Safari/604.1'
      }
    );
    if (response.statusCode == 200) {
      Utf8Decoder utf8decoder = Utf8Decoder(); 
      var result = json.decode(utf8decoder.convert(response.bodyBytes));
      return result;
    } else {
      throw Exception('fail to upload image fail');
    }
  }
}