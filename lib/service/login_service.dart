import 'package:http/http.dart' as http;
import 'dart:async';
import 'dart:convert';

import 'package:shequ/utils/storage_util.dart';

Map<String, dynamic> loginApi = {
  'login': 'https://bbs.codemao.cn/api/user/login',
  'loginOut': 'https://bbs.codemao.cn/api/user/logout'
};

class LoginService {
  static Future login(Map<String, dynamic>dataObj) async {
    final response = await http.post(
      loginApi['login'],
      body: dataObj
    );
    if (response.statusCode == 200) {
      Utf8Decoder utf8decoder = Utf8Decoder(); 
      var result = json.decode(utf8decoder.convert(response.bodyBytes));
      return result;
    } else {
      throw Exception('fail to load work list data');
    } 
  }

  static Future loginOut() async {
    String authorToken = await StorageUtil.getStringItem("token");
    final response = await http.get(
      loginApi['loginOut'],
      headers: {
        'Auth-Token': authorToken
      },
    );
    if (response.statusCode == 200) {
      Utf8Decoder utf8decoder = Utf8Decoder(); 
      var result = json.decode(utf8decoder.convert(response.bodyBytes));
      return result;
    } else {
      throw Exception('fail to login out');
    }
  }
}