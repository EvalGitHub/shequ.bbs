import 'package:flutter/material.dart';
import 'package:shequ/pages/community_page.dart';
import 'package:shequ/pages/my_page.dart';
import 'package:shequ/pages/work_page.dart';
import 'package:shequ/pages/life_page.dart';

class TabNavigator extends StatefulWidget {
  @override 
  _TabNavigatorState createState() => _TabNavigatorState();
}

class _TabNavigatorState extends State<TabNavigator> with TickerProviderStateMixin  {
  final _defaultColor = Colors.grey;
  final _activeColor = Colors.redAccent;
  final double _iconSize = 25.0;
  int _currentIndex = 0;

  final PageController _pageViewCtrl = PageController(
    initialPage: 0
  );

  List _bottomBarArr = [
    {
      'text': '工作',
      'id': 0,
      'component': WorkPage,
      'icon': Icons.work,
    },
    {
      'text': '生活',
      'id': 1,
      'component': LifePage,
      'icon': Icons.home,
    },
    {
      'text': '社团',
      'id': 2,
      'component': CommunityPage,
      'icon': Icons.group,
    },
    {
      'text': '我的',
      'id': 3,
      'component': MyPage,
      'icon': Icons.settings,
    },
  ];

  @override 
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.white,
      body: PageView(
        controller: _pageViewCtrl,
        children: <Widget>[
          WorkPage(),
          LifePage(),
          CommunityPage(),
          MyPage(),
        ],
        onPageChanged: (index) {
          setState(() {
            _currentIndex = index;
          });
        },
      ),
      bottomNavigationBar: BottomAppBar(
        color: Colors.white,
        shape: CircularNotchedRectangle(),
        child: Container(
          height: 60,
          padding: EdgeInsets.fromLTRB(0, 6, 0, 6),
          child: Row(
            mainAxisSize: MainAxisSize.min,
            mainAxisAlignment: MainAxisAlignment.spaceAround,
            children: _createBottomBar,
          ),
        ),
      ),
      floatingActionButton: FloatingActionButton(
        heroTag: 'add',
        tooltip: 'add',
        child: Icon(Icons.add),
        onPressed: () {
          Navigator.pushNamed(context, 'AddPost');
        },
      ),
      floatingActionButtonLocation: FloatingActionButtonLocation.centerDocked,
    );
  }

  List<Widget> get _createBottomBar {
    return  _bottomBarArr.map((item) => _createBottomBarItem(item)).toList();
  }

  Widget _createBottomBarItem(var barItem) {
    return GestureDetector(
      child: Container(
        margin: EdgeInsets.fromLTRB(barItem['id'] == 2 ? 24 : 0, 0, barItem['id'] == 1 ? 24 : 0, 0),
        child: Column(
          children: <Widget>[
            Icon(barItem['icon'], color: _currentIndex !=barItem['id'] ? _defaultColor : _activeColor, size: _iconSize),
            Text(
              barItem['text'],
              style: TextStyle(
                color: _currentIndex !=barItem['id'] ? _defaultColor : _activeColor,
              )
            )
          ],
        )
      ),
      onTap: () {
        setState(() {
          _currentIndex = barItem['id'];
        });
        _pageViewCtrl.animateToPage(barItem['id'],  duration: const Duration(milliseconds: 100), curve: Curves.easeOutSine);
      },
    );
  } 
}